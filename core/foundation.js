var DZ = {
    defaultRel: 'content',
    rel: 'content',
    get: {
        'serviceUri': 'http://localhost/kao/index.php/',
        'loginUri': 'http://localhost/kao/index.php/login'
    },
    dataTableReport: function(tableName, isi){
        // $('#'+tableName+' .looptemplate').remove();
        var oTable = $('#'+tableName).dataTable({
            "bScrollCollapse": true,
            "bPaginate": false,
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "  <<  ",
                    "sLast": "  >>  ",
                    "sNext": "  >  ",
                    "sPrevious": "  <  "
                }
            },
            "aaSorting": [],
            // "fixedHeader": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": -1,
            "bAutoWidth": false,
            "bRetrieve": true,
            "aLengthMenu": [[20, 25], [20, 25]],
            "bInfo": false //Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });
        // $('#'+tableName).after(isi);
        return oTable;
    },
    dataTableReport2: function(tableName, isi){
        // $('#'+tableName+' .looptemplate').remove();
        var oTable = $('#'+tableName).dataTable({
            "bScrollCollapse": true,
            "bFilter": false,
            "bPaginate": false,
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "  <<  ",
                    "sLast": "  >>  ",
                    "sNext": "  >  ",
                    "sPrevious": "  <  "
                }
            },
            "ordering": false,
            "aaSorting": [],
            "sPaginationType": "full_numbers",
            "iDisplayLength": -1,
            "bAutoWidth": false,
            "bRetrieve": true,
            "aLengthMenu": [[20, 25], [20, 25]],
            "bInfo": false //Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });
        // $('#'+tableName).after(isi);
        return oTable;
    },
    dataTable: function(tableName){
        var isi = $('#'+tableName+' .looptemplate').clone();
        $('#'+tableName+' .looptemplate').remove();
        var oTable = $('#'+tableName).dataTable({
            "bScrollCollapse": true,
            "bPaginate": true,
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "  <<  ",
                    "sLast": "  >>  ",
                    "sNext": "  >  ",
                    "sPrevious": "  <  "
                }
            },
            "order": [[ 0, 'asc' ], [ 1, 'asc' ]],
            // "fixedHeader": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 10,
            "bAutoWidth": false,
            // "bRetrieve": true,
            // "bDestroy": true,
            "aLengthMenu": [[10, 25], [10, 25]],
            "bInfo": true //Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });
        $('#'+tableName).after(isi);
        return oTable;
    },
    dataTablePopup: function(tableName){
        var isi = $('#'+tableName+' .looptemplate').clone();
        $('#'+tableName+' .looptemplate').remove();
        var oTable = $('#'+tableName).dataTable({
            "bScrollCollapse": true,
            "bPaginate": true,
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "  <<  ",
                    "sLast": "  >>  ",
                    "sNext": "  >  ",
                    "sPrevious": "  <  "
                }
            },
            "order": [[ 0, 'asc' ], [ 1, 'asc' ]],
            // "fixedHeader": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 5,
            "bAutoWidth": false,
            // "bRetrieve": true,
            // "bDestroy": true,
            "aLengthMenu": [[5, 10], [5, 10]],
            "bInfo": false //Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });
        $('#'+tableName).after(isi);
        return oTable;
    },
    ajaxExt: function (data) {
        data.url = data.url;
        data.data = JSON.stringify(data.data) || {};
        data.type = data.type ? 'get' : 'post';
        data.contentType = 'application/json';
        var ret = data;

        ret.error = function(jqXHR, textStatus, errorThrown) {
            if(typeof error == 'function') {
                options.error(jqXHR, textStatus, errorThrown);
                return;
            }

            try {
                var o = $.parseJSON(jqXHR.responseText);
                if(o.status == 'error' && o.errno == '1') {
                    location.href=DZ.loginUri;
                    return;
                }
            } catch(e) {
                alert('Error occured when parsing JSON. Get response text: ' + jqXHR.responseText);
            }
        };
        ret.complete = function(){
            // $('#myModal').foundation('reveal', 'close');
        };

        return $.ajax(ret);
    },
    remDataTable: function(tableName){
        var isi = $('#'+tableName+' .looptemplate').clone();
        $('#'+tableName).dataTable().fnDestroy();
        $('#'+tableName+' .looptemplate').remove();
        $('#'+tableName).after(isi);
    },
    showAlertSuccess: function (self, msg, id, flag){
        $(".alert").remove();
        // $('#myModal').modal('hide');

        if(flag == 0) { //ALERT POPUP DITUTUP
            $(id).before('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');
            self.closePopup();
            $('body').animate({
                scrollTop: 0
            }, 1000);
        } else if(flag == 1) { //ALERT POPUP TIDAK DITUTUP
            $(id).after('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');
        } else if(flag == 3) { //ALERT BIASA
            $(id).before('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');
            $('body').animate({
                scrollTop: 0
            }, 1000);
        }
        setTimeout(function(){
            $(".alert").remove();
            // if(flag == 1) {
            //     self.closePopup();
            // }
        },2500);
    },
    showAlertSuccess2: function (msg){
        $(".alert").remove();
        // $('#myModal').modal('hide')
        $('.modal-footer').after('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');

        setTimeout(function(){
            $(".alert").remove();
        },2500);
    },
    showAlertWarning: function (msg, id){
        $(".alert").remove();
        // $('#myModal').modal('hide');
        $(id).after('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');
    },
    showAlertInfo: function (msg){
        $(".alert").remove();
        $('#myModal').modal('hide')
        $('.addbutton').before('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+msg+'</div>');
    },
    clearFormInput: function (btnName) {
        $('.'+btnName).click(function(){
            $(':input','#action')
            .not(':button, :submit, :reset')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
            $(".alert").remove();

        });
    },
    callBackSelect: function (id) {
      $(id).select2();
      $(id).trigger('change');
      $(id).attr("style", "width:100%");
  },
  navAttr: function(navActive) {
    $('#nav-Dashboard').removeClass('active');
    $('#nav-Report').removeClass('active');
    $('#nav-Library').removeClass('active');
    $('#nav-Report-category').removeClass('active');
    $('#nav-Report-menu').removeClass('active');
    $('#nav-Report-modifier').removeClass('active');
    $('#nav-Report-deal').removeClass('active');
    $('#nav-Report-discount').removeClass('active');
    $('#nav-Report-inventory').removeClass('active');
    $('#nav-Setting').removeClass('active');
    $('#nav-Setting-settings').removeClass('active');
    $('#nav-Setting-branch').removeClass('active');
    $('#nav-Setting-section').removeClass('active');
    $('#nav-Setting-user').removeClass('active');
    $('#nav-Setting-customer').removeClass('active');
    $('#nav-Setting-payment-method').removeClass('active');
    $('#nav-Setting-promotion').removeClass('active');
    $('#nav-Setting-expense-transaction').removeClass('active');
    $('#nav-Setting-floor').removeClass('active');
    navActive.addClass('active');
  }
}

var format = {
    shortDate: function (date) {
        var d = new Date(date);
        var day = d.getDate();
        var month = d.getMonth();
        var year = d.getFullYear();

        if (month < 10) {
            month = "0" + month;
        }

        if (day < 10) {
            day = "0" + day;
        }
        var months = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December" ];

        var selectedMonthName = months[month];
        return day + "-" + month + "-" + year;
    }
}