(function ($F) {
    var config = {};

    config.baseUri     = 'http://localhost/hello-bill/';
    config.loginUri    = 'http://localhost/hello-bill/test.html';
    config.imageUri    = config.baseUri + 'images/';
    config.serviceUri  = 'http://localhost/hello-bill/services/index.php/';
    config.uploadUri   = 'http://localhost/hello-bill/file_upload/';
    config.viewUri     = config.baseUri + 'view/';
    config.rollBar     = false ;
    config.months      = ['','January','February','March','April','May','June','July','August','September','October','November','December'];
    config.shortMonths = ['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],

    config.defaultRel  = 'primarycontent',

    config.debug       = true;

    $F.config.load(config);
})($F);