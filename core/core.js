(function ($, $F) {
    "use strict";

    $F.alertSuccess = function (msg,place){
        $(place).before('<div class="alert alert-success" role="alert">'+msg+'</div>');
        setTimeout(function() {
          $(".alert").fadeTo(500, 0).slideUp(500, function(){
              $(this).remove();
          });
      }, 4000);
    };
    // contoh cara pakai altersuccess
    // $F.alertSuccess(data.txt,'.modalalert');

    $F.alertError = function (msg,place){
        $(place).before('<div class="alert alert-danger" role="alert">'+msg+'</div>');
    };
    // contoh cara pakai alertError
    // $F.alertError(data.txt,'.modalalert');

    $F.readFile = function (el, afterRead) {
        if(!window.FileReader) { afterRead(false, null); return false; }

        var read = new window.FileReader();
        read.onload = (function(after, f){
            return function(e) {
                var output = {
                    data: window.btoa(e.target.result),
                    name: f.name,
                    size: f.size,
                    type: f.type
                }
                after(true, output);
            }
        })(afterRead, el);

        if(read.readAsBinaryString)
            read.readAsBinaryString(el);
        else
            read.readAsArrayBuffer(el);

        return true;
    },
    $F.readURL = function (input,id,Idb) {
        var ext = input.value.substring(input.value.lastIndexOf('.') + 1);

        if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG")
            { document.getElementById(Idb).style.display="";
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+id+'').attr('src', e.target.result);
                $('#'+id+id.charAt(id.length-1)+'').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    } else {
        $F.alertError('Invalid Format Image','.popupalert');
        document.getElementById('myFile').value = '';
    }
}

})(jQuery, $F);