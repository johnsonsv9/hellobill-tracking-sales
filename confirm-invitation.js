
$(document).ready(function($) {
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
   $.ajax({
         url: "services/index.php/confirm/getConfirmInvitation/"+getUrlParameter('token'), 
         success: 
         function(result){
            var item = result.data.data;
            $('#username').val(item.Username);
            $('#fullname').val(item.Fullname);
         }
   });
  $('#action').submit(function(e){
    e.preventDefault();
     $.ajax({
         url: "services/index.php/confirm/action/"+getUrlParameter('token'), 
         type: "POST",
         data : $('#action').serialize(),
         success: 
         function(result){
            var err = '';
             if(result.data.data.Message == 'Success'){
                alert("Confirm Invitation Success");
                window.location.replace('login.html');
             }
             else{
                for(var i = 0; i < result.data.data.Errors.length; i++){
                    err += result.data.data.Errors[i].ID +" , "+ result.data.data.Errors[i].Msg + '</br>';
                }
                DZ.showAlertWarning(err, "#action :submit", 0);
             }
         }
   });
  });

});