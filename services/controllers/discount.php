<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class discount extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getAllDataForHardware($id){
		$data = $this->rest->post();
    $param = array(
				 'HardwareID' => $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount_detail_by_hardware_id', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getAllDataForLicense($id){
		$data = $this->rest->post();
    $param = array(
				 'LicenseID' => $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount_detail_by_license_id', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	  function getDetailData($id){
			$data = $this->rest->post();
	    $param = array(
					 'DiscountID' => $id,
	         'Token' =>  $this->session->userdata('token')
	     );
	    $variabel = json_encode($param);
			// print_r($variabel);
			// die();
	    $opts = array('http' =>
	          array(
	              'method'  => 'POST',
	              'header'  => 'Content-type: application/json',
	              'content' => $variabel
	          )
	    );
	    $context  = stream_context_create($opts);
	    $data = file_get_contents(SERVICE_URL.'get_discount_detail_by_discount_id', false, $context);
			// print_r($data);
			// die();
	    $data = json_decode($data, true);

	    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
	  }

	function action(){
		$data = $this->rest->post();
    $param = array(
         'DiscountCode' =>  $data->optiondata->data->DiscountCode,
         'DiscountName' => $data->optiondata->data->DiscountName,
				 'Type' => $data->optiondata->data->Type,
				 'Description' => $data->optiondata->data->Description,
				 'Token' => $this->session->userdata('token')
    );
		if(isset($data->optiondata->id)) $param["DiscountID"] = $data->optiondata->id;
		if($data->optiondata->data->Percentage != "") $param["DiscountPercentage"] = $data->optiondata->data->Percentage;
		else if($data->optiondata->data->Value != "") $param["DiscountValue"] = $data->optiondata->data->Value;
		if(isset($data->optiondata->data->Period) && $data->optiondata->data->Period != "") {
		// if(isset($data->optiondata->data->StartDate) && isset($data->optiondata->data->EndDate) && $data->optiondata->data->StartDate != "" && $data->optiondata->data->EndDate != "") {
			// $param["StartDate"] = $data->optiondata->data->StartDate;
			// $param["EndDate"] = $data->optiondata->data->EndDate;
			$param["StartDate"] = substr($data->optiondata->data->Period, 0, 10);
			$param["EndDate"] = substr($data->optiondata->data->Period, 13);
		}
		if(isset($data->optiondata->data->Item)) {
			if($data->optiondata->data->Type == "Hardware") $param["HardwareID"] = $data->optiondata->data->Item;
			else $param["LicenseID"] = $data->optiondata->data->Item;
		}

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_discount', false, $context);
    $data = json_decode($data, true);
		// print_r($data);
		// die();
			// $sess["discountid"] = $data["data"];
			// $this->session->set_userdata($sess);
		// nanti $this->session->set_userdata('discountid') bakal dioper ke discountdetail action
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'DiscountID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'DiscountID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_discount', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
