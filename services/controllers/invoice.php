<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class invoice extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){ //Invoice, tabel semua invoice
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice', false, $context);
    $data = json_decode($data, true);
		$data['UserID'] = $this->session->userdata('user')[0]["UserID"];
		$data['UserTypeName'] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getAllDataReseller(){ //InvoiceReseller, tabel semua invoice reseller
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_reseller', false, $context);
    $data = json_decode($data, true);
		$data['Usertype'] = $this->session->userdata('user')[0]["UserTypeName"];
		$data['UserID'] = $this->session->userdata('user')[0]["UserID"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){ //Quotation, generate header invoice dari quotation yg sudah di approve, download, dan request inv
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $data->id,
				 'Token' => $this->session->userdata('token')
    );

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_invoice', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

		function actionReseller(){ //Quotation, generate invoice reseller dari quotation yg sudah di app, download, req inv
			$data = $this->rest->post();
	    $param = array(
					 'QuotationID' =>  $data->id,
					 'Token' => $this->session->userdata('token')
	    );

	    $variabel = json_encode($param);
			// print_r($variabel);
			// die();
	    $opts = array('http' =>
	          array(
	              'method'  => 'POST',
	              'header'  => 'Content-type: application/json',
	              'content' => $variabel
	          )
	    );
	    $context  = stream_context_create($opts);
	    $data = file_get_contents(SERVICE_URL.'insert_update_invoice_reseller', false, $context);
			// print_r($data);
			// die();
	    $data = json_decode($data, true);
	    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
	  }

	function getUpdateData($id){ //ngambil header invoice utk invoice tertentu, ada di payment & preview
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateDataReseller($id){ //ngambil header invoice reseller utk invoice reseller tertentu
		$data = $this->rest->post();
    $param = array(
				 'InvoiceResellerID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_reseller_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getDetailData($id){ //ngambil semua inv payment utk inv tertentu
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_payment_by_invoice_id', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateDetailData($id){ //ngambil inv payment tertentu dari inv payment dan inv tertentu
		$data = $this->rest->post();
    $param = array(
				 'InvoicePaymentID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_payment_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		// $data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function actionPayment($id){ //insert dan/atau update payment
		$data = $this->rest->post();
		// print_r($data);
		// die();
    $param = array(
				 'InvoiceID' =>  $id,
				 'Date' =>  $data->optiondata->data->Date,
				 'Paid' =>  $data->optiondata->data->Amount,
				 'Token' => $this->session->userdata('token')
    );
		$param['TransferProof']['Data'] = $data->optiondata->files[0]->data;
		$param['TransferProof']['Filename'] = $data->optiondata->files[0]->name;
		if($data->optiondata->id != '') $param['InvoicePaymentID'] = $data->optiondata->id;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_invoice_payment', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function actionStatus($id){ //update status invoice
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
				 'Status' =>  $data->Status,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'update_invoice_status', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function actionStatusReseller($id){ //update status invoice reseller
		$data = $this->rest->post();
    $param = array(
				 'InvoiceResellerID' =>  $id,
				 'Status' =>  $data->Status,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'update_invoice_reseller_status', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){ //ga kepake
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_invoice', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
