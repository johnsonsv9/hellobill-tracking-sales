<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class quotation extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
				$this->load->library('Excel');
        $this->load->library('Pdf');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token'),
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_quotation', false, $context);
    $data = json_decode($data, true);
		$data["UserID"] = $this->session->userdata('user')[0]["UserID"];
		$data["UserTypeName"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function action(){
		$data = $this->rest->post();
    $param = array(
				 'SalesVisitationID' =>  $data->id,
				 'Token' => $this->session->userdata('token')
    );
		// if($data->QuotationID != '') $param['QuotationID'] = $data->QuotationID;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_quotation', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function actionDetail($id){
		$data = $this->rest->post();
		// print_r($data->Status);
		// die();
    $param = array(
				 'QuotationID' =>  $id,
				 'Status' => $data->Status,
				 'LicenseID' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->LicenseID),
				 'HardwareID' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->HardwareID),
				 'Price' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->Price),
				 'ResellerPrice' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->ResPrice),
				 'Qty' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->Qty),
				 'CommissionPercentage' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->ComPer),
				 'CommissionValue' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->ComVal),
				 'DiscountID' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->DiscountID),
				 'Percentage' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->DiscPer),
				 'Value' =>  array_map(function($value) { return $value === "" ? NULL : $value; }, $data->DiscVal),
				 'Token' => $this->session->userdata('token')
    );

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_quotation_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_quotation_detail', false, $context);

		$data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];
		$data["Userid"] = $this->session->userdata('user')[0]["UserID"];
		// print_r($data);
		// die();
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getDetailData($id){
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_quotation_detail_by_quotation_id', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function actionStatus($id){
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $id,
				 'Status' =>  $data->Status,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'update_quotation_status', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function print_receipt($id){
	        ini_set('memory_limit', '7000M');
	        ini_set('memory_limit', '-1');
	        set_time_limit(0);
	        $data = $this->getDetailData($id, 0);
	         $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetCreator(PDF_CREATOR);
	//        $fontname = $pdf->addTTFfont('../third_party/tcpdf/fonts/open-sans/-Regular.ttf', 'TrueTypeUnicode','', 32);
	        $title = 'test';
	        $pdf->setPrintHeader(false);
	        $pdf->SetTitle(false);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->SetFont('', '', 10);
	        $pdf->AddPage();
	        $pdf->setImageScale(3.3);
	        $pdf->Image(dirname(__FILE__,3).'/assets/images/logogram.png', 13,6,0,0, 'PNG', '', '', true, 150, '', false, false, false, false, false, false);
	        $pdf->SetFont('b', '', 9);
	        $pdf->Cell(45,0, '',0,0,'L');

	        $pdf->Cell(70,0, 'PT NOVA TEKNOLOGI AWANI',0,0,'L');
	        $pdf->Ln(4);
	        $pdf->SetFont('', '', 9);
	        $pdf->Cell(45,0, '',0,0,'L');
	        $pdf->Cell(70,0, 'APL Tower 16th Floor T9, Jl. Let Jend S. Parman Kav. 28',0,0,'L');
	        $pdf->Ln(4);
	        $pdf->Cell(45,0, '',0,0,'L');
	        $pdf->Cell(70,0, 'Jakarta Barat 11470',0,0,'L');
	        $pdf->Ln(4);
	        $pdf->Cell(45,0, '',0,0,'L');
	        $pdf->Cell(70,0, 'Phone : 021-5688248 / 0852-1010-2455',0,0,'L');
	        $pdf->Ln(4);
	        $pdf->Cell(45,0, '',0,0,'L');
	        $pdf->Cell(70,0, 'Website : www.hellobill.id',0,0,'L');
	        $pdf->Ln(7);
	        $pdf->SetLineStyle(array('width' => 0));
	        $pdf->SetFillColor(96,193,173);
	        $pdf->MultiCell(190, 4, '', 0, 'C', 1, 0);
	        $pdf->Ln(5);
	        $pdf->SetFont('b', '', 15);
	        $pdf->Cell(0,0, 'RECEIPT',0,0,'C');
	        $pdf->Line(94, 45, 116, 45);
	        $pdf->Ln(12);
	        $item = $data->Data;
	        for($i = 0; $i < 2; $i++){
	            $this->rect($pdf,10,200,49,102); // x1 x2 y1 y2
	            $pdf->Line(10, 81, 200, 81);
	            $pdf->Line(56, 54, 195, 54); // underline received from name
	            $pdf->Line(56, 63, 195, 63); // underline received from name
	            $pdf->Line(56, 72, 195, 72); // underline amount 1
	            $pdf->Line(56, 77, 195, 77); // underline amount 2
	            $pdf->Line(56, 87, 195, 87); // underline payment for 1
	            $pdf->Line(56, 92, 195, 92); // underline payment for 2
	            $pdf->Line(56, 96, 195, 96); // underline payment for 3
	        }

	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(35,0, 'No. Receipt',0,0,'L');
	        $pdf->Cell(10,0, ' : ',0,0,'L');
	        $pdf->Cell(150,0, $item->Invoice->ReceiptNumber,0,0,'L');
	        $pdf->Line(11, 54, 40, 54);
	        $pdf->Ln(4);
	        $pdf->SetFont('i', '', 10);
	        $pdf->Cell(35,0, 'Receipt Number',0,0,'L');
	        $pdf->Ln(5);

	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(35,0, 'Telah terima Dari',0,0,'L');
	        $pdf->Cell(10,0, ' : ',0,0,'L');
	        $pdf->Cell(150,0, $item->Invoice->To,0,0,'L');
	        $pdf->Line(11, 63, 40, 63);
	        $pdf->Ln(4);
	        $pdf->SetFont('i', '', 10);
	        $pdf->Cell(35,0, 'Received From',0,0,'L');
	        $pdf->Ln(5);


	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(35,0, 'Uang Sejumlah',0,0,'L');
	        $pdf->Cell(10,0, ' : ',0,0,'L');
	        $value = $this->spelledOut($item->Invoice->TotalInvoice,'3');
	        $this->rectHTML($pdf,55,68,142,12,$value);

	        $pdf->Line(11, 72, 36, 72);
	        $pdf->Ln(4);
	        $pdf->SetFont('i', '', 10);
	        $pdf->Cell(35,0, 'Amount',0,0,'L');
	        $pdf->Ln(11);

	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(35,0, 'Untuk Pembayaran',0,0,'L');
	        $pdf->Cell(10,0, ' : ',0,0,'L');
	        $value = 'Invoice No '.$item->Invoice->InvoiceNumber;
	        $this->rectHTML($pdf,55,83,142,18,$value);

	        $pdf->Line(11, 87, 43, 87);
	        $pdf->Ln(4);
	        $pdf->SetFont('i', '', 10);
	        $pdf->Cell(35,0, 'For Payment',0,0,'L');
	        $pdf->Ln(18);
	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(127,0, '',0,0,'R');
	        $date = date_create($item->Approval->ApprovalDate);
	        $pdf->Cell(55,0, 'Jakarta, '.date_format($date,"d M Y"),0,0,'C');
	        $pdf->Ln(5);
	        $pdf->setImageScale(2.2);
	        $pdf->Image(dirname(__FILE__,3).'/assets/images/line.png', 10,107,0,0, 'PNG', '', '', true, 150, '', false, false, false, false, false, false);
	        $pdf->Image(dirname(__FILE__,3).'/assets/images/line.png', 10,119,0,0, 'PNG', '', '', true, 150, '', false, false, false, false, false, false);

	        $pdf->SetFont('b', '', 17);
	        $pdf->Cell(25,0, 'Rp.',0,0,'L');
	        $pdf->SetFont('b', '', 16);
	        $pdf->Cell(35,1, number_format ($item->Invoice->TotalInvoice, 0 ),0,0,'C');
	        $x1 = 32; $x2 = 81; $y = 109;
	        for($i = 0; $i < 8 ; $i++){
	            $x1 -= 1; $x2 -= 1; $y += 1;
	            $pdf->Line($x1, $y, $x2, $y); // underline nominal price
	        }

	        $pdf->Ln(15);
	        $pdf->SetFont('b', '', 9);
	        $pdf->Cell(55,0, 'Catatan:',0,0,'L');
	        $pdf->Ln(5);
	        $pdf->SetFont('', '', 8);
	        $pdf->Cell(0,0, '1. Kwitansi ini dianggap sah setelah pembayaran dengan',0,0,'L');
	        $pdf->Ln(5);
	        $pdf->Cell(3,0, '',0,0,'L');
	        $pdf->Cell(0,0, 'Bilyet Giro/Cek dicairkan',0,0,'L');
	        $pdf->Ln(5);
	        $pdf->Cell(128,0, '2. Hardware yang sudah dibeli tidak dapat ditukar atau',0,0,'L');
	        $pdf->SetFont('', '', 10);
	        $pdf->Cell(55,0, 'Dian Surya',0,0,'C');
	        $pdf->SetFont('', '', 8);
	        $pdf->Ln(5);
	        $pdf->Cell(3,0, '',0,0,'L');
	        $pdf->Cell(125,0, 'dikembalikan',0,0,'L');
	        $pdf->SetFont('b', '', 10);
	        $pdf->Cell(55,0, 'Finance',0,0,'C');
	        $pdf->Ln(5);

	        $pdf->lastPage();
	        ob_end_clean();
	        $pdf->Output('Receipt '.$item->Invoice->InvoiceNumber.".pdf");
	    }


	// function deleteUpdateData($id){
	// 	$data = $this->rest->post();
  //   $param = array(
	// 			 'QuotationID' =>  $id,
  //        'Token' =>  $this->session->userdata('token')
  //    );
  //   $variabel = json_encode($param);
	// 	// print_r($variabel);
	// 	// die();
  //   $opts = array('http' =>
  //         array(
  //             'method'  => 'POST',
  //             'header'  => 'Content-type: application/json',
  //             'content' => $variabel
  //         )
  //   );
  //   $context  = stream_context_create($opts);
  //   $data = file_get_contents(SERVICE_URL.'delete_quotation', false, $context);
	// 	// print_r($data);
	// 	// die();
  //   $data = json_decode($data, true);
	//
  //   return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  // }

}
