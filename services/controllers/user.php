<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_user', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function action(){
		$data = $this->rest->post();
    $param = array(
         'UserCode' =>  $data->optiondata->data->UserCode,
         'UserFullName' => $data->optiondata->data->UserFullName,
				 'UserTypeID' =>  $data->optiondata->data->UserTypeID,
         'Email' => $data->optiondata->data->Email,
				 'Phone' =>  $data->optiondata->data->Phone,
				 'Address' => $data->optiondata->data->Address,
         'Username' => $data->optiondata->data->Username,
				 'Password' => $data->optiondata->data->Password,
				 'Token' => $this->session->userdata('token')
    );
		if(isset($data->optiondata->id)) $param['UserID'] = $data->optiondata->id;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_user', false, $context);
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'UserID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_user_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'UserID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_user', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


}
