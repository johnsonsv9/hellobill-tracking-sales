<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }

	function getCommissionProfit($period){
		$data = $this->rest->post();
    $param = array(
				 'StartDate' => substr($period, 0, 10),
				 'EndDate' => substr($period, 10),
				 'UserID' => $this->session->userdata('user')[0]['UserID'],
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_commission_profit', false, $context);
    $data = json_decode($data, true);
		$data["UserTypeName"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

  function getTransaction($period){
		$data = $this->rest->post();
    $param = array(
				 'StartDate' => substr($period, 0, 10),
				 'EndDate' => substr($period, 10),
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_transaction', false, $context);
    $data = json_decode($data, true);
		$data['UserID'] = $this->session->userdata('user')[0]["UserID"];
		$data['UserTypeName'] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){
		$data = $this->rest->post();
    $param = array(
				 'ProductID' =>  $data->optiondata->data->ProductID,
         'BrandCode' =>  $data->optiondata->data->BrandCode,
         'BrandName' => $data->optiondata->data->BrandName,
				 // 'Images' => $data->optiondata->files,
				 // 'Data' => $data->Base64,
				 // 'Filename' => $data->FileName,
				 'Token' => $this->session->userdata('token')
    );
		$param['Images']['Data'] = $data->optiondata->files[0]->Data;
		// print_r($param['Images']['Data']);
		$param['Images']['Filename'] = $data->optiondata->files[0]->Filename;
		// print_r($data->optiondata->files);
		if($data->optiondata->data->BrandID != '') $param['BrandID'] = $data->optiondata->data->BrandID;

    $variabel = json_encode($param);
		print_r($variabel);
		die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_brand', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'BrandID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_brand_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'BrandID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_brand', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


}
