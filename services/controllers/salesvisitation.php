<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class salesvisitation extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_sales_visitation', false, $context);
    $data = json_decode($data, true);
		$data["UserID"] = $this->session->userdata('user')[0]["UserID"];
		$data["UserTypeName"] = $this->session->userdata('user')[0]["UserTypeName"];
		$data["Permissions"] = $this->session->userdata('permission')[0];
		// print_r($data);
		// die();

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){
		$data = $this->rest->post();
    $param = array(
         'SalesVisitationNO' =>  $data->SalesVisitationNO,
         'BranchID' => $data->BranchID,
				 'UserID' => $data->UserID,
				 'LastStatus' => $data->LastStatus,
				 'BriefDescription' => $data->BriefDescription,
				 'GPS' => $data->GPS,
				 'Token' => $this->session->userdata('token')
    );
		if($data->SalesVisitationID != '') $param['SalesVisitationID'] = $data->SalesVisitationID;
		if($data->StartDate != '') $param['StartDate'] = $data->StartDate;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_sales_visitation', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'SalesVisitationID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_sales_visitation_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["UserID"] = $this->session->userdata('user')[0]["UserID"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'BrandID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_sales_visitation', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


}
