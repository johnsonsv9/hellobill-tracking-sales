<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class invoicereseller extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token'),
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_reseller', false, $context);
    $data = json_decode($data, true);
		$data[0] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function actionPayment($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
				 'Date' =>  $data->Date,
				 'Paid' =>  $data->Amount,
				 // 'TransferProof' =>  $data->TrfProof,
				 'Token' => $this->session->userdata('token')
    );
		// if($data->InvoicePaymentID != '') $param['InvoicePaymentID'] = $data->InvoicePaymentID;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_invoice_payment', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $data->id,
				 'Token' => $this->session->userdata('token')
    );

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_invoice_reseller', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

		function actionReseller(){
			$data = $this->rest->post();
	    $param = array(
					 'QuotationID' =>  $data->id,
					 'Token' => $this->session->userdata('token')
	    );

	    $variabel = json_encode($param);
			// print_r($variabel);
			// die();
	    $opts = array('http' =>
	          array(
	              'method'  => 'POST',
	              'header'  => 'Content-type: application/json',
	              'content' => $variabel
	          )
	    );
	    $context  = stream_context_create($opts);
	    $data = file_get_contents(SERVICE_URL.'insert_update_invoice', false, $context);
			// print_r($data);
			// die();
	    $data = json_decode($data, true);
	    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
	  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getDetailData($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_payment_by_invoice_id', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateDetailData($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoicePaymentID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_invoice_payment_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		$data["Usertype"] = $this->session->userdata('user')[0]["UserTypeName"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function actionStatus($id){
		$data = $this->rest->post();
    $param = array(
				 'InvoiceID' =>  $id,
				 'Status' =>  $data->Status,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'update_invoice_status', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'QuotationID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_invoice', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
