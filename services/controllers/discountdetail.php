<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class discountdetail extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData($id){
		$data = $this->rest->post();
    $param = array(
				 'DiscountID' => $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount_detail_by_discount_id', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){
		$data = $this->rest->post();

    $param = array(
				 'DiscountID' => $this->session->userdata('discountid'),
				 'Token' => $this->session->userdata('token')
    );
		if($data->hardware_id != "") $param["HardwareID"] = $data->hardware_id;
		if($data->license_id != "") $param["LicenseID"] = $data->license_id;
		if($data->id != "") $param["DiscountDetailID"] = $data->id;

    $variabel = json_encode($param);
		print_r($variabel);
		die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_discount_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		// if($data->Status == 0){
		// 	$sess["discountid"] = $data["DiscountID"];
		// 	$this->session->set_userdata($sess);
		// nanti $this->session->set_userdata('discountid') bakal dioper ke discountdetail action
		// }
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'DiscountDetailID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_discount_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'DiscountDetailID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_discount_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
