<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function loginWeb(){
		$data = $this->rest->post();
    $param = array(
         'EmailUsername' =>  $data->username,
         'Password' => $data->password
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'login', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
		if(isset($data)){
			if($data->Status == 0){
				if(isset($data["Token"]) && isset($data["UserData"]) && isset($data["Permission"])){
					$sess["token"] = $data["Token"];
					$this->session->set_userdata($sess);
					$sess["user"] = $data["UserData"];
					$this->session->set_userdata($sess);
					$sess["permission"] = $data["Permission"];
					$this->session->set_userdata($sess);
				}
			}
		}
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function logout() {
		$this->session->sess_destroy();
		$status["IsLogin"] = "0";
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $status)));
	}

	function changePassword(){
		$data = $this->rest->post();
		// print_r($data);
		// die();
	$param = array(
		 'OldPassword' =>  $data->data->OldPassword,
		 'NewPassword' => $data->data->NewPassword,
				 'Token' => $this->session->userdata('token')
	);

	$variabel = json_encode($param);
		// print_r($variabel);
		// die();
	$opts = array('http' =>
		  array(
			  'method'  => 'POST',
			  'header'  => 'Content-type: application/json',
			  'content' => $variabel
		  )
	);
	$context  = stream_context_create($opts);
	$data = file_get_contents(SERVICE_URL.'change_password', false, $context);
		// print_r($data);
		// die();
	$data = json_decode($data, true);
	return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


}
