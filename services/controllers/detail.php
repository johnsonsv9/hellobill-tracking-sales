<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class detail extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData($id){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token'),
				 'SalesVisitationID' => $id
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_sales_visitation_detail_by_sales_id', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token'),
				 'SalesVisitationID' => $id
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_sales_visitation_detail', false, $context);
    $data = json_decode($data, true);
		$data["UserID"] = $this->session->userdata('user')[0]["UserID"];
		// $data["0"] = $this->session->userdata('user')[0]["UserID"];

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action($salesvisitation_id){
		$data = $this->rest->post();
    $param = array(
				 'SalesVisitationID' =>  $salesvisitation_id,
				 'Status' => $data->StatusTerpilih,
				 'Description' => $data->Description,
				 'Token' => $this->session->userdata('token')
    );
		if($data->SalesVisitationDetailID != '') $param['SalesVisitationDetailID'] = $data->SalesVisitationDetailID;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_sales_visitation_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateDetailData($id){
		$data = $this->rest->post();
    $param = array(
				 'SalesVisitationDetailID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_sales_visitation_detail_by_id', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'BranchID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
