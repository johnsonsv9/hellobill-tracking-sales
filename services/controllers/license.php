<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class license extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
    );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_license', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function action(){
		$data = $this->rest->post();
    $param = array(
         'LicenseCode' =>  $data->optiondata->data->LicenseCode,
         'LicenseName' => $data->optiondata->data->LicenseName,
				 'ProductID' => $data->optiondata->data->ProductID,
				 'Price' => $data->optiondata->data->Price,
				 'ResellerPrice' => $data->optiondata->data->ResPrice,
				 'Duration' => $data->optiondata->data->Duration,
				 'Token' => $this->session->userdata('token')
    );
		if(isset($data->optiondata->id)) $param['LicenseID'] = $data->optiondata->id;
		if($data->optiondata->data->Percentage != "") $param["CommissionPercentage"] = $data->optiondata->data->Percentage;
		else if($data->optiondata->data->Value != "") $param["CommissionValue"] = $data->optiondata->data->Value;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_license', false, $context);
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'LicenseID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_license_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'LicenseID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_license', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

}
