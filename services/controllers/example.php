<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class example extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }
    
     
  function getValidSort(){
    $param = array(
         'Token' =>  $this->session->userdata('Token'),
         'METAONLY' => 'true'
     );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'/user/get_shift', false, $context);
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
      
  }

  function getAllData(){
        $draw = $_POST['draw'];
        $orderByColumnIndex  = $_POST['order'][0]['column'];
        $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];
        $orderDir = $_POST['order'][0]['dir'];
        $orderDir = strtoupper($orderDir);
        $start  = $_POST["start"];
        $length = $_POST['length'];
        $search = $_POST['search']['value'];
        $param = array(
             'Token' =>  $this->session->userdata('Token'),
             'Draw' => $draw,
             'OrderBy' => $orderBy,
             'OrderDirection' => $orderDir,
             'SearchQuery' => $search,
             'PageSize' => $length,
             'StartFrom' => $start,
             'SearchQuery' => $search

         );
         $variabel = json_encode($param);
         $opts = array('http' =>
              array(
                  'method'  => 'POST',
		          'header'  => 'Content-type: application/json',
		          'content' => $variabel
              )
        );
		$context  = stream_context_create($opts);
		$data = file_get_contents(SERVICE_URL.'/user/get_shift', false, $context);
        $data = json_decode($data, true);
        $length = count($data['data']);  
        for($i = 0; $i < $length; $i++){
            $data['data'][$i]['Action'] = '<a href="#/user/shift/action-shift.'.$data['data'][$i]['ShiftID'].'" class="editbutton edit fa fa-edit" >&nbsp;Edit&nbsp;&nbsp;</a>';
            $data['data'][$i]['Action'] = $data['data'][$i]['Action'] . '<a class="deletebutton delete icon-trash" attr-id="'.$data['data'][$i]['ShiftID'].'" value="">&nbsp;Delete</a>';

        }
      /*
        foreach ($data['Data'] as &$d) {
          if($d['Image'] != "") {
            if (strpos($d['Image'],'default') !== false) {
              $d['Image'] = "http://hellobill.co.id/core/services/assets/images/default/".$d['Image'];
            } else {
              $d['Image'] = $this->get_file_path($d->RestaurantID, 'food_category_image_path', true).$d['Image'];
            }

          } else {
            $d['Image'] = "http://hellobill.co.id/core/services/assets/images/default/default-square.png";
          }
        }
*/
        return $this->load->view('json_view', array('json' => $data));
  }
  function getOptionPermission(){
    $data = array();
    $data['data'] =  $this->db->query('select* from "Permission"')->result();
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }
  function getUpdateData($ShiftID){
        $param = array(
             'Token' =>  $this->session->userdata('Token'),
             'ShiftID' => $ShiftID

         );
         $variabel = json_encode($param);
         $opts = array('http' =>
              array(
                  'method'  => 'POST',
		          'header'  => 'Content-type: application/json',
		          'content' => $variabel
              )
        );
		$context  = stream_context_create($opts);
		$data = file_get_contents(SERVICE_URL.'/user/get_shift', false, $context);
        $data = json_decode($data, true);
        return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }
    
  function action(){
    $data = $this->rest->post();
    $msg = NULL;
      
      if(!$data->data->ShiftID){
            $status = "Insert Success";
            $id = '';
        }
        else{
            $status = "Update Success";
            $id = '"ShiftID": '.$data->data->ShiftID.',';
        }
        
        $postdata = '{
	        "Token":  "'.$this->session->userdata('Token').'",'
            .$id.'
            "ShiftName":  "'.$data->data->ShiftName.'",
            "From":  "'.$data->data->FromHour.'",
            "To":  "'.$data->data->ToHour.'"
		    }';
        
		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/json',
		        'content' => $postdata
		    )
		);
		$context  = stream_context_create($opts);
		$result = file_get_contents(SERVICE_URL.'/user/save_shift', false, $context);
		$result = json_decode($result);
        return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => array('data' => $result, 'Messages'=> $status))));
 }

 function deletedata(){
        $data = $this->rest->post();
        $postdata = '{
          "Token": "'.$this->session->userdata('Token').'",
	      "ShiftID": "'.$data->id.'"
		}';

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/json',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts);
		$result = file_get_contents(SERVICE_URL.'/user/delete_shift', false, $context);
        $result = json_decode($result);
        $result = array('data' => $result, 'status' => 'success');
		return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => array('data' => $result))));
}
    
    
    function num2alpha($n){
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 0x41) . $r;
        return $r;
    }
    
    public function export_excel() {
        $xls = new PHPExcel();
        $sheet = $xls->getActiveSheet();
        $this->initBody($xls, $sheet);
    }
    public function generateAutoSize($sheet) {
        $sheet->getColumnDimension("A")->setAutoSize(true);
        $sheet->getStyle("A1")->getFont()->setBold(true);
        $sheet->getColumnDimension("B")->setAutoSize(true);
        $sheet->getStyle("B1")->getFont()->setBold(true);
        $sheet->getColumnDimension("C")->setAutoSize(true);
        $sheet->getStyle("C1")->getFont()->setBold(true);

    }
    public function initBody($xls, $sheet) {
        
        $param = array(
             'Token' =>  $this->session->userdata('Token'),
             'METAONLY' => 'true'
         );
        $variabel = json_encode($param);
        $opts = array('http' =>
              array(
                  'method'  => 'POST',
                  'header'  => 'Content-type: application/json',
                  'content' => $variabel
              )
        );
        $context  = stream_context_create($opts);
        $data = file_get_contents(SERVICE_URL.'/user/get_shift', false, $context);
        $data = json_decode($data);
        $count = $data->DataCount;
        if($count == 0)
        $count = 1;
        $param = array(
             'Token' =>  $this->session->userdata('Token'),
             'PageSize' => $count

         );
         $variabel = json_encode($param);
         $opts = array('http' =>
              array(
                  'method'  => 'POST',
		          'header'  => 'Content-type: application/json',
		          'content' => $variabel
              )
        );
		$context  = stream_context_create($opts);
		$item = file_get_contents(SERVICE_URL.'/user/get_shift', false, $context);
        $item = json_decode($item);
        
        $HeaderName = "Shift List";
        $sheet->setCellValue('A1', 'Shift Name');
        $sheet->setCellValue('B1', 'From Hour');
        $sheet->setCellValue('C1', 'To Hour');
        
        $this->generateAutoSize($sheet, 'A', 'B', 'C');
        $row = 2;
        for($i = 0; $i < $data->DataCount; $i++){
            $d = $item->data[$i];
            $sheet->setCellValueByColumnAndRow(0, $row, $d->ShiftName);
            $sheet->setCellValueByColumnAndRow(1, $row, $d->From);
            $sheet->setCellValueByColumnAndRow(2, $row, $d->To);
            $row++;
            }
        $maxColoum = 2;
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    ),
            ),
        );
        $sheet->getStyle('A1:'.$this->num2alpha($maxColoum).($row-1))->applyFromArray($styleArray);
        
           $objPHPExcel = new PHPExcel_Writer_Excel5($xls);
           $filename = $HeaderName." Report ".".xls";
           header('Content-Type: application/xls');
           header('Content-Disposition: attachment;filename="'.$filename.'"');
           header('Cache-Control: max-age=0');
           $objPHPExcel->save('php://output');
        
    }
    
}