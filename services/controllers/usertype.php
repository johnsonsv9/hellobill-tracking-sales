<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class usertype extends BM_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('rest');
		$this->load->library('Validation');
		$this->load->helper('general');
		$this->config->load('apps');
        $this->load->library('Excel');
        $this->load->model('basic_model');
  }


  function getAllData(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_user_type', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function action(){
		$data = $this->rest->post();
		$data->optiondata->data->PIC = $data->optiondata->data->PIC === "1" ? true : false;
		$data->optiondata->data->PassedPIC = $data->optiondata->data->PassedPIC === "1" ? true : false;
		// print_r($data);
		// die();
    $param = array(
         'UserTypeCode' =>  $data->optiondata->data->UserTypeCode,
         'UserTypeName' => $data->optiondata->data->UserTypeName,
				 'Description' => $data->optiondata->data->Description,
				 'PermissionID' => $data->optiondata->data->Permission,
				 'PIC' =>  $data->optiondata->data->PIC,
				 'PassedPIC' => $data->optiondata->data->PIC === false ? false : $data->optiondata->data->PassedPIC,
				 'RevenueType' =>  $data->optiondata->data->Revenue,
				 'Token' => $this->session->userdata('token')
    );
		if(isset($data->optiondata->id)) $param["UserTypeID"] = $data->optiondata->id;

    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'insert_update_user_type', false, $context);
    $data = json_decode($data, true);
    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


	function getUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'UserTypeID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_user_type_detail', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

	function deleteUpdateData($id){
		$data = $this->rest->post();
    $param = array(
				 'UserTypeID' =>  $id,
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
		// print_r($variabel);
		// die();
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'delete_user_type', false, $context);
		// print_r($data);
		// die();
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }

  function getPermission(){
		$data = $this->rest->post();
    $param = array(
         'Token' =>  $this->session->userdata('token')
     );
    $variabel = json_encode($param);
    $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/json',
              'content' => $variabel
          )
    );
    $context  = stream_context_create($opts);
    $data = file_get_contents(SERVICE_URL.'get_permission', false, $context);
    $data = json_decode($data, true);

    return $this->load->view('json_view', array('json' => array('status' => 'success', 'data' => $data)));
  }


}
