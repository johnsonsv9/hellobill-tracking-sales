<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BM_Controller extends CI_Controller {
	public function __construct($use_session = true)
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('rest');
		$this->load->helper('general');

		// if($use_session)
		// {
			// $username = $this->session->userdata('username');
			// if(!$username)
			// {
				// $this->denyRequest();
			// }
		// }
	}


	public function dbdate($date, $format = array('format' => 'd-m-y', 'separator' => '-', 'default' => '0')){
            // echo "haha";
		if(!is_array($format))
		{
			return;
		}

        // fix here: try to look for the default value if the date string is zero length
		if(!$date)
		{
			return $format['default'];
		}
		$preformated = explode($format['separator'], $date);
		if(count($preformated) != 3)
		{
			return;
		}
		$formatpos = explode($format['separator'], $format['format']);
		$form = array(0, 0, 0);
		foreach($formatpos as $i => $type)
		{
			switch($type)
			{
				case 'd':
				case 'dd':
				$form[2] = intval($preformated[$i]);
				break;
				case 'm':
				case 'mm':
				$form[1] = intval($preformated[$i]);
				break;
				case 'y':
				case 'yy':
				case 'yyyy':
				$form[0] = intval($preformated[$i]);
				break;
			}
		}
		$day = $form[2];
		if ($day < 10) {
			$day = "0".$day;
		}
		$month = $form[1];
		if ($month < 10) {
			$month = "0".$month;
		}
		$result = $form[0]."-".$month."-".$day;
		return $result;
	}

	public function denyRequest() {
		header("Content-Type: application/json;charset=utf-8", true, 401);
		echo '{"status":"400","errno":"1","error":"No valid sessions found. Unathorized access."}';
		exit;
	}
	public function std_to_json(&$array, $array_column){
		foreach ($array_column as $d) {
			foreach ($array as &$dd) {
				$dd->{$d} = json_decode($dd->{$d});
			}
		}
	}
	public function single_std_to_json(&$std, $array_column){
		foreach ($array_column as $d) {
			$std->{$d} = json_decode($std->{$d});
		}
	}
  public function get_file_path($restaurant_id, $pathname, $physical = true){
    $foldername = $restaurant_id.'-'.$this->db->query('Select "Folder" from "RestaurantMaster" where "RestaurantID" = ? ', array($restaurant_id))->row()->Folder;

    if(!$physical){
      $base_uri = '/home/core/public_html/services/assets/';
    }else{
      $base_uri = "http://hellobill.co.id/core/services/assets/";
    }
    if($base_uri != "") {
      $data["restaurant_logo_path"] = $base_uri."images/restaurants/$foldername/logo/";
      $data["table_image_path"] = $base_uri."images/tables/";
      $data["section_bg_image_path"] = $base_uri."images/restaurants/$foldername/section/floor-bg/";
      $data["food_category_image_path"] = $base_uri."images/restaurants/$foldername/food-category/";
      $data["food_menu_image_path"] = $base_uri."images/restaurants/$foldername/food-menu/";
      $data["food_deal_image_path"] = $base_uri."images/restaurants/$foldername/food-deal/";
      $data["image_promo_path"] = $base_uri."images/restaurants/$foldername/promotion/";
      return $data[$pathname];
    } else {
      return "";
    }
  }

  public function upload_file($restaurant_id, $for, $data){
    $filename = time().'.'.$data->name;
    $path = ($this->get_file_path($restaurant_id, $for, false));
    $target_file = ($this->get_file_path($restaurant_id, $for, false)).''.$filename;
    $data64 = base64_decode($data->data);
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $myfile = fopen($target_file, "w");
    file_put_contents($target_file, $data64);

    return $filename;
  }
  public function sp($sp_name, $array = array())
  {
    $param_name = array();
    $param_list = array();
    foreach($array as $key => $val)
    {
     $param_name[] = '@' . $key . '=?';
     $param_list[] = $val;
   }

   $sp_name .= ' '. implode(', ', $param_name);

   return $this->db->query($sp_name, $param_list);
 }

 public function isSpError($returned)	{
  $result = $returned->row();
  if(isset($result->status) && $result->status == 'error' && isset($result->errno) && isset($result->error))
  {
   return true;
 }

 return false;

		/**
		//// USAGE EXAMPLE
		$stmt = $this->sp('blablablal');
		if($this->isSpError($stmt))
		{
			$error = $stmt->row();
			$error->status;
			echo $error->error;
			$error->errno;
			return;
		}

		// proses datanya
		return $this->load->view('blablabla');
		**/
	}
}

/* End of file BM_Controller.php */