<?php
class Basic_model extends CI_MODEL

{
    function __construct()
    {
        parent::__construct();
    }
    
    public function uploadData($files, $filename, $validation){
        if ($validation == FALSE) {
            $txt = implode(', ', $this->validation->error_array());
            $status = 'error';
        } 
        else {
            $upload_url = $this->config->item('upload_url');
            $dir = $upload_url['import_menu'];
            $decodeddata = base64_decode($files->content);
            $size = $files->size;
            $file = $files->filename;
            $ext = explode('.', $file); //get extantion
            $ext = array_pop($ext);
            $allowedExts = array("doc", "docx", "txt", "xls", "xlsx"); // ext nya
            if(in_array($ext, $allowedExts)) {
                $newfilename = $this->session->userdata('AccountID').'_'.trim($filename," ").'_'.date("Y-m-d-H-i-s").'.'.$ext; //NAMA FILENYA
                if (!is_dir($dir)) {
                    mkdir($dir); //buat directory baru
                }
                $uploadDir = realpath($dir);
                file_put_contents($uploadDir . '/' . $newfilename, $decodeddata);
            } 
            else {
                $success = 'File anda berekstensi ' . $ext . ', yang boleh cuma ' . implode(', ', $allowedExts);
                return $this->basic_model->returnDataAction(1, 'Import Success', $suc);
            }
            $this->load->library('Excel');
            $objPHPExcel = PHPExcel_IOFactory::Load($dir . '/' . $newfilename);
            $count = $objPHPExcel->getActiveSheet()->getHighestRow() - 1; 
            $data = $this->input->get();
            $param = array(  
                'filename'  => $newfilename,
                'base64' => $files->content,
                'size' => $size
            );
            $setting = json_encode($param);
            $opts = array('http' =>
                          array(
                              'method'  => 'POST',
                              'header'  => 'Content-type: application/json',
                              'content' => $setting
                          )
                         );
            $context  = stream_context_create($opts);
            $result = file_get_contents(SERVICE_URL_IMPORT.'import.php', false, $context);
            $resultphparray = (array)json_decode($result);
            $resultphparray["RealFileName"] = $filename;
            $resultphparray["Count"] = $count;
            return $resultphparray;
        }
    }
    public function prepareImport($phparray, $startFrom, $importfor){
        $param = array(  
            'Token'  => $this->session->userdata('Token'),
            'Filename' => $phparray["Filename"],
            'FullPath' => $phparray["Path"],
            'DownloadPath' => "",
            'RealFileName' => $phparray["RealFileName"],
            'Size' => $phparray["Size"],
            'StartFrom' => $startFrom,
            'TotalRow' => $phparray["Count"],
            'ImportFor' => $importfor
        );
        $setting = json_encode($param);
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $setting
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents(SERVICE_URL . "/restaurant-w3/import/prepare_import", false, $context);
        return $result;
    }

    public function checkUniqueKeyInsertBranch($tabel, $coloum1, $coloum2, $id1, $id2)
    {
        return ($this->db->query('SELECT * FROM "' . $tabel . '" WHERE "' . $coloum1 . '" = ? AND "' . $coloum2 . '" =?  ', array(
            $id1,
            $id2
        ))->num_rows());
    }

    public function checkUniqueKeyUpdateBranch($tabel, $coloum, $id, $coloum2, $id2, $coloum3, $id3)
    {
        return ($this->db->query('SELECT * FROM "' . $tabel . '" WHERE "' . $coloum . '" = ? AND "' . $coloum2 . '" NOT IN (?) AND "' . $coloum3 . '" = ?', array(
            $id,
            $id2,
            $id3
        ))->num_rows());
    }

    public function checkUniqeKeyInsert($tabel, $coloum, $id)
    {
        return ($this->db->query('SELECT * FROM "' . $tabel . '" WHERE "' . $coloum . '" = ?', array(
            $id
        ))->num_rows());
    }

    public function checkUniqeKeyUpdate($tabel, $coloum, $id, $coloum2, $id2)
    {
        return ($this->db->query('SELECT * FROM "' . $tabel . '" WHERE "' . $coloum . '" = ? AND "' . $coloum2 . '" NOT IN (?)', array(
            $id,
            $id2
        ))->num_rows());
    }

    public function getalldata($query, $array = null)
    {
        $query = $this->db->query($query, $array);
        return $query->result();
    }

    function toNumber($num)
    {
        $am = '';
        $amount = $num;
        $amount = explode(".", $amount);
        $am = substr($amount[0], -3) . $am;
        $strlen = strlen($amount[0]) - 3;
        if ($strlen < 0) {
            $amount[0] = '';
        }
        else {
            $amount[0] = substr($amount[0], 0, strlen($amount[0]) - 3);
        }

        for (; strlen($amount[0]) > 3;) {
            $am = substr($amount[0], -3) . "." . $am;
            $strlen = strlen($amount[0]) - 3;
            $amount[0] = substr($amount[0], 0, strlen($amount[0]) - 3);
        }

        if(strlen($amount[0]) > 0) $am = $amount[0] . "." . $am;
        if(isset($amount[1]) && strlen($amount[1]) > 0)
            if($amount[1] != '00')
                $am = $am . ',' . $amount[1];
        return $am;
    }

    public function getalldatarow($query, $array = null)
    {
        $query = $this->db->query($query, $array);
        return $query->row();
    }

    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);

        // return "".$this->db->insert_id();

        return "success";
    }

    public function insert_query($table, $array)
    {
        $CI = get_instance();
        $collist = "(";
        $questionlist = "(";
        $ct = 0;
        $vallist = array();
        foreach($array as $key => $value) {
            if ($ct != (count((array)$array) - 1)) {
                $collist = $collist . " \"" . $key . "\",";
                $questionlist = $questionlist . " ?,";
            }
            else {
                $collist = $collist . " \"" . $key . "\")";
                $questionlist = $questionlist . " ?)";
            }

            $vallist[] = $value;
            $ct++;
        }

        $query = "insert into \"" . ($table) . "\" " . $collist . " values " . $questionlist;
        $CI->db->query($query, $vallist);
        return $CI->db->query("SELECT lastval() id")->row()->id;
    }

    public function update($table_name, $collumn_name, $data, $id)
    {
        $this->db->where($collumn_name, $id);
        $this->db->update($table_name, $data);
        return $id;
    }

    public function delete($table_name, $collumn_name, $id, $data)
    {
        $this->db->where($collumn_name, $id);
        $this->db->update($table_name, $data);
        return "success";
    }

    public function realdelete($table_name, $collumn_name, $id)
    {
        $this->db->where($collumn_name, $id);
        $this->db->delete($table_name);
        return "success";
    }

    public function common_collumn($stsrc, $action)
    {
        if ($action == "I") {
            $datasend = array(
                'stsrc' => $stsrc,
                'user_create' => $this->session->userdata('user_id') ,
                'date_create' => date("Y-m-d H:i:s") ,
                'user_change' => $this->session->userdata('user_id') ,
                'date_change' => date("Y-m-d H:i:s")
            );
        }
        else {
            $datasend = array(
                'stsrc' => $stsrc,
                'user_change' => $this->session->userdata('user_id') ,
                'date_change' => date("Y-m-d H:i:s")
            );
        }

        return $datasend;
    }

    public function returnDataAction($flag, $msg, $id = "")
    {
        if ($flag == 1) { //success validate
            $this->db->trans_complete();
            $txt = $msg;
            $status = 'success';
            return $this->load->view('json_view', array(
                'json' => array(
                    'status' => 'success',
                    'data' => array(
                        'status' => $status,
                        'msg' => $txt,
                        'id' => $id
                    )
                )
            ));
        }
        else
        if ($flag == 0) { //error validate
            if (is_array($msg)) {
                $txt = $msg;
            }
            else {
                $txt[] = $msg;
            }

            $status = 'error';
            return $this->load->view('json_view', array(
                'json' => array(
                    'status' => 'error',
                    'data' => array(
                        'status' => $status,
                        'errors' => $txt
                    )
                )
            ));
        }
    }

    public function dbdate($date, $format = array(
        'format' => 'd-m-y',
        'separator' => '-',
        'default' => '0'
    ))
    {

        // echo "haha";

        if (!is_array($format)) {
            return;
        }

        // fix here: try to look for the default value if the date string is zero length

        if (!$date) {
            return $format['default'];
        }

        $preformated = explode($format['separator'], $date);
        if (count($preformated) != 3) {
            return;
        }

        $formatpos = explode($format['separator'], $format['format']);
        $form = array(
            0,
            0,
            0
        );
        foreach($formatpos as $i => $type) {
            switch ($type) {
            case 'd':
            case 'dd':
                $form[2] = intval($preformated[$i]);
                break;

            case 'm':
            case 'mm':
                $form[1] = intval($preformated[$i]);
                break;

            case 'y':
            case 'yy':
            case 'yyyy':
                $form[0] = intval($preformated[$i]);
                break;
            }
        }

        $day = $form[2];
        if ($day < 10) {
            $day = "0" . $day;
        }

        $month = $form[1];
        if ($month < 10) {
            $month = "0" . $month;
        }

        $result = $form[0] . "-" . $month . "-" . $day;
        return $result;
    }

    public function convertuserid($FullName, $date, $format = array(
        'format' => 'd-m-y',
        'separator' => '-',
        'default' => '0'
    ))
    {

        // echo "haha";

        if (!is_array($format)) {
            return;
        }

        // fix here: try to look for the default value if the date string is zero length

        if (!$date) {
            return $format['default'];
        }

        $preformated = explode($format['separator'], $date);
        if (count($preformated) != 3) {
            return;
        }

        $formatpos = explode($format['separator'], $format['format']);
        $form = array(
            0,
            0,
            0
        );
        foreach($formatpos as $i => $type) {
            switch ($type) {
            case 'd':
            case 'dd':
                $form[2] = intval($preformated[$i]);
                break;

            case 'm':
            case 'mm':
                $form[1] = intval($preformated[$i]);
                break;

            case 'y':
            case 'yy':
            case 'yyyy':
                $form[0] = intval($preformated[$i]);
                break;
            }
        }

        $FullName = trim(strtoupper(str_replace(" ", "", $FullName)));
        if (strlen($FullName) >= 4) {
            $day = $form[2];
            if ($day < 10) {
                $day = "0" . $day;
            }

            $month = $form[1];
            if ($month < 10) {
                $month = "0" . $month;
            }

            $result = substr($FullName, 0, 4) . "" . $day . "" . $month;
        }
        else {
            $ck = 4 - strlen($FullName);
            for ($i = 0; $i < $ck; $i++) {
                $FullName = $FullName . "O";
            }

            $day = $form[2];
            if ($day < 10) {
                $day = "0" . $day;
            }

            $month = $form[1];
            if ($month < 10) {
                $month = "0" . $month;
            }

            $result = $FullName . "" . $day . "" . $month;
        }

        return $result;
    }
}