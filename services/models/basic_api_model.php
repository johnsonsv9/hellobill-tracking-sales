<?php
class Basic_api_model extends CI_MODEL{

public function getalldata($query, $array = null) {
    $query = $this->db->query($query, $array);
    return $query->result();
}

public function getalldatarow($query, $array = null) {
    $query = $this->db->query($query, $array);
    return $query->row();
}

public function insert($table_name, $data) {
    $this->db->insert($table_name, $data);
    return "success";
}

public function update($table_name, $collumn_name, $data, $id) {
    $this->db->where($collumn_name,$id);
    $this->db->update($table_name, $data);
    return "success";
}

public function delete($table_name, $collumn_name, $id, $data) {
    $this->db->where($collumn_name,$id);
    $this->db->update($table_name, $data);
    return "success";
}

public function realdelete($table_name, $collumn_name, $id) {
    $this->db->where($collumn_name,$id);
    $this->db->delete($table_name);
    return "success";
}

public function common_collumn($stsrc, $action) {
    if($action == "I") {
        $datasend = array(
            'stsrc' => $stsrc,
            'user_create' => $this->session->userdata('user_id'),
            'date_create' => date("Y-m-d H:i:s"),
            'user_change' => $this->session->userdata('user_id'),
            'date_change' => date("Y-m-d H:i:s")
            );
    } else {
        $datasend = array(
            'stsrc' => $stsrc,
            'user_change' => $this->session->userdata('user_id'),
            'date_change' => date("Y-m-d H:i:s")
            );
    }
    return $datasend;
}

public function dbdate($date, $format = array('format' => 'd-m-y', 'separator' => '-', 'default' => '0')){
            // echo "haha";
    if(!is_array($format))
    {
        return;
    }

        // fix here: try to look for the default value if the date string is zero length
    if(!$date)
    {
        return $format['default'];
    }
    $preformated = explode($format['separator'], $date);
    if(count($preformated) != 3)
    {
        return;
    }
    $formatpos = explode($format['separator'], $format['format']);
    $form = array(0, 0, 0);
    foreach($formatpos as $i => $type)
    {
        switch($type)
        {
            case 'd':
            case 'dd':
            $form[2] = intval($preformated[$i]);
            break;
            case 'm':
            case 'mm':
            $form[1] = intval($preformated[$i]);
            break;
            case 'y':
            case 'yy':
            case 'yyyy':
            $form[0] = intval($preformated[$i]);
            break;
        }
    }

    $result = $form[0]."-".$form[1]."-".$form[2];
    return $result;
}

public function convertuserid($FullName, $date, $format = array('format' => 'd-m-y', 'separator' => '-', 'default' => '0')){
            // echo "haha";
    if(!is_array($format))
    {
        return;
    }

        // fix here: try to look for the default value if the date string is zero length
    if(!$date)
    {
        return $format['default'];
    }
    $preformated = explode($format['separator'], $date);
    if(count($preformated) != 3)
    {
        return;
    }
    $formatpos = explode($format['separator'], $format['format']);
    $form = array(0, 0, 0);
    foreach($formatpos as $i => $type)
    {
        switch($type)
        {
            case 'd':
            case 'dd':
            $form[2] = intval($preformated[$i]);
            break;
            case 'm':
            case 'mm':
            $form[1] = intval($preformated[$i]);
            break;
            case 'y':
            case 'yy':
            case 'yyyy':
            $form[0] = intval($preformated[$i]);
            break;
        }
    }
    $FullName = trim(strtoupper(str_replace(" " ,"" ,$FullName)));
    if(strlen($FullName) >= 4) {

        $day = $form[2];
        if ($day < 10) {
            $day = "0".$day;
        }
        $month = $form[1];
        if ($month < 10) {
            $month = "0".$month;
        }
        $result = substr($FullName, 0, 4)."".$day."".$month;
    } else {
        $ck = 4 - strlen($FullName);
        for($i=0; $i<$ck; $i++) {
            $FullName = $FullName."O";
        }

        $day = $form[2];
        if ($day < 10) {
            $day = "0".$day;
        }
        $month = $form[1];
        if ($month < 10) {
            $month = "0".$month;
        }
        $result = $FullName."".$day."".$month;
    }
    return $result;
}
}