<?php

/**
 * Reformat the date to database format
 *
 * @param string The unformated date
 * @param array Optional parameter which contains the input time format and separator
 *              Fix: new optional parameter which will return for default value if no value is entered.
 *              This fix may prevent the database server to produce error message because of the improper date format
 *              The best default value is 0000-00-00 since this value is accepted by the MySQL database server
 *              (I don't know if any other database may accept this value as their default date value)
 * @return string The Formated date (yyyy-mm-dd)
 */
function dbdate($date, $format = array('format' => 'd/m/y', 'separator' => '/', 'default' => '1900-01-01'))
{
    if(!is_array($format))
    {
        return;
    }

    // fix here: try to look for the default value if the date string is zero length
    if(!$date)
    {
        return $format['default'];
    }
    $preformated = explode($format['separator'], $date);
    if(count($preformated) != 3)
    {
        return;
    }
    $formatpos = explode($format['separator'], $format['format']);
    $form = array(0, 0, 0);
    foreach($formatpos as $i => $type)
    {
        switch($type)
        {
            case 'd':
            case 'dd':
                $form[2] = intval($preformated[$i]);
                break;
            case 'm':
            case 'mm':
                $form[1] = intval($preformated[$i]);
                break;
            case 'y':
            case 'yy':
            case 'yyyy':
                $form[0] = intval($preformated[$i]);
                break;
        }
    }

    $result = $form[0]."-".$form[1]."-".$form[2];
    return $result;
}

/**
 * Compare two date in yyyy-mm-dd format
 *
 * @param string The first date to be compared
 * @param string The operator, valid: '<', '>', '<=', '>=', '=='
 * @param string The second date to be compared
 * @return int   Return -1 if the first date is lower than the second date, 0 if equal, 1 if the first is greater
 */
function compare_date($first, $operator, $second)
{
    if(!$first || !$second)
    {
        return false;
    }
    $first = explode("-", $first);
    $second = explode("-", $second);

    // compare year
    if($first[0] > $second[0])
    {
        return ($operator == '>' || $operator == '>=');
    }
    elseif($first[0] < $second[0])
    {
        return ($operator == '<' || $operator == '<=');
    }
    else
    {
        // compare month
        if($first[1] > $second[1])
        {
            return ($operator == '>' || $operator == '>=');
        }
        elseif($first[1] < $second[1])
        {
            return ($operator == '<' || $operator == '<=');
        }
        else
        {
            // compare date
            if($first[2] > $second[2])
            {
                return ($operator == '>' || $operator == '>=');
            }
            elseif($first[2] < $second[2])
            {
                return ($operator == '<' || $operator == '<=');
            }
            else
            {
                return ($operator == '==' || $operator == '<=' || $operator == '>=');
            }
        }
    }
}

function dbdate1($date, $format = array('format' => 'd-m-y', 'separator' => '-', 'default' => '01-01-1900'))
{
    if(!is_array($format))
    {
        return;
    }

    // fix here: try to look for the default value if the date string is zero length
    if(!$date)
    {
        return $format['default'];
    }
    $preformated = explode($format['separator'], $date);
    if(count($preformated) != 3)
    {
        return;
    }
    $formatpos = explode($format['separator'], $format['format']);
    $form = array(0, 0, 0);
    foreach($formatpos as $i => $type)
    {
        switch($type)
        {
            case 'd':
            case 'dd':
                $form[2] = intval($preformated[$i]);
                break;
            case 'm':
            case 'mm':
                $form[1] = intval($preformated[$i]);
                break;
            case 'y':
            case 'yy':
            case 'yyyy':
                $form[0] = intval($preformated[$i]);
                break;
        }
    }

    $result = $form[0]."-".$form[1]."-".$form[2];
    return $result;
}