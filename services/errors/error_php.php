<?php

require_once dirname(__FILE__) . '/../third_party/rollbar-php/src/rollbar.php';
$message = '
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">
<h4>A PHP Error was encountered</h4>

<p>Severity: '.$severity.'</p>
<p>Message:  '.$message.'</p>
<p>Filename: '.$filepath.'</p>
<p>Line Number: '.$line.'</p>
</div>';
if(ROLLBAR_STATUS == 'false'){
    echo $message;
} else {
    $config = array(
    // required
    'access_token' => ROLLBAR_TOKEN,
    // optional - environment name. any string will do.
    'environment' => ROLLBAR_ENVIRONMENT,
    // optional - path to directory your code is in. used for linking stack traces.
);
    Rollbar::init($config);
//    Rollbar::init(array('access_token' => ROLLBAR_TOKEN));
    Rollbar::report_message($message);
    $message = '';
}