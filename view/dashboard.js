

// google.load('visualization', '1.0', {'packages':['corechart']});
$F.loadView(function () {
  "use strict";
  return {
   title: 'Dashboard - HelloBill',
   viewtitle: 'DASHBOARD',
   urlController: 'dashboard/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
    var sf = this;
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());

    var fullDate = new Date()
    var twoDigitMonth  = (((fullDate.getMonth()+1).length) == 1) ? '0'+(fullDate.getMonth()+1) :(fullDate.getMonth()+1);
    var currentDate = fullDate.getDate() + "-" + twoDigitMonth + "-" + fullDate.getFullYear();

    $('#start_end_date').daterangepicker({
      format: 'DD-MM-YYYY',
      startDate: currentDate,
      endDate: currentDate
    });
    google.setOnLoadCallback(sf.drawChartCategory());
    google.setOnLoadCallback(sf.drawChartTable());
    google.setOnLoadCallback(sf.drawChartBarTransaction());
    google.setOnLoadCallback(sf.drawChartBarGross());
    google.setOnLoadCallback(sf.drawChartBarGuest());


  }, drawChartCategory: function() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Misc', 3],
          ['Drink', 1],
          ['Main Course', 1],
          ['Desert', 1]
          ]);

        // Set chart options
        var options = {'title':'Category',
        'width':400,
        'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_cetegory_div'));
        chart.draw(data, options);
      }, drawChartTable: function() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Dani', 5],
          ['Luvita', 2],
          ['Elvi', 3],
          ['Varis', 4]
          ]);

        // Set chart options
        var options = {'title':'Category',
        'width':400,
        'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_table_div'));
        chart.draw(data, options);
      }, drawChartBarTransaction: function() {
        var data = google.visualization.arrayToDataTable([
          ['Day', '# of Transaction'],
          ['12 Jun', 100],
          ['13 Jun', 50],
          ['14 Jun', 20],
          ['15 Jun', 40],
          ['16 Jun', 100]
          ], false);
        if(data.length > 4) {
          var height = (data.length)*70;
        } else {
          var height = 400;
        }
        var options = {
          title: '# of Transaction',
          height:height,
          vAxis: {title: 'Days',  titleTextStyle: {color: 'red'}},
          hAxis: {title: '#Dummy or Live Unit',  titleTextStyle: {color: 'red'}},
          bars: 'horizontal' // Required for Material Bar Charts.
        };
        var chart =  new google.visualization.BarChart(document.getElementById('chart_transaction_div'));
        chart.draw(data, options);
      }, drawChartBarGross: function() {
        var data = google.visualization.arrayToDataTable([
          ['Day', '# of Transaction'],
          ['12 Jun', 100],
          ['13 Jun', 50],
          ['14 Jun', 20],
          ['15 Jun', 40],
          ['16 Jun', 100]
          ], false);
        if(data.length > 4) {
          var height = (data.length)*70;
        } else {
          var height = 400;
        }
        var options = {
          title: '# of Transaction',
          height:height,
          vAxis: {title: 'Days',  titleTextStyle: {color: 'red'}},
          hAxis: {title: '#Gross Sales (In Million)',  titleTextStyle: {color: 'red'}},
          bars: 'horizontal' // Required for Material Bar Charts.
        };
        var chart =  new google.visualization.BarChart(document.getElementById('chart_gross_div'));
        chart.draw(data, options);
      }, drawChartBarGuest: function() {
        var data = google.visualization.arrayToDataTable([
          ['Day', '#Guest', { role: 'style' }],
          ['Mon', 20, 'green'],
          ['Wed', 40, 'green'],
          ['Fri', 50, 'green'],
          ['Sun', 80, 'green'],
          ], false);
        if(data.length > 4) {
          var height = (data.length)*70;
        } else {
          var height = 400;
        }
        var options = {
          title: '# of Transaction',
          height:height,
          vAxis: {title: 'Days',  titleTextStyle: {color: 'red'}},
          hAxis: {title: '#Guest per Day of Week',  titleTextStyle: {color: 'red'}},
          bars: 'horizontal' // Required for Material Bar Charts.
        };
        var chart =  new google.visualization.BarChart(document.getElementById('chart_quest_div'));
        chart.draw(data, options);
      }
    };
  }());
