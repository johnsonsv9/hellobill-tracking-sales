$F.loadView(function () {
  "use strict";
    return {
        title: 'Outlet List - HelloBill',
        viewtitle: 'Outlet List',
        urlController: 'init/',
        defaultArguments: {
            param1: ''
        }, 
        afterLoad: function (arg) {
            var sf = this;
            window.document.title = sf.title;
            $('#viewTitle').text(sf.viewtitle.toUpperCase());
            $F.service({
                type: 'get',    
                url: sf.urlController+'getOptionBranch',
                success: function(data){
                    for (var i = 0; i < data.data.ValidBranchs.length; i++) {
                        var opt = $('<option></option>').attr('value', data.data.ValidBranchs[i].BranchID).text(data.data.ValidBranchs[i].BranchName);
                        $('#MBranchID').append(opt);
                    }
                    $("#MBranchID").val(data.data.activeBranch);    
                }
            });
            //----------------------------sso-------------------------------
//            $F.service({
//                type: 'get',    
//                url: sf.urlController+'getOptionBranch',
//                success: function(data){
//                    for (var i = 0; i < data.data.restaurants.length; i++) {
//                        var opt = $('<option></option>').attr('value', data.data.restaurants[i].Outlets[j].ProductCode +'-'+data.data.restaurants[i].Outlets[j].BranchID).text(data.data.restaurants[i].Name);
//                        $('#MBranchID').append(opt);
//                    }
//                    for (var i = 0; i < data.data.stores.length; i++) {
//                        var opt = $('<option></option>').attr('value', data.data.restaurants[i].Outlets[j].ProductCode +'-'+data.data.restaurants[i].Outlets[j].BranchID).text(data.data.restaurants[i].Name);
//                        $('#MBranchID').append(opt);
//                    }
//                }
//            });
                
            //--------------------------------------------------------------
            
            $('#MBranchID').change(function() {
                changeActiveBranch();
            });
        }
    };
    function changeActiveBranch(){
        var branchID = $('#MBranchID').val(); 
        $F.service({
            type: 'get',
            url: $F.util.buildUrl('init/activeBranch/',{
                SBranchID : branchID
            }),
            success: function(data){
                window.location.replace("#/view-dashboard");
            }
        })   
    }
    //------------------------------sso---------------------------
//    function changeActiveBranch(){
//        var branchID = $('#MBranchID').val(); 
//        var branch = branchID.split('-');   
//        var pCode = branch[0];
//        branchID = branch[1];
//        $F.service({
//            type: 'get',
//            url: $F.util.buildUrl('init/activeBranch/',{
//                SBranchID : branchID, PCode : pCode
//            }),
//            success: function(data){
//                window.location.replace("#/view-dashboard");
//            }
//        })   
//    }
}());