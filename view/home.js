$F.loadView(function () {
  "use strict";

  return {
   title: 'Home - HelloBill',
   viewtitle: 'Home',
   urlController: 'home/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var self = this;
   //  checkPermission();
    window.document.title = self.title;
    $('#viewTitle').text(self.viewtitle.toUpperCase());
    // $('.addbutton').attr('href','#/master/brand/action-brand');
    // $('#MainTable_Brand').DataTable({
    //   "pagingType": "full_numbers"
    // });
   //  $('.branch').attr('href','#/master/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ self.urlController +'export_excel';
   //  });

   var start = moment().subtract(29, 'days');
   var end = moment();
   // console.log("The default date has been set to: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));

   function cb(start, end) {
       $('#reportrange span').html(start.format('MMMM DD, YYYY') + ' - ' + end.format('MMMM DD, YYYY'));
       console.log("A new date selection was made to: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
       // $('#MainTable_category').DataTable().destroy();
       // if ( $.fn.dataTable.isDataTable('#MainTable_category') ) {
       //   alert("a");
       //   $('#MainTable_category').DataTable().destroy();
       // }
       getTotalcategory(self, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
       self.refreshTable(self, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
       // $('#row').remove();
       // $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
   }

   $('#reportrange').daterangepicker({
       startDate: start,
       endDate: end,
       maxDate: end,
       ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       },
       alwaysShowCalendars: true
   }, cb);

   cb(start, end);

   // $F.service({
   //        url: self.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            self.refreshTable(self);
   //        }
   //      });
 }, refreshTable: function (self, from, to) {
    $F.service({
        url: self.urlController + "getTransaction/" + from + to,
        type: 'GET',
        success: function(data){
          // if ( $.fn.dataTable.isDataTable('#MainTable_category') ) {
          //   $('#MainTable_category').DataTable().destroy();
          // }
          DZ.remDataTable("MainTable_category");
          $('.dataRow').remove();
          if(data.Transaction != null){
            var length = data.Transaction.length;
            var row = $("#row");
            var body = $("#Body");
            var usr = data["UserID"];
            var usrtype = data["UserTypeName"];

            for(var i = 0; i < length; i++) {
              var item = data.Transaction[i];

              if(usrtype != "Sales" && usrtype != "Reseller" || usr == item.UserID) {
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");

                $(".InvoiceNO", temp).text(item.InvoiceNO);
                $(".BranchName", temp).text(item.BranchName);
                $(".UserFullName", temp).text(item.UserFullName);
                $(".PaidDate", temp).text(item.PaidDate);
                $(".GrandTotal", temp).text(item.GrandTotal);
                $(".Commission", temp).text(item.Commission);
                $(".Profit", temp).text((item.UserTypeName == "Reseller") ? (item.GrandTotal - item.GrandTotalReseller) : 0);
                body.append(temp);
              }

            }

          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
}
}

function getTotalcategory(self, from, to) {
  $F.service({
      url: self.urlController + "getCommissionProfit/" + from + to,
      type: 'GET',
      success: function(data){
        if(data != null){
          if(data.Status == 0) {
            // console.log(data);
            // alert(data["Total Commission"]);
            if(data["UserTypeName"] == "Sales") {
              $('#report-title').html("Total Commission");
              $('#report').html(data["TotalCommission"]);
            } else if(data["UserTypeName"] == "Reseller") {
              $('#report-title').html("Total Reseller Profit");
              $('#report').html(data["TotalProfit"]);
            }
            // alert(data.TotalCommission[0]);
          }
        } else {
          DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
        }
      }
  })
}

//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
