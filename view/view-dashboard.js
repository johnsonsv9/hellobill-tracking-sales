//author : Ferry Chandra

$F.loadView(function() {
    "use strict";

    return {
        title: 'Dashboard - HelloBill',
        viewtitle: 'DASHBOARD',
        urlController: 'dashboard/',
        defaultArguments: {
            param1: ''
        },
        make_id: function(row, col) {
            return "cellid-" + row + '-' + col;
        },
        afterLoad: function(arg) {
            var sf = this;
            var StartDate = 0;
            var EndDate = 0;
            var cycle = 1;
            window.document.title = sf.title;
            sf.getPermission(sf);
            sf.refreshReport(sf);
            $('#viewTitle').text(sf.viewtitle.toUpperCase());
            $(function() {
                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    StartDate = start.format('D-MM-YYYY');
                    EndDate = end.format('D-MM-YYYY');
                    sf.refreshTable(sf, StartDate, EndDate);
                    sf.refreshSalesTrend(sf, StartDate, EndDate, cycle);
                }
                cb(moment(), moment());
                $('#reportrange').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    "opens": "right"
                }, cb);
                $('.daterangepicker').addClass("show-calendar");
                $('.daterangepicker_start_input').remove();
                $('.daterangepicker_end_input').remove();
                $(".ranges").click(function() {
                    $('.daterangepicker').addClass("show-calendar");
                });
            });
             $( "#nav-day" ).click(function() {
                  var SelectedStartDate = $('#reportrange').data('daterangepicker').startDate.format('D-MM-YYYY');
                  var SelectedEndDate = $('#reportrange').data('daterangepicker').endDate.format('D-MM-YYYY');
                  $('.timeClear').removeClass("active");
                  $('#nav-day').addClass("active");
                  cycle = 1;
                  sf.refreshSalesTrend(sf, SelectedStartDate, SelectedEndDate, cycle);
             });
             $( "#nav-week" ).click(function() {
                  var SelectedStartDate = $('#reportrange').data('daterangepicker').startDate.format('D-MM-YYYY');
                  var SelectedEndDate = $('#reportrange').data('daterangepicker').endDate.format('D-MM-YYYY');
                  $('.timeClear').removeClass("active");
                  $('#nav-week').addClass("active");
                  cycle = 2;
                  sf.refreshSalesTrend(sf, SelectedStartDate, SelectedEndDate, cycle);
             });
             $( "#nav-month" ).click(function() {
                 var SelectedStartDate = $('#reportrange').data('daterangepicker').startDate.format('D-MM-YYYY');
                 var SelectedEndDate = $('#reportrange').data('daterangepicker').endDate.format('D-MM-YYYY');
                 $('.timeClear').removeClass("active");
                 $('#nav-month').addClass("active");
                 cycle = 3;
                 sf.refreshSalesTrend(sf, SelectedStartDate, SelectedEndDate, cycle);
             });
             $( "#nav-year" ).click(function() {
                 var SelectedStartDate = $('#reportrange').data('daterangepicker').startDate.format('D-MM-YYYY');
                 var SelectedEndDate = $('#reportrange').data('daterangepicker').endDate.format('D-MM-YYYY');
                 $('.timeClear').removeClass("active");
                 $('#nav-year').addClass("active");
                 cycle = 4;
                 sf.refreshSalesTrend(sf, SelectedStartDate, SelectedEndDate, cycle);
            });
        }, refreshReport: function(self) {
          $F.service({
            url: 'init/getCommissionProfit',
            success: function (data) {
              console.log(data);
              alert(data.TotalCommission[0]);
               $('#report').html(data.TotalCommission[0]);
          }
        });

        },
        refreshTable: function(self, StartDate, EndDate) {
            var rf = this;
            $F.service({
                url: self.urlController + 'getDashboardReport/' + StartDate +'/' + EndDate ,
                timeout: 300000,
                type: 'GET',
                success: function(result) {
                    if(result.data){
                        textDashBoard(result);
                        createTopCategoryByAmount(result);
                        createTopCategoryByQty(result);
                        createTopItemByQty(result);
                        createTopItemByAmount(result);
                        createTopPaymentMethodByQty(result);
                        createTopPaymentMethodByAmount(result);
                        createTopSpender(result);
                        createStaffOnlineStatus(result);
                        createStaffPerformance(result);
                    }
                    $('.currency').text(result.currency + ' ');
                }
            })
        },getPermission: function(self) {
            var rf = this;
            $F.service({
                url: 'init/getValidPermissionGroup/Dashboard',
                type: 'GET',
                success: function(data) {
                    $('.dashboard').each(function(){
                        $(this).addClass('hidden');
                    });
                    var length = data.data.PermissionList.length;
                    if(length > 0){
                        $('#dashboard-calendar').removeClass('hidden');
                    } else {
                        $('#topic').text('You Dont Have Permission to View Dashboard');
                    }
                    for(var i = 0; i < length; i++){
                        var permission = data.data.PermissionList[i];
                        $('#'+permission.BackendPermissionID).removeClass('hidden');
                    }
                    $('.hidden.dashboard').remove();
                }
            })
        },
        refreshSalesTrend: function(self, StartDate, EndDate, cycle) {
            var rf = this;
            $F.service({
                url: self.urlController + 'getSalesFigure/' + StartDate +'/' + EndDate +'/' + cycle ,
                type: 'GET',
                success: function(result) {
                    if(result.data){
                        createSalesTrend(result);
//                        element = $('#chart_neo_revenue_graph_matrix');
//                        data = result.data.Sales;
//                        createMatrix(result,self, element, data);

                        createSalesGraphAmount(result);
                        createSalesGraphQty(result);
                    }
                }
            })
        },
        getHour: function(isi) {
            if(isi < 10) {
                isi = "0"+isi;
            }
            return isi;
        }
    }

    function textDashBoard(ajaxResult){

        $("#value_gross_sales").text($F.format.cleanNumber(ajaxResult.data.GrossSales));
        $("#value_profit_report").text($F.format.cleanNumber(ajaxResult.data.Sales));
        $("#value_guest").text($F.format.cleanNumber(ajaxResult.data.TotalTransaction));
        $("#value_avg_transaction").text($F.format.cleanNumber(ajaxResult.data.AverageTransaction));
    }


    function createTopCategoryByAmount(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Amount";
        var element = "chart_top_category_grossing";
        data.addColumn("string", "CategoryName");
        data.addColumn("number", "Total");
        length = ajaxResult.data.TopCategoryByAmount.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopCategoryByAmount[i];
             arr[i] = [item.CategoryName];
             arr[i].push(parseInt(item.Amount));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }

    function createTopCategoryByQty(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Qty";
        var element = "chart_top_category_sales";
        data.addColumn("string", "CategoryName");
        data.addColumn("number", "Qty");
        length = ajaxResult.data.TopCategoryByQty.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopCategoryByQty[i];
             arr[i] = [item.CategoryName];
             arr[i].push(parseInt(item.Qty));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }


    function createStaffOnlineStatus(ajaxResult){
        DZ.remDataTable('MainTable_Online_Status');
        var c = $('#iTemplateBody');
        var t = $('#iTemplate');
        $('.dataRow').remove();
        for(var i = 0; i < ajaxResult.data.StaffOnlineStatus.length; i++) {
            var item = ajaxResult.data.StaffOnlineStatus[i];
            var e = t.clone();
            var clockin = "";
            var clockout = "";
            var date = "";
            if(item.ClockIn){
                clockin = item.ClockIn.substring(11, 16);
                date = item.ClockIn.substring(0, 10);
            }
            if(item.ClockOut){
                clockout = item.ClockOut.substring(11, 16);
            }
            if(item.AttendanceStatus == "O" || !item.AttendanceStatus){
                e.removeClass('looptemplate').removeAttr('id').addClass('dataRow');
            }
            else{
                e.removeClass('looptemplate').removeAttr('id').addClass('dataRow').css("background-color", "#FFB6C1");
            }
            $('.iFullname', e).text(item.Fullname);
            $('.iUserTypeName', e).text(item.UserTypeName);
            $('.iClockIn', e).text(clockin);
            $('.iClockOut', e).text(clockout);
            $('.iStatus', e).text(item.Status);
            $('.iDate', e).text(date);
            c.append(e);
        }
    }

    function createTopItemByAmount(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Amount";
        var element = "chart_top_grossing";
        data.addColumn("string", "ItemName");
        data.addColumn("number", "Amount");
        length = ajaxResult.data.TopItemByAmount.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopItemByAmount[i];
             arr[i] = [item.ItemName];
             arr[i].push(parseInt(item.Amount));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }

    function createStaffPerformance(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "Staff Performance";
        var element = "chart_staff_performance";
        data.addColumn("string", "Fullname");
        data.addColumn("number", "TotalSalesTransaction");
        length = ajaxResult.data.StaffPerformance.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.StaffPerformance[i];
             arr[i] = [item.Fullname];
             arr[i].push(parseInt(item.TotalSalesTransaction));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }


    function createTopItemByQty(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Qty";
        var element = "chart_top_sales";
        data.addColumn("string", "ItemName");
        data.addColumn("number", "Qty");
        length = ajaxResult.data.TopItemByQty.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopItemByQty[i];
             arr[i] = [item.ItemName];
             arr[i].push(parseInt(item.Qty));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }


    function createTopPaymentMethodByAmount(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Amount";
        var element = "chart_payment_method_breakdown_total_report";
        data.addColumn("string", "PaymentMethodName");
        data.addColumn("number", "Amount");
        length = ajaxResult.data.TopPaymentByAmount.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopPaymentByAmount[i];
             arr[i] = [item.PaymentMethodName];
             arr[i].push(parseInt(item.Amount));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }


    function createTopPaymentMethodByQty(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Qty";
        var element = "chart_payment_method_breakdown_count_report";
        data.addColumn("string", "PaymentMethodName");
        data.addColumn("number", "Qty");
        length = ajaxResult.data.TopPaymentByQty.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopPaymentByQty[i];
             arr[i] = [item.PaymentMethodName];
             arr[i].push(parseInt(item.Qty));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }

    function createTopSpender(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Qty";
        var element = "chart_customer_spending_breakdown";
        data.addColumn("string", "CustomerName");
        data.addColumn("number", "TotalSpent");
        length = ajaxResult.data.TopSpender.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.TopSpender[i];
             arr[i] = [item.CustomerName];
             arr[i].push(parseInt(item.TotalSpent));
             data.addRow(arr[i]);
        }
        drawChart(title, element, data, "pie");
    }

    function createSalesGraphAmount(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Amount";
        var element = "chart_sales_graph_by_amount";
        data.addColumn("string", "CycleName");
        data.addColumn("number", "Gross Sales");
        length = ajaxResult.data.SalesGraph.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.SalesGraph[i];
            arr[i] = [item.CycleName];
            arr[i].push(parseInt(item.TotalSales));
            data.addRow(arr[i]);
        }
        drawChart(title, element, data, "line");
    } // this is for calling Top Category By Amount data then create its chart

    function createSalesGraphQty(ajaxResult){
        var arr =[];
        var data = new google.visualization.DataTable();
        var title = "By Qty";
        var element = "chart_sales_graph_by_qty";
        data.addColumn("string", "CycleName");
        data.addColumn("number", "Transaction");
        length = ajaxResult.data.SalesGraph.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.SalesGraph[i];
            arr[i] = [item.CycleName];
            arr[i].push(parseInt(item.TotalTransactions));
            data.addRow(arr[i]);
        }
        drawChart(title, element, data, "line");
    } // this is for calling Top Category By Amount data then create its chart

    function createSalesTrend(ajaxResult){

      var arr =[];
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Day of Week');
      data.addColumn('number', 'Sales');
        length = ajaxResult.data.SalesTrend.length;
        for(var i = 0; i < length; i++){
            var item = ajaxResult.data.SalesTrend[i];
            arr[i] = [item.Day];
            arr[i].push(parseInt(item.TotalSales));
            data.addRow(arr[i]);
        }
      var options = {
        title: 'Sales Trend',
        hAxis: {
          title: 'Time of Day'
        },
        vAxis: {
          title: 'Trend'
        }
      };

      var material = new google.visualization.ColumnChart(document.getElementById('chart_sales_trend'));
      material.draw(data, options);
    }

    function findBootstrapEnvironment() {
        var envs = ['xs', 'sm', 'md', 'lg'];
        var $el = $('<div>');
        $el.appendTo($('body'));
        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];
            $el.addClass('hidden-' + env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env;
            }
        }
    } // this is for getting web size, if return xs mean so small, especially when someone open in smartphone
    function drawChart(title, element, data, type) {
        if(type == "pie")
        {
            var position = "right";
            if(findBootstrapEnvironment() == "xs"){
                position = "bottom";
            }
            var options = {
              'title': title,
              'height': 300,
              'pieHole': 0.4,
              'legend' : position
            };
            var chart = new google.visualization.PieChart(document.getElementById(element));
        }
        else if(type == "line")
        {
            var position = "bottom";
            var options = {
              'title': title,
              'legend': { position: position },
              hAxis : {
                textStyle : {
                    fontSize: 10 // or the number you want
                }
              }

            };
            var chart = new google.visualization.LineChart(document.getElementById(element));
        }
        chart.draw(data, options);
    } // this is for creating chart
}());
