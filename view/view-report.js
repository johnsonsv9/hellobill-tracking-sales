(function($, $F) {
    "use strict";

    $F.loadView({
        title: 'Reports - HelloBill',
        viewtitle: 'REPORTS',
        afterLoad: function(arg) {
            $('.date-picker').datepicker({
                orientation: "top auto",
                autoclose: true
            });
            $('#viewTitle').text(this.viewtitle.toUpperCase());
            $('.clear').removeClass('.active');
            setTimeout(function(){
                getValidPermission();
            }, 400);
           
        },
    });
     function getValidPermission(){
          $F.service({
               type: 'get',
               url: 'init/getValidPermissionGroup/Report',
               success: function(data){
                   var length = data.data.PermissionList.length;
                   var first = '';
                   for(var i = 0; i < length; i++){
                       var permission = data.data.PermissionList[i];
                       var last = permission.Link.split("/");
                       $('.nav-pills').append('<li id="'+permission.BackendPermissionID+'" class="clear"><a class = "get dataReport" id="nav-'+last[last.length-1]+'">'+permission.Name+'</a></li>');
                       $('#nav-'+last[last.length-1]).attr("href", permission.Link);
                   }
                   getAngkasaPuraPermission();
                   $('.dataReport').eq(0).closest('li').addClass('active');
                   window.location.href = $('.dataReport').eq(0).attr('href');
               }
            }) 
      }
     function getAngkasaPuraPermission(){
          $F.service({
               type: 'get',
               url: 'report/getDataAndCheckAngkasaPura'+"/check"+"/none",
               success: function(data){
                   var check = data.HaveAccess;
                   if(check == true){
//                       $('#nav-angkasa_pura').removeClass("hidden");
                   }
                   else{
                       $('#report-13').remove();
                   }
               }
           }) 
      }
})(jQuery, $F);