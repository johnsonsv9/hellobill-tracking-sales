$F.loadView(function() {
    "use strict";
    return {
       title: 'Quotation - HelloBill',
       viewtitle: 'Quotation',
       urlController: 'quotation/',
       urlControllerLicense: 'license/',
       urlControllerHardware: 'hardware/',
       urlControllerDiscount: 'discount/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post="";
            self.refreshTableLicense(param, self);
            self.refreshTableHardware(param, self);
            $('#back').attr("href", "#/transaction/quotation");
            // $('.edit').prop('hidden', 'hidden');
            // $('.edit').removeAttr('hidden');
              // $('#validationQuotation').html('<button class="btn btn-primary revisi looptemplate">Approve</button>');
            // $('#validationQuotation').append('<button class="btn btn-addon revisi looptemplate">Reject</button>');

      //bwt munculin judul
            if (param.param[0]) {
                post = param.param[0];
                getUpdateData(param, self);
                // var numRows = [];
                self.refreshTableItem(param, self);
                // alert(typeof(numRows));
            } else {
                window.location = "#/transaction/quotation";
            }
            $('#topic').text('Detail ' + self.viewtitle);


            //mengatur combobox license
            $("#LicenseIDFormL").change(function(){
              $("#DiscountIDFormL, #PriceFormL, #QtyFormL, #ResPriceFormL, #ComPercentageFormL, #ComValueFormL, #PercentageFormL, #ValueFormL").val('');
              $("#DiscountIDFormL").empty();
              $("#DiscountIDFormL").append("<option value=''>Please select</option>");
              // $("#PriceFormL").val('');
              // $("#ResPriceFormL").val('');
              // $("#ComPercentageFormL").val('');
              // $("#ComValueFormL").val('');
              // $("#PercentageFormL").val('');
              // $("#ValueFormL").val('');
              if($(this).val() != '') {
                var val = $(this).val();
                var price = $('option:selected', this).attr("price");
                var resprice = $('option:selected', this).attr("resprice");
                var p = $('option:selected', this).attr("p");
                var v = $('option:selected', this).attr("v");

                $("#PriceFormL").val(price);
                $("#QtyFormL").val("1");
                // alert($('#Type').val());

                if($('#Type').val() == "Reseller") {
                  $("#ResPriceFormL").val(resprice);
                  $("#ComPercentageFormL").val(0);
                  $("#ComValueFormL").val(0);
                } else if($('#Type').val() == "Sales") {
                  $("#ResPriceFormL").val(0);
                  $("#ComPercentageFormL").val(p);
                  $("#ComValueFormL").val(v);
                }
                self.refreshTableDiscountForLicense(param, self, val, 0);
              }
            });

            //mengatur combobox discount (di sisi license)
            $("#DiscountIDFormL").change(function(){
              if($(this).val()!='') {
                var val = $(this).val();
                var p = $('option:selected', this).attr("p");
                var v = $('option:selected', this).attr("v");

                // if(p != "") $("#PercentageFormL").removeAttr('required');
                // else $("#PercentageFormL").prop('required', 'required');
                //
                // if(p != "") $("#ValueFormL").removeAttr('required');
                // else $("#ValueFormL").prop('required', 'required');

                // $("#DiscountIDFormA").val(val);
                $("#PercentageFormL").val(p);
                $("#ValueFormL").val(v);
              }
            });

            //mengatur combobox hardware
            $("#HardwareIDFormH").change(function(){
              $("#DiscountIDFormH, #PriceFormH, #QtyFormH #PercentageFormH, #ValueFormH").val('');
              $("#DiscountIDFormH").empty();
              $("#DiscountIDFormH").append("<option value=''>Please select</option>");
              if($(this).val() != '') {
                var val = $(this).val();
                var price = $('option:selected', this).attr("price");
                var p = $('option:selected', this).attr("p");
                var v = $('option:selected', this).attr("v");

                // $("#LicenseIDFormA").val(val);
                $("#PriceFormH").val(price);
                $("#QtyFormH").val("1");
                self.refreshTableDiscountForHardware(param, self, val, 0);
              }
            });

            //mengatur combobox discount (di sisi hardware)
            $("#DiscountIDFormH").change(function(){
              if($(this).val()!='') {
                var val = $(this).val();
                var p = $('option:selected', this).attr("p");
                var v = $('option:selected', this).attr("v");

                // if(p != "") $("#PercentageFormL").removeAttr('required');
                // else $("#PercentageFormL").prop('required', 'required');
                //
                // if(p != "") $("#ValueFormL").removeAttr('required');
                // else $("#ValueFormL").prop('required', 'required');

                // $("#DiscountIDFormA").val(val);
                $("#PercentageFormH").val(p);
                $("#ValueFormH").val(v);
              }
            });

            //mengatur persen ato value doang
            terikat($('input.terikat.License'), $('.terikat.License'), $("#DiscountIDFormL"));
            terikat($('input.terikat.Hardware'), $('.terikat.Hardware'), $("#DiscountIDFormH"));

            //jika persen ato value dirubah2, diskon preset jd kosong
            $('.terikat.License').on("input", function(){
              $("#DiscountIDFormL").val('');
            });
            $('.terikat.Hardware').on("input", function(){
              $("#DiscountIDFormH").val('');
            });


            var tbL = document.getElementById("Body_License");
            var tbH = document.getElementById("Body_Hardware");
            // var noL = tbL.rows.length;
            // var noH = $('#Body_Hardware tr').length;
            // var noL = $('#countRowL').val(),
            //     noH = $('#countRowH').val();
            // var noL = numRows[0],
            //     noH = numRows[1];
            // alert(noL);
            // alert(noH);

            var LicenseID = [];
            var HardwareID = [];
            var Price = [];
            var ResPrice = [];
            var Qty = [];
            var ComP = [];
            var ComV = [];
            var DiscountID = [];
            var Percentage = [];
            var Value = [];


            $('#finish').click(function(){
              var is_it = $('#finish').attr("isheadofsales");
              // alert(is_it);
              var sttus = "Waiting";
              if(is_it == "yes") sttus = "Approved";

              var rowLicense = document.getElementById("Body_License");
              for (var i = 1; i < rowLicense.rows.length; i++) {
                var cell = rowLicense.rows.item(i).cells;
                LicenseID.push(cell.item(0).getAttribute("licenseid"));
                HardwareID.push(cell.item(0).getAttribute("hardwareid"));
                Price.push(cell.item(1).innerHTML);
                ResPrice.push(cell.item(2).innerHTML);
                Qty.push(cell.item(3).innerHTML);
                DiscountID.push(cell.item(4).getAttribute("discountid"));
                Percentage.push(cell.item(5).innerHTML);
                Value.push(cell.item(6).innerHTML);
                ComP.push(cell.item(7).innerHTML);
                ComV.push(cell.item(8).innerHTML);
              }

              var rowHardware = document.getElementById("Body_Hardware");
              for (var i = 1; i < rowHardware.rows.length; i++) {
                var cell = rowHardware.rows.item(i).cells;
                LicenseID.push(cell.item(0).getAttribute("licenseid"));
                HardwareID.push(cell.item(0).getAttribute("hardwareid"));
                Price.push(cell.item(1).innerHTML);
                if ($('#Type').val() == "Reseller") {
                  ResPrice.push(cell.item(1).innerHTML);
                } else ResPrice.push("0");
                Qty.push(cell.item(2).innerHTML);
                DiscountID.push(cell.item(3).getAttribute("discountid"));
                Percentage.push(cell.item(4).innerHTML);
                Value.push(cell.item(5).innerHTML);
                ComP.push("0");
                ComV.push("0");
              }

              console.log(JSON.stringify({
                Status: sttus,
                LicenseID: LicenseID,
                HardwareID: HardwareID,
                Price: Price,
                ResPrice: ResPrice,
                Qty: Qty,
                ComPer: ComP,
                ComVal: ComV,
                DiscountID: DiscountID,
                DiscPer: Percentage,
                DiscVal: Value
              }));


              $F.service({
                type: 'post',
                url: 'quotation/actionDetail/' + post,
                data: JSON.stringify({
                  Status: sttus,
                  LicenseID: LicenseID,
                  HardwareID: HardwareID,
                  Price: Price,
                  ResPrice: ResPrice,
                  Qty: Qty,
                  ComPer: ComP,
                  ComVal: ComV,
                  DiscountID: DiscountID,
                  DiscPer: Percentage,
                  DiscVal: Value
                }),
                success: function(data){
                  if(data != null) {
                    if(data.Status == 0) {
                      $F.popup.show({
                          content: data.Message
                      });
                      $('.modal-content').attr("style", "width : 250px");
                      setTimeout(function() {
                          window.location = $F.config.get('baseUri') + '#/transaction/quotation';
                          $('.popup-close-button').click();
                      }, 1500);
                    } else {
                      var err = '';
                      for (var field in data.Errors) {
                          err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                      }
                      DZ.showAlertWarning(err, ".modal-footer", 0);
                    }
                  } else {
                    DZ.showAlertWarning("Something is wrong, Update failed.", ".modal-footer", 0);
                  }
                }
              }); //tutup service
            });

      //Save dari form ke tabel Hardware (action2) atau License (action)
            $('#action2').submit(function(e) {
              // $('#MainTable_Hardware').DataTable().destroy();
              DZ.remDataTable("MainTable_Hardware");
              e.preventDefault();
              if($('#HardwareIDFormH').val() == '') {
                DZ.showAlertWarning("You haven't chose the hardware yet.", "#error_hardware_null", 0);
                $('#HardwareIDFormH').focus();
              }
              else {

                var noH = $('#countRowH').val();

                var p = $("#PriceFormH").val();
                var q = ($("#QtyFormH").val()) ? $("#QtyFormH").val() : "1";
                var discount = ($("#DiscountIDFormH option:selected").val()) ? $("#DiscountIDFormH option:selected").text() : '';
                var pr = $("#PercentageFormH").val();
                var vl = $("#ValueFormH").val();

                if($('#currentRowH').val() != '') {
                    var currentRow = parseInt($("#currentRowH").val()) + 1;
                    alert()
                  var tr = $('#'+currentRow);
                  console.log(tr);
                  DZ.remDataTable("MainTable_Hardware");

                  $(".Hardware", tr).text($("#HardwareIDFormH option:selected").text());
                    $(".Hardware", tr).attr("LicenseID", "");
                    $(".Hardware", tr).attr("HardwareID", $("#HardwareIDFormH option:selected").val());
                  $(".Price", tr).text(p);
                  // alert(q);
                  $(".Qty", tr).text(q);
                  if(pr || vl) $(".Discount", tr).text("Custom");
                  else $(".Discount", tr).text(discount);
                    $(".Discount", tr).attr('DiscountID', $("#DiscountIDFormH option:selected").val());
                  $(".Percentage", tr).text(pr);
                  $(".Value", tr).text(vl);
                  $(".SubTotal", tr).text( ( ( p * ( 1 - ( pr / 100 ) ) ) - vl ) * q );

                  $('#currentRowH').val('');
                } else {
                    DZ.remDataTable("MainTable_Hardware");
                  var row = $("#row_Hardware");
                  var body = $("#Body_Hardware");
                  var temp = row.clone();


                  noH++;
                  $('#countRowH').val(noH);

                  temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                  temp.attr('id', 'H' + noH);
                  // $(".No", temp).text(noH);
                  $(".Hardware", temp).text($("#HardwareIDFormH option:selected").text());
                    $(".Hardware", temp).attr("LicenseID", "");
                    $(".Hardware", temp).attr("HardwareID", $("#HardwareIDFormH option:selected").val());
                  $(".Price", temp).text(p);
                  $(".Qty", temp).text(q);
                  if(pr || vl) $(".Discount", temp).text("Custom");
                  else $(".Discount", temp).text(discount);
                    $(".Discount", temp).attr('DiscountID', $("#DiscountIDFormH option:selected").val());
                  $(".Percentage", temp).text(pr);
                  $(".Value", temp).text(vl);
                  $(".SubTotal", temp).text( ( ( p * ( 1 - ( pr / 100 ) ) ) - vl ) * q );
                  $(".Action", temp).attr('attr-id', 'H' + tbH.rows.length);
                  body.append(temp);
                }
                // $('#row_Hardware').remove();
                $('#MainTable_Hardware').DataTable({ "pagingType": "full_numbers" });

                $(".Hardware").val('');
                $("#DiscountIDFormH").empty();
                $("#DiscountIDFormH").append("<option value=''>Please select</option>");
                $("table[tabIndex=1]").focus();

                $(".Action > .edittable", temp).click(function(){
                  $(".Hardware").val('');
                  $("#DiscountIDFormH").empty();
                  $("#DiscountIDFormH").append("<option value=''>Please select</option>");
                  var Val = $(this).closest('.Action').attr('attr-id');
                  alert(Val);
                  var td = $(this).closest('tr');
                  $('#field_Hardware').focus();
                  // alert(Val);

                  $('#HardwareIDFormH').val(td.find('.Hardware').attr('hardwareid'));
                  $('#currentRowH').val(Val);
                  var hardwareid = td.find('.Hardware').attr('hardwareid');
                  var discountid = td.find('.Discount').attr('discountid');
                  self.refreshTableDiscountForHardware(param, self, hardwareid, discountid);
                  $('#PriceFormH').val(td.find('.Price').text());
                  $('#QtyFormH').val(td.find('.Qty').text());
                  $('#PercentageFormH').val(td.find('.Percentage').text());
                  $('#ValueFormH').val(td.find('.Value').text());

                });
                $('.Action > .delete', temp).click(function(){
                    DZ.remDataTable("MainTable_Hardware");
                  var Val = $(this).closest('.Action').attr('attr-id');
                  alert(Val);
                  $(this).parents("tr").remove();
                  $('#MainTable_Hardware').DataTable({ "pagingType": "full_numbers" });
                });
                // var row = document.getElementById("Body_Hardware");
                // var rowLength = row.rows.length - 1;
                //
                // var cell = row.rows.item(rowLength).cells;
                // LicenseID.push(cell.item(0).getAttribute("licenseid"));
                // HardwareID.push(cell.item(0).getAttribute("hardwareid"));
                // Price.push(cell.item(1).innerHTML);
                // Qty.push(cell.item(2).innerHTML);
                // DiscountID.push(cell.item(3).getAttribute("discountid"));
                // Percentage.push(cell.item(4).innerHTML);
                // Value.push(cell.item(5).innerHTML);
                // ComP.push('');
                // ComV.push('');
              }

            });

            $('#action').submit(function(e) {
              // $('#MainTable_License').DataTable().destroy();
              DZ.remDataTable("MainTable_License");
                e.preventDefault();
                if($('#LicenseIDFormL').val() == '') {
                  DZ.showAlertWarning("You haven't chose the license yet.", "#error_license_null", 0);
                  $('#LicenseIDFormL').focus();
                }
                else {
                  // alert($('#currentRowL').val());
                  var noL = $('#countRowL').val();

                  var p = $("#PriceFormL").val();
                  var rp = $("#ResPriceFormL").val();
                  var q = ($("#QtyFormL").val()) ? $("#QtyFormL").val() : "1";
                  var discount = ($("#DiscountIDFormL option:selected").val()) ? $("#DiscountIDFormL option:selected").text() : '';
                  var dpr = $("#PercentageFormL").val();
                  var dvl = $("#ValueFormL").val();
                  var cpr = $("#ComPercentageFormL").val();
                  var cvl = $("#ComValueFormL").val();

                  if($('#currentRowL').val() != '') {
                    var tr = $('#'+$("#currentRowL").val());

                    $('.License', tr).text($("#LicenseIDFormL option:selected").text());
                      $(".License", tr).attr("LicenseID", $("#LicenseIDFormL option:selected").val());
                      $(".License", tr).attr("HardwareID", '');
                    $(".Price", tr).text(p);
                    $(".ResPrice", tr).text(rp);
                    $(".Qty", tr).text(q);
                    if(dpr || dvl) $(".Discount", tr).text("Custom");
                    else $(".Discount", tr).text(discount);
                      $(".Discount", tr).attr('DiscountID', $("#DiscountIDFormL option:selected").val());
                    $(".DPercentage", tr).text(dpr);
                    $(".DValue", tr).text(dvl);
                    $(".CPercentage", tr).text(cpr);
                    $(".CValue", tr).text(cvl);
                    $(".SubTotal", tr).text(( ( p * ( 1 - ( dpr / 100 ) ) ) - dvl ) * q);
                    $(".ResSubTotal", tr).text(rp * q);
                    if($("#ComValueFormL").val())
                      $(".Commission", tr).text(cvl * q);
                    else
                      $(".Commission", tr).text(( p * ( cpr / 100 ) ) * q);

                    $('#currentRowL').val('');
                  } else {
                    var row = $("#row_License");
                    var body = $("#Body_License");
                    var temp = row.clone();


                    noL++;
                    $('#countRowL').val(noL);

                    temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                    temp.attr('id', 'L' + noL);
                    // $(".No", temp).text(noL);
                    $(".License", temp).text($("#LicenseIDFormL option:selected").text());
                      $(".License", temp).attr("LicenseID", $("#LicenseIDFormL option:selected").val());
                      $(".License", temp).attr("HardwareID", '');
                    $(".Price", temp).text(p);
                    $(".ResPrice", temp).text(rp);
                    $(".Qty", temp).text(q);
                    if(dpr || dvl) $(".Discount", temp).text("Custom");
                    else $(".Discount", temp).text(discount);
                      $(".Discount", temp).attr('DiscountID', $("#DiscountIDFormL option:selected").val());
                    $(".DPercentage", temp).text(dpr);
                    $(".DValue", temp).text(dvl);
                    $(".CPercentage", temp).text(cpr);
                    $(".CValue", temp).text(cvl);
                    $(".SubTotal", temp).text(( ( p * ( 1 - ( dpr / 100 ) ) ) - dvl ) * q);
                    $(".ResSubTotal", temp).text(rp * q);
                    if($("#ComValueFormL").val())
                      $(".Commission", temp).text(cvl * q);
                    else
                      $(".Commission", temp).text(( p * ( cpr / 100 ) ) * q);
                    $(".Action", temp).attr('attr-id', 'L' + tbL.rows.length);
                    body.append(temp);
                  }
                  // $('#row_License').remove();
                  $('#MainTable_License').DataTable({ "pagingType": "full_numbers" });

                  $(".License").val('');
                  $("#DiscountIDFormL").empty();
                  $("#DiscountIDFormL").append("<option value=''>Please select</option>");
                  $("table[tabIndex=0]").focus();

                  $(".Action > .edittable", temp).click(function(){
                    $('.License').val('');
                    $("#DiscountIDFormL").val('');
                    $("#DiscountIDFormL").empty();
                    $("#DiscountIDFormL").append("<option value=''>Please select</option>");
                    var Val = $(this).closest('.Action').attr('attr-id');
                    var td = $(this).closest('tr');
                    $('#field_License').focus();
                    // alert(Val);

                    $('#LicenseIDFormL').val(td.find('.License').attr('licenseid'));
                    $('#currentRowL').val(Val);
                    var licenseid = td.find('.License').attr('licenseid');
                    var discountid = td.find('.Discount').attr('discountid');
                    self.refreshTableDiscountForLicense(param, self, licenseid, discountid);
                    $('#PriceFormL').val(td.find('.Price').text());
                    $('#ResPriceFormL').val(td.find('.ResPrice').text());
                    $('#QtyFormL').val(td.find('.Qty').text());
                    // $('#DiscountIDFormL').val(td.find('.Discount').attr('discountid'));
                    $('#PercentageFormL').val(td.find('.DPercentage').text());
                    $('#ValueFormL').val(td.find('.DValue').text());
                    $('#ComPercentageFormL').val(td.find('.CPercentage').text());
                    $('#ComValueFormL').val(td.find('.CValue').text());
                  });
                  $('.Action > .delete', temp).click(function(){
                    var Val = $(this).closest('.Action').attr('attr-id');
                    alert(Val);
                    $(this).parents("tr").remove();
                  });
                  // var row = document.getElementById("Body_License");
                  // var rowLength = row.rows.length - 1;
                  //
                  // var cell = row.rows.item(rowLength).cells;
                  // LicenseID.push(cell.item(0).getAttribute("licenseid"));
                  // HardwareID.push(cell.item(0).getAttribute("hardwareid"));
                  // Price.push(cell.item(1).innerHTML);
                  // Qty.push(cell.item(2).innerHTML);
                  // DiscountID.push(cell.item(3).getAttribute("discountid"));
                  // Percentage.push(cell.item(4).innerHTML);
                  // Value.push(cell.item(5).innerHTML);
                  // ComP.push(cell.item(6).innerHTML);
                  // ComV.push(cell.item(7).innerHTML);
                }

            });


        }, refreshTableItem: function (param, self) {
           $F.service({
             url:self.urlController+"getDetailData/"+param.param[0],
             type: 'GET',
             success: function(data){
                 DZ.remDataTable("MainTable_License");
                 DZ.remDataTable("MainTable_Hardware");
                 $('.dataRow').remove();
               if(data != null) {
                 var x = data.QuotationDetailbyQuotationID.length;
                 var rowL = $('#row_License');
                 var bodyL = $("#Body_License");
                 var rowH = $('#row_Hardware');
                 var bodyH = $("#Body_Hardware");
                 var noLicense = 1;
                 var noL = 0;
                 var noHardware = 1;
                 var noH = 0;
                 // body.empty();
                 // var firstLine = "<option value=''>Please select</option>";
                 // body.append(firstLine);
                 // alert(jenis);
                 for(var i = 0; i < x; i++){
                   var item = data.QuotationDetailbyQuotationID[i];

                   var tbL = document.getElementById("Body_License");
                   var tbH = document.getElementById("Body_Hardware");

                   if(item.LicenseID) {
                     var tempL = rowL.clone();
                     tempL.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                     tempL.attr('id', 'L' + tbL.rows.length);
                     $(".No", tempL).text(noLicense);
                     $(".License", tempL).text(item.LicenseName);
                      $(".License", tempL).attr('licenseid', item.LicenseID);
                      $(".License", tempL).attr('hardwareid', '');
                     $(".Price", tempL).text(item.Price);
                     $(".ResPrice", tempL).text(item.ResellerPrice);
                     $(".Qty", tempL).text(item.Qty);
                     $(".Discount", tempL).text(item.DiscountName);
                      $(".Discount", tempL).attr('discountid', item.DiscountID);
                     $(".DPercentage", tempL).text(item.Percentage);
                     $(".DValue", tempL).text(item.Value);
                     $(".CPercentage", tempL).text(item.CommissionPercentage);
                     $(".CValue", tempL).text(item.CommissionValue);
                     $(".SubTotal", tempL).text(item.SubTotal);
                     $(".ResSubTotal", tempL).text(item.SubTotalReseller);
                     $(".Commission", tempL).text(item.SubCommission);
                     $(".Action", tempL).attr('attr-id', 'L' + tbL.rows.length);

                     $(".Action > .edittable", tempL).click(function(){
                       $('.License').val('');
                       $("#DiscountIDFormL").val('');
                       $("#DiscountIDFormL").empty();
                       $("#DiscountIDFormL").append("<option value=''>Please select</option>");
                       var Val = $(this).closest('.Action').attr('attr-id');
                       var td = $(this).closest('tr');
                       $('#field_License').focus();
                       // alert(Val);
                       // alert(td.find('.License').text());
                       // alert(td.find('.Price').text());

                       $('#LicenseIDFormL').val(td.find('.License').attr('licenseid'));
                       $('#currentRowL').val(Val);
                       var licenseid = td.find('.License').attr('licenseid');
                       var discountid = td.find('.Discount').attr('discountid');
                       self.refreshTableDiscountForLicense(param, self, licenseid, discountid);
                       $('#PriceFormL').val(td.find('.Price').text());
                       $('#ResPriceFormL').val(td.find('.ResPrice').text());
                       $('#QtyFormL').val(td.find('.Qty').text());
                       // $('#DiscountIDFormL').val(td.find('.Discount').attr('discountid'));
                       $('#PercentageFormL').val(td.find('.DPercentage').text());
                       $('#ValueFormL').val(td.find('.DValue').text());
                       $('#ComPercentageFormL').val(td.find('.CPercentage').text());
                       $('#ComValueFormL').val(td.find('.CValue').text());
                     });
                     $('.Action > .delete', tempL).click(function(){
                       var Val = $(this).closest('.Action').attr('attr-id');
                       alert(Val);
                       $(this).parents("tr").remove();
                     });

                     noL=noLicense;
                     noLicense++;
                     bodyL.append(tempL);
                   } else if(item.HardwareID) {
                     var tempH = rowH.clone();
                     tempH.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                     tempH.attr('id', 'H' + tbH.rows.length);
                     $(".No", tempH).text(noHardware);
                     $(".Hardware", tempH).text(item.HardwareName);
                      $(".Hardware", tempH).attr('licenseid', '');
                      $(".Hardware", tempH).attr('hardwareid', item.HardwareID);
                     $(".Price", tempH).text(item.Price);
                     $(".Qty", tempH).text(item.Qty);
                     $(".Discount", tempH).text(item.DiscountName);
                      $(".Discount", tempH).attr('discountid', item.DiscountID);
                     $(".Percentage", tempH).text(item.Percentage);
                     $(".Value", tempH).text(item.Value);
                     $(".SubTotal", tempH).text(item.SubTotal);
                     $(".Commission", tempH).text(item.SubCommission);
                     $(".Action", tempH).attr('attr-id', 'H' + tbH.rows.length);

                     $(".Action > .edittable", tempH).click(function(){
                       $(".Hardware").val('');
                       $("#DiscountIDFormH").empty();
                       $("#DiscountIDFormH").append("<option value=''>Please select</option>");
                       var Val = $(this).closest('.Action').attr('attr-id');
                       var td = $(this).closest('tr');
                       $('#field_Hardware').focus();
                       // alert(Val);

                       $('#HardwareIDFormH').val(td.find('.Hardware').attr('hardwareid'));
                       $('#currentRowH').val(Val);
                       var hardwareid = td.find('.Hardware').attr('hardwareid');
                       var discountid = td.find('.Discount').attr('discountid');
                       self.refreshTableDiscountForHardware(param, self, hardwareid, discountid);
                       $('#PriceFormH').val(td.find('.Price').text());
                       $('#QtyFormH').val(td.find('.Qty').text());
                       $('#PercentageFormH').val(td.find('.Percentage').text());
                       $('#ValueFormH').val(td.find('.Value').text());

                     });
                     $('.Action > .delete', tempH).click(function(){
                       var Val = $(this).closest('.Action').attr('attr-id');
                       alert(Val);
                       $(this).parents("tr").remove();
                     });

                     noH=noHardware;
                     noHardware++;
                     bodyH.append(tempH);
                   }
                 }
                 // $('#row_Hardware').remove();
                 // $('#row_License').remove();

                 $('#countRowL').val(noL); $('#countRowH').val(noH);
                 // var numRows = [noL, noH];
                 // return numRows;
               } else {
                 DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
               }
               // if(jenis == "updated") getUpdateData(param, self);
               $('#MainTable_License').DataTable({ "pagingType": "full_numbers" });
               $('#MainTable_Hardware').DataTable({ "pagingType": "full_numbers" });
             }
           });
         }, refreshTableLicense: function (param, self) {
          $F.service({
            url:self.urlControllerLicense+"getAllData",
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.length;
                var body = $("#LicenseIDFormL");
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data[i],
                      p = 0,
                      v = 0;
                  if(item.CommissionPercentage) p = item.CommissionPercentage;
                  else v = item.CommissionValue;
                  var td = "<option value='"+item.LicenseID+"' price='"+item.Price+"' resprice='"+item.ResellerPrice+"' p='"+p+"' v='"+v+"'>"+item.LicenseName+"</option>";
                  body.append(td);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              // if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }, refreshTableHardware: function (param, self) {
          $F.service({
            url:self.urlControllerHardware+"getAllData",
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.length;
                var body = $("#HardwareIDFormH");
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data[i];
                  var td = "<option value='"+item.HardwareID+"' price='"+item.Price+"'>"+item.HardwareName+"</option>";
                  body.append(td);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              // if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }, refreshTableDiscountForLicense: function (param, self, id, discountid) {
          $F.service({
            url:self.urlControllerDiscount+"getAllDataForLicense/" + id,
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.Discount.length;
                var body = $("#DiscountIDFormL");
                body.empty();
                var firstLine = "<option value=''>Please select</option>";
                body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data.Discount[i];
                  var v = '';
                  var p = '';
                  if(item.DiscountValue){
                    v = item.DiscountValue;
                  } else {
                    p = item.DiscountPercentage;
                  }
                  var td = "<option value='"+item.DiscountID+"' p='"+p+"' v='"+v+"'>"+item.DiscountName+"</option>";
                  body.append(td);
                }
                if(discountid != 0) {
                  $('#DiscountIDFormL').val(discountid);
                  // alert($('option[value="'+discountid+'"]').attr("p"));
                  // $("#PercentageFormL").val($('option[value="'+discountid+'"]').attr("p"));
                  // $("#ValueFormL").val($('option[value="'+discountid+'"]').attr("v"));
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              // if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }, refreshTableDiscountForHardware: function (param, self, id, discountid) {
          $F.service({
            url:self.urlControllerDiscount+"getAllDataForHardware/" + id,
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.Discount.length;
                var body = $("#DiscountIDFormH");
                body.empty();
                var firstLine = "<option value=''>Please select</option>";
                body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data.Discount[i];
                  var v = '';
                  var p = '';
                  if(item.DiscountValue){
                    v = item.DiscountValue;
                  } else {
                    p = item.DiscountPercentage;
                  }
                  var td = "<option value='"+item.DiscountID+"' p='"+p+"' v='"+v+"'>"+item.DiscountName+"</option>";
                  body.append(td);
                }
                if(discountid != 0) {
                  $('#DiscountIDFormH').val(discountid);
                  // alert($('option[value="'+discountid+'"]').attr("p"));
                  // $("#PercentageFormL").val($('option[value="'+discountid+'"]').attr("p"));
                  // $("#ValueFormL").val($('option[value="'+discountid+'"]').attr("v"));
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              // if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }



    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null){
                if(data.QuotationDetail[0]){
                  var item = data.QuotationDetail[0];
                  $('#QuotationNO').val(item.QuotationNO);
                  $('#BranchName').val(item.BranchName);
                  $('#UserFullName').val(item.UserFullName);
                  $('#Type').val(item.UserTypeName);
                  if(item.Status == "Waiting") {
                    $('#validationQuotation').html('<button class="btn btn-primary revisi looptemplate" onclick="updateStatus(\'Approved\','+ param.param[0] +')">Approve</button>');
                    $('#validationQuotation').append('<button class="btn btn-addon revisi looptemplate" onclick="updateStatus(\'Rejected\','+ param.param[0] +')">Reject</button>');
                  } else if(item.Status == "Requesting Invoice" || item.Status == "Closing") {
                    $('.edit').prop('hidden', 'hidden');
                  }

                  if(data["Usertype"] == "Head of Sales") {
                    $('.revisi').removeClass('looptemplate');
                    $('#finish').attr("isHeadofSales", "yes");
                  } else {
                    $('#finish').attr("isHeadofSales", "no");
                  }

                  if(data["Userid"] != item.UserID) {
                    if(data["Usertype"] != "Head of Sales") $('.edit').prop('hidden', 'hidden');
                  }
                } else DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
              }
            }
        }); //tutup service
    }

    function terikat(input, input2, diskon_id) {
        input.unbind();
        input2.unbind();

        input.keyup(function(){
          if($(this).val()) {
            input.not(this).val('');
            // $('input.terikat').not(this).removeAttr('required');
          }
        });

        input2.on("input", function(){
          diskon_id.val('');
        });
    }

    function updateStatus(sttus) { //ga kepake, gagal, jadi function nya dipanggil dari html nya di dalam <script>

        $F.service({
          type: 'post',
          url: 'quotation/actionStatus/' + post,
          data: JSON.stringify({
            Status: sttus
          }),
          success: function(d){
            if(d != null) {
              if(d.Status == 0) {
                window.location= '#/transaction/quotation';
              }
            } else {
              DZ.showAlertWarning("Something is wrong, Update failed.", "#primarycontent", 0);
            }
          }
        }); //tutup service

    }

}());
