$F.loadView(function() {
    "use strict";
    return {
       title: 'Quotation - HelloBill',
       viewtitle: 'Quotation',
       urlController: 'quotation/',
       urlControllerLicense: 'license/',
       urlControllerHardware: 'hardware/',
       urlControllerDiscount: 'discount/',
       afterLoad: function(param) {
         var self = this;
         var th = this.parent;
         var post = '';
         $('#back').attr('href','#/transaction/quotation');

         if (param.param[0]) {
           post = param.param[0];
           getUpdateData(param, self);
           self.refreshTableItem(param, self);
         } else {
           window.location = "#/transaction/quotation";
         }
         $('#topic').text('Detail ' + self.viewtitle);

         $('#Download').click(function(){
           //save as pdf dsni, lalu update status
           window.open('services/index.php/'+ self.urlController +'print_receipt/'+2, '_blank');
           var status = $('#Status').val();
           if(status == 'Approved') {
             $F.service({
               type: 'post',
               url: 'quotation/actionStatus/' + post,
               data: JSON.stringify({
                 Status: "PDF Downloaded"
               }),
               success: function(data){
                 if(data != null) {
                   if(data.Status == 0) {
                     $F.popup.show({
                         content: data.Message
                     });
                     $('.modal-content').attr("style", "width : 250px");
                     setTimeout(function() {
                         window.location = $F.config.get('baseUri') + '#/transaction/quotation';
                         $('.popup-close-button').click();
                     }, 1500);
                   } else {
                     var err = '';
                     for (var field in data.Errors) {
                         err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                     }
                     DZ.showAlertWarning(err, ".modal-footer", 0);
                   }
                 } else {
                   DZ.showAlertWarning("Something is wrong, Update failed.", ".modal-footer", 0);
                 }
               }
             }); //tutup service
           } else {
             window.location= '#/transaction/quotation';
           }
         });

       }, refreshTableItem: function (param, self) {
          $F.service({
            url:self.urlController+"getDetailData/"+param.param[0],
            type: 'GET',
            success: function(data){
                DZ.remDataTable("MainTable_License");
                DZ.remDataTable("MainTable_Hardware");
                $('.dataRow').remove();
              if(data != null) {
                var x = data.QuotationDetailbyQuotationID.length;
                var rowL = $('#row_License');
                var bodyL = $("#Body_License");
                var rowH = $('#row_Hardware');
                var bodyH = $("#Body_Hardware");
                var noL = 1;
                var noH = 1;
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i < x; i++){
                  var item = data.QuotationDetailbyQuotationID[i];

                  if(item.LicenseID) {
                    var tempL = rowL.clone();
                    tempL.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                    $(".No", tempL).text(noL);
                    $(".License", tempL).text(item.LicenseName);
                    $(".Price", tempL).text(item.Price);
                    $(".Qty", tempL).text(item.Qty);
                    if(item.DiscountID) $(".Discount", tempL).text(item.DiscountName);
                    else $(".Discount", tempL).text("Custom");
                    $(".DPercentage", tempL).text(item.Percentage);
                    $(".DValue", tempL).text(item.Value);
                    $(".SubTotal", tempL).text(item.SubTotal);
                    // $(".Commission", tempL).text(item.SubCommission);

                    noL++;
                    bodyL.append(tempL);
                  } else if(item.HardwareID) {
                    var tempH = rowH.clone();
                    tempH.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                    $(".No", tempH).text(noH);
                    $(".Hardware", tempH).text(item.HardwareName);
                    $(".Price", tempH).text(item.Price);
                    $(".Qty", tempH).text(item.Qty);
                    if(item.DiscountID) $(".Discount", tempH).text(item.DiscountName);
                    else $(".Discount", tempH).text("Custom");
                    $(".Percentage", tempH).text(item.Percentage);
                    $(".Value", tempH).text(item.Value);
                    $(".SubTotal", tempH).text(item.SubTotal);
                    // $(".Commission", tempH).text(item.SubCommission);

                    noH++;
                    bodyH.append(tempH);
                  }
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              // if(jenis == "updated") getUpdateData(param, self);
              $('#MainTable_License').DataTable({ "pagingType": "full_numbers" });
              $('#MainTable_Hardware').DataTable({ "pagingType": "full_numbers" });
            }
          });
        }

      };


      function getUpdateData(param, self) {
          $F.service({
              type: 'get',
              url: self.urlController + 'getUpdateData/' + param.param[0],
              success: function(data) {
                  if(data.QuotationDetail) {
                      var item = data.QuotationDetail[0];
                      $('#QuotationNO').val(item.QuotationNO);
                      $('#BranchName').val(item.BranchName);
                      $('#Address').val(item.Address);
                      $('#UserFullName').val(item.UserFullName);
                      $('#GrandTotal').val(item.GrandTotal);
                      $('#Status').val(item.Status);
                  } else DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
          });
      }
  }());
