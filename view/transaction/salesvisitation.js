$F.loadView(function () {
  "use strict";

  return {
   title: 'Sales Visitation - HelloBill',
   viewtitle: 'Sales Visitation',
   urlController: 'salesvisitation/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var sf = this;
     sf.refreshTable(sf);
   //  checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    // $('.addbutton').attr('href','#/visitation/action-visitation');
   //  $('.branch').attr('href','#/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
  }, refreshTable: function (self) {
    $F.service({
        url:self.urlController+"getAllData",
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
            console.log(data);
          if(data.SalesVisitation != null) {
            var x = data.SalesVisitation.length;
            var row = $("#row");
            var body = $("#Body");
            var usr = data["UserID"];
            var usrtype = data["UserTypeName"];
            // alert(usr);

            for(var i = 0; i < x; i++){
              var salesvisitation = data.SalesVisitation[i];
              if(usrtype != "Sales" && usrtype != "Reseller" || usr == salesvisitation.UserID) {
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                $(".Number", temp).text(salesvisitation.SalesVisitationNO);
                $(".BranchName", temp).text(salesvisitation.BranchName);
                $(".PIC", temp).text(salesvisitation.UserFullName);
                $(".StartDate", temp).text(salesvisitation.StartDate);
                $(".LastStatus", temp).text(salesvisitation.LastStatus);
                $(".BriefDesc", temp).text(salesvisitation.BriefDescription);
                $(".GPS", temp).text(salesvisitation.GPS);
                if(usr == salesvisitation.UserID) {
                  $('.act', temp).append('<option value="1">Edit</option>');
                  if(salesvisitation.LastStatus == "Closing") {
                    $('.act', temp).append('<option value="4" class="createQuotation">Create Quotation</option>');
                  }
                }
                $('.act', temp).attr("attr-id", salesvisitation.SalesVisitationID);
                body.append(temp);

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  if(Val == 3)
                    window.location.href = '#/transaction/salesvisitation/detail.'+attrID;
                  else if(Val == 1)
                    window.location.href = '#/transaction/salesvisitation/action-salesvisitation.'+attrID;
                  else if(Val == 2) { //not used
                    // var ser = $F.serialize('#action');
                    // console.log(ser);
                    $F.service({
                      type: 'post',
                      url: 'salesvisitation/deleteUpdateData/'+attrID,
                      data: JSON.stringify({
                        id:attrID
                      }),
                      success: function(d){
                        if(d != null) {
                          if(d.Status == 0) {
                            location.reload();
                          }
                        } else {
                          DZ.showAlertWarning("Something is wrong, data is not deleted", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                } else if(Val == 4) {
                    $F.service({
                      type: 'post',
                      url: 'quotation/action',
                      data: JSON.stringify({
                        id:attrID
                      }),
                      success: function(d){
                        if(d != null) {
                          if(d.Status == 0) {
                            // alert(d.QuotationID);
                            window.location.href = '#/transaction/quotation/action-quotation.'+ d.QuotationID; // ID Quot yg dilempar dari fungsi ini
                          }
                        } else {
                          DZ.showAlertWarning("Something is wrong, Quotation is not created", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                  }
                });
              }
            } //akhir loop for
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({"pagingType": "full_numbers"});
        }
    })
  }
}

function getUpdateData(arg, self) { //obsolete
  $F.service({
      url:self.urlController+"getAllData/"+arg.param[0],
      type: 'GET',
      success: function(data){
        if(data != null) {
          if(data) {
            var x = data.length;
            var row = $("#row");
            var body = $("#Body");
            if(data["UserID"] != data[0].UserID || data[0].LastStatus == "Closing") {
              $('.edit').addClass("looptemplate");
            }
            $("#topic").text(data[0].SalesVisitationNO + ', ' + data[0].BranchName);
            // $("#Topic").text(data[0].BrandName);
            // document.getElementById("Topic").innerHTML(data[0].BrandName);
            for(var i = 0; i < x; i++){
              var temp = row.clone();
              temp.removeAttr("id").removeClass("looptemplate");
              $(".Date", temp).text(data[i].Date);
              $(".Description", temp).text(data[i].Description);
              $(".Status", temp).text(data[i].Status);
              $('.act', temp).attr("attr-id", data[i].SalesVisitationDetailID);
              body.append(temp);

              $('.act', temp).change(function () {
                var Val = $(this).val();
                var attrID = $(this).attr("attr-id");
                if(Val == 1)
                  window.location.href = '#/transaction/salesvisitation/detail/action-detail.'+arg.param[0]+'/'+attrID;
                else if(Val == 2) {
                  // var ser = $F.serialize('#action');
                  // console.log(ser);
                  $F.service({
                    type: 'post',
                    url: 'detail/deleteUpdateData/'+attrID,
                    data: JSON.stringify({
                      id:attrID
                    }),
                    success: function(d){
                      if(d != null) {
                        if(d.Status == 0) {
                          // window.location= '#/brand';
                          location.reload();
                        }
                      } else {
                        DZ.showAlertWarning("Something is wrong, row is not deleted", "#primarycontent", 0);
                      }
                    }
                  }); //tutup service
                }
              });

            }
          } else {
            DZ.showAlertWarning("It looks like you don't have branch yet. Insert some.", "#primarycontent", 0);
          }
        } else {
          DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
        }
      }
  })

}

    function checkPIC(){

    }
//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
