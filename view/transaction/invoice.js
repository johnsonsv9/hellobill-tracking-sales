$F.loadView(function () {
  "use strict";

  return {
   title: 'Invoice - HelloBill',
   viewtitle: 'Invoice',
   urlController: 'invoice/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var sf = this;
     sf.refreshTable(sf);
   //  checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    // $('.addbutton').attr('href','#/transaction/quotation/action-quotation');
   //  $('.branch').attr('href','#/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
  }, refreshTable: function (self) {
    $F.service({
        url:self.urlController+"getAllData",
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data.Invoice != null) {
            var x = data.Invoice.length;
            var row = $("#row");
            var body = $("#Body");
            var usr = data["UserID"];
            var usrtype = data["UserTypeName"];

            for(var i = 0; i < x; i++){
              var item = data.Invoice[i];
              if(usrtype != "Sales" && usrtype != "Reseller" || usr == item.UserID) {
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                $(".InvNo", temp).text(item.InvoiceNO);
                $(".QuoNo", temp).text(item.QuotationNO);
                $(".BranchName", temp).text(item.BranchName);
                $(".PIC", temp).text(item.UserFullName);
                $(".GrandTotal", temp).text(item.GrandTotal);
                $(".GrandTotalRes", temp).text(item.GrandTotalReseller);
                $(".Commission", temp).text(item.Commission);
                $(".DateDeliv", temp).text(item.DeliveryDate);
                $(".DateDue", temp).text(item.DueDate);
                $(".Status", temp).text(item.Status);
                $(".DatePaid", temp).text(item.PaidDate);
                if(item.Status == "New" || item.Status == "Sent" || item.Status == "Pending" ||  item.Status == "Unpaid" ||item.Status == "Paid") {
                  if(item.Status != "New") {
                    $('.act', temp).append('<option value="3">Payment</option>');
                  }
                  if(item.UserTypeName == "Sales") {
                    $('.act', temp).append('<option value="1" class="preview">View Detail</option>');
                  } else if(item.UserTypeName == "Reseller") {
                    $('.act', temp).append('<option value="2" class="preview">View Detail</option>');
                    // if(usr == item.UserID)
                    //   $('.act', temp).append('<option value="4" class="preview">View Invoice Reseller</option>');
                  }
                  // if(item.Status == "Downloaded")
                  //   $('.act', temp).append('<option value="2" class="looptemplate afterApp">Send Invoice</option>');
                }

                // else if(item.Status == "Requesting Invoice" || item.Status == "Closing") {
                //   $('.act', temp).append('<option value="3" class="looptemplate InvRes">Create Invoice</option>');
                //   $('.act', temp).append('<option value="4" class="looptemplate genInv">Generate Invoice</option>');
                // }
                $('.act', temp).attr("attr-id", item.InvoiceID);
                body.append(temp);

                // if(data[0] == "Finance") {
                //   $('.afterApp').removeClass("looptemplate");
                // }

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  if(Val == 3)
                    window.location.href = '#/transaction/invoice/payment.'+attrID;
                  else if(Val == 1)
                    window.location.href = '#/transaction/invoice/preview.'+attrID;
                  else if(Val == 2)
                    window.location.href = '#/transaction/invoice/preview.'+attrID+'/RES';
                  else if(Val == 4)
                    window.location.href = '#/transaction/invoice/invoicereseller.'+attrID;
                  // else if(Val == 2) {
                  //   $F.service({
                  //     type: 'post',
                  //     url: 'quotation/actionStatus/'+attrID,
                  //     data: JSON.stringify({
                  //       Status: 'Requesting Invoice'
                  //     }),
                  //     success: function(d){
                  //       if(d.Status == 0) {
                  //         location.reload();
                  //       } else {
                  //         DZ.showAlertWarning("Username or password is wrong", "#error", 0);
                  //       }
                  //     }
                  //   }); //tutup service
                  // } else if(Val == 3) {
                  //   //disini create invoice utk reseller
                  // } else if(Val == 4) {
                  //   //disini untuk generate invoice, setelah itu status berubah jadi closing
                  // }
                });
              }
            } //akhir loop for
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
  }
}

//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
