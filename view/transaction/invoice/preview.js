$F.loadView(function() {
    "use strict";
    return {
       title: 'Preview Invoice - HelloBill',
       viewtitle: 'Invoice',
       urlController: 'invoice/',
       urlControllerQuotation: 'quotation/',
       urlControllerLicense: 'license/',
       urlControllerHardware: 'hardware/',
       urlControllerDiscount: 'discount/',
       afterLoad: function(param) {
         var self = this;
         var th = this.parent;
         var post = '';
         var routeback = '';
         var is_reseller = '/';

         if (param.param[0]) {
           post = param.param[0];
           if(param.param[1]) {
             if(param.param[1] == 'CUST') {
               routeback = 'reseller';
               is_reseller = 'Reseller/'
             }
           }
           getUpdateData(param, self, is_reseller);
           // self.refreshTableItem(param, self);
         } else {
           window.location = "#/transaction/invoice";
         }

         $('#back').attr('href','#/transaction/invoice' + routeback);

         $('#topic').text('Detail ' + self.viewtitle);

         //Function for adding days to date, and reformatting those date to yyyy-mm-dd format
         Date.prototype.addDays = function(days) {
           var dat = new Date(this.valueOf());
           dat.setDate(dat.getDate() + days);
           return dat;
         }
         Date.prototype.reFormat = function() {
           var twoDigitMonth = ((this.getMonth()+1) < 10) ? '0'+(this.getMonth()+1) : (this.getMonth()+1);
           var currentDate = this.getFullYear() + "-" + twoDigitMonth + "-" + this.getDate();
           return currentDate;
         }

         $('#Download').click(function(){
           //save as pdf dsni, lalu update status mnjdi sent (jika new)
           var status = $('#Status').val();
           var is_Reseller = '/',
               is_reseller = '';
           if(param.param[1] == 'CUST') {is_Reseller = 'Reseller/'; is_reseller = 'reseller';}
           if(status == 'Pending') {
             $F.service({
               type: 'post',
               url: 'invoice/actionStatus' + is_Reseller + post,
               data: JSON.stringify({
                 Status: "Sent"
               }),
               success: function(d){
                 if(d != null) {
                   if(d.Status == 0) {
                     $F.popup.show({
                         content: data.Message
                     });
                     $('.modal-content').attr("style", "width : 250px");
                     setTimeout(function() {
                         window.location = $F.config.get('baseUri') + '#/transaction/invoice' + is_reseller;
                         $('.popup-close-button').click();
                     }, 1500);
                   } else {
                     var err = '';
                     for (var field in data.Errors) {
                         err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                     }
                     DZ.showAlertWarning(err, ".modal-footer", 0);
                   }
                 } else {
                   DZ.showAlertWarning("Something is wrong, Update failed.", ".moodal-footer", 0);
                 }
               }
             }); //tutup service
           } else {
             $F.popup.show({
                 content: data.Message
             });
             $('.modal-content').attr("style", "width : 250px");
             setTimeout(function() {
                 window.location = $F.config.get('baseUri') + '#/transaction/invoice' + is_reseller;
                 $('.popup-close-button').click();
             }, 1500);
           }
         });

       }, refreshTableItem: function (param, self) {
          $F.service({
            // url:self.urlControllerQuotation+"getDetailData/"+param.param[0],
            url:self.urlControllerQuotation+"getDetailData/"+param,
            type: 'GET',
            success: function(data){
                DZ.remDataTable("MainTable_License");
                DZ.remDataTable("MainTable_Hardware");
                $('.dataRow').remove();
              if(data != null) {
                var x = data.QuotationDetailbyQuotationID.length;
                var rowL = $('#row_License');
                var bodyL = $("#Body_License");
                var rowH = $('#row_Hardware');
                var bodyH = $("#Body_Hardware");
                var noL = 1;
                var noH = 1;
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i < x; i++){
                  var item = data.QuotationDetailbyQuotationID[i];

                  if(item.LicenseID) {
                    var tempL = rowL.clone();
                    tempL.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                    $(".No", tempL).text(noL);
                    $(".License", tempL).text(item.LicenseName);
                    $(".Price", tempL).text(item.Price);
                    $(".Qty", tempL).text(item.Qty);
                    $(".Discount", tempL).text(item.DiscountName);
                    $(".DPercentage", tempL).text(item.Percentage);
                    $(".DValue", tempL).text(item.Value);
                    if(item.ResellerSubTotal == 0)
                      $(".SubTotal", tempL).text(item.SubTotal);
                    else
                      $(".SubTotal", tempL).text(item.ResellerSubTotal);
                    // $(".Commission", tempL).text(item.SubCommission);

                    noL++;
                    bodyL.append(tempL);
                  } else if(item.HardwareID) {
                    var tempH = rowH.clone();
                    tempH.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                    $(".No", tempH).text(noH);
                    $(".Hardware", tempH).text(item.HardwareName);
                    $(".Price", tempH).text(item.Price);
                    $(".Qty", tempH).text(item.Qty);
                    $(".Discount", tempH).text(item.DiscountName);
                    $(".Percentage", tempH).text(item.Percentage);
                    $(".Value", tempH).text(item.Value);
                    $(".SubTotalH", tempH).text(item.SubTotal);
                    // $(".Commission", tempH).text(item.SubCommission);

                    noH++;
                    bodyH.append(tempH);
                  }
                }
                // $('#row_License').remove();
                // $('#row_Hardware').remove();
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              $('#MainTable_License').DataTable({ "pagingType": "full_numbers" });
              $('#MainTable_Hardware').DataTable({ "pagingType": "full_numbers" });
            }
          });
        }

      };


      function getUpdateData(param, self, is_reseller) {
          $F.service({
              type: 'get',
              url: self.urlController + 'getUpdateData' + is_reseller + param.param[0],
              success: function(data) {
                if(data.InvoiceDetail || data.InvoiceResellerDetail) {
                  if(data.InvoiceDetail) var item = data.InvoiceDetail[0];
                  else var item = data.InvoiceResellerDetail[0];
                  if(data["Usertype"] != "Finance" && is_reseller != "Reseller/")
                    $("#Download").addClass("looptemplate");

                  if(item.Status == "New") {
                    $("#Download").text("Download and Send");
                  } else {
                    $("#Download").text("Download");
                  }

                  // $('#QuotationID').val(item.QuotationID);
                  if(item.InvoiceNO) $('#InvoiceNO').val(item.InvoiceNO);
                  else $('#InvoiceNO').val(item.InvoiceResellerNO);
                  // if(item.UserTypeName == "Sales") {
                  //   $('.SubTotal').removeClass("looptemplate");
                  //   $('.ResSubTotal').addClass("looptemplate");
                  //   $('#Total').val(item.GrandTotal);
                  // }
                  if(param.param[1]) {
                    if(param.param[1] == 'RES') {
                      // $('.SubTotal').addClass("looptemplate");
                      // $('.ResSubTotal').removeClass("looptemplate");
                      $('#Total').val(item.GrandTotalReseller);
                    } else {
                      // $('.SubTotal').removeClass("looptemplate");
                      // $('.ResSubTotal').addClass("looptemplate");
                      $('#Total').val(item.GrandTotal);
                      $('.form-group.sales_only').addClass("looptemplate"); //kalau reseller ga ada due date
                    }
                  } else {
                    // $('.SubTotal').removeClass("looptemplate");
                    // $('.ResSubTotal').addClass("looptemplate");
                    $('#Total').val(item.GrandTotal);
                  }
                  $('#BranchName').val(item.BranchName);
                  $('#UserFullName').val(item.UserFullName);
                  $('#Status').val(item.Status);

                  // alert(item.DeliveryDate);
                  // alert(item.DueDate);
                  if(item.DeliveryDate) {
                    $('#DeliveryDate').val(item.DeliveryDate);
                    $('#DueDate').val(item.DueDate);
                  } else {
                    var fullDate = new Date();
                    // alert('a');
                    $('#DeliveryDate').val(fullDate.reFormat());
                    $('#DueDate').val(fullDate.addDays(30).reFormat());
                  }

                  self.refreshTableItem(item.QuotationID, self);
                } else {
                  DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
                }
              }
          });
      }
  }());
