$F.loadView(function () {
  "use strict";

  return {
   title: 'Payment - HelloBill',
   viewtitle: 'Payment',
   urlController: 'invoice/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
    var sf = this;
    // checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    $('.addbutton').attr('href','#/transaction/invoice/payment/action-payment.'+ arg.param[0]);
    $('#back').attr('href','#/transaction/invoice');
    sf.refreshTable(sf, arg);
    if(arg.param[0]) {
      getHeaderData(arg, sf);
    } else {
      window.location.href = '#/transaction/invoice';
    }
    // alert(arg.param[0]);
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
 }, refreshTable: function (self, arg) {
    $F.service({
        url:self.urlController+"getDetailData/"+arg.param[0],
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data != null) {
            if(data.InvoicePayment != null) {
              var x = data.InvoicePayment.length;
              var row = $("#row");
              var body = $("#Body");
              // $("#Topic").text(data[0].BrandName);
              // document.getElementById("Topic").innerHTML(data[0].BrandName);
              for(var i = 0; i < x; i++){
                var temp = row.clone();
                var item = data.InvoicePayment[i];
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                $(".Date", temp).text(item.Date);
                $(".Amount", temp).text(item.Paid);
                if(item.TransferProof != "" && item.TransferProof != null) {
                  $(".Picture", temp).attr('src', item.TransferProof).removeClass("hidden").attr('download', item.TransferProof);
                  $(".Picture", temp).click(function(){
                      window.open($(this).attr("download"), '_blank');
                  });
                  // $(".PictureHref", temp).attr('href', item.TransferProof).attr('download', item.TransferProof);
                  $('.image').removeClass("hidden");
                }

                $('.act', temp).attr("attr-id", item.InvoicePaymentID);
                body.append(temp);

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  if(Val == 1)
                    window.location.href = '#/transaction/invoice/payment/action-payment.'+arg.param[0]+'/'+attrID;
                  else if(Val == 2) {
                    // download gmbar bukti bayar
                    window.open($(this).attr("download"), '_blank');
                    $(this).val(0);
                  }
                });

              }
            }
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
            }
}


    function getHeaderData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data.InvoiceDetail) {
                var item = data.InvoiceDetail[0];
                $("#topic").text('Histori Pembayaran, ' + item.BranchName);
                if(data["Usertype"] != "Finance" || item.Status == "Pending" || item.Status == "Expired" || item.Status == "Paid") {
                    // if ( $.fn.dataTable.isDataTable('#MainTable_category') ) {
                    //   $('#MainTable_category').DataTable().destroy();
                    // }
                  $('.edit').addClass("hidden");
                  // $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
                  if(item.Status == "Paid") {
                    $('.receipt').removeClass("hidden");
                    $('.receipt').attr('href', "bla-bla"); // link utk download pdf bukti tanda lunas
                  }
                }
                $('#InvoiceNO').val(item.InvoiceNO);
                $('#BranchName').val(item.BranchName);
                $('#UserFullName').val(item.UserFullName);
                if(item.UserTypeName == "Sales")
                  $('#Total').val(item.GrandTotal);
                else if(item.UserTypeName == "Reseller")
                  $('#Total').val(item.GrandTotalReseller);
                $('#Status').val(item.Status);
              }
            }
        });
    }

    // function checkPermission(){
    //     $F.service({
    //         url:'init/getValidPermissionSingle/payment-method',
    //         type: 'GET',
    //         success: function(data){
    //             if(data.data.Permissions[0].HavePermission === false)
    //                 window.location.href = $F.config.get('baseUri') + 'forbidden.html';
    //         }
    //     })
    //  };
}());
