$F.loadView(function() {
    "use strict";
    return {
       title: 'Payment - HelloBill',
       viewtitle: 'Payment',
       urlController: 'invoice/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = ''; //menyimpan branch ID
            var routeback = ''; //menyimpan path /branch.brandID atau string kosong
            var txt = '';

            $("#Files").change(function() {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#PhotoCategory')
                            .attr('src', e.target.result);
                    };
                    reader.readAsDataURL(this.files[0]);
                    $('.close').removeAttr('delete').removeClass('hidden');
                }
            });

      //bwt munculin judul
            if (param.param[1]) {
                $('#topic').text('Update ' + self.viewtitle);
                getUpdateData(param, self);
                post = param.param[1];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }

      //Mengatur tombol back
            if(param.param[0]) {
              routeback = '/payment.' + param.param[0];
            }
            $("#back").attr('href', '#/transaction/invoice' + routeback);

      //Datepicker
            Date.prototype.addDays = function(days) {
              var dat = new Date(this.valueOf());
              dat.setDate(dat.getDate() + days);
              return dat;
            }
            Date.prototype.reFormat = function() {
              var twoDigitMonth = ((this.getMonth()+1) < 10) ? '0'+(this.getMonth()+1) : (this.getMonth()+1);
              var currentDate = this.getFullYear() + "-" + twoDigitMonth + "-" + this.getDate();
              return currentDate;
            }

            var currentDate = new Date();
            $('#Date').datepicker({
              format: 'yyyy-mm-dd',
              todayHighlight: true,
              // startDate: currentDate.reFormat(),
              // minDate: currentDate.reFormat()
            });

            $('.close').click(function() {
               $('#PhotoCategory').attr('src', 'assets/images/default-item.jpg');
               $("#Files").val("");
               $(this).attr('delete', 'true');
               $('.close').addClass('hidden');
           });

           $('#PhotoCategory').click(function() {
             $('#Files').click();
           });

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  var optiondata;
                  if($('#Files').val().length != 0) {
                    var maxSize = 2 * 1024 * 1024;
                    var fileData = [];
                    var myFile = document.getElementById('Files');
                    var col = [myFile];
                    var f = col[0];
                    if (f.files[0].size > maxSize) {
                      var report = 'Your file is more than 2 MB\nYour file is ' + f.files[0].size + ' bytes';
                      DZ.showAlertWarning(report, "#action :submit", 0);
                      return false;
                    }
                    self.readFile(f, function(a, d) {
                        fileData.push(d);
                        optiondata = {
                            files: fileData,
                            data: ser,
                            id: param.param[0],
                            delete: $('.close').attr('delete')
                        }; console.log(JSON.stringify({optiondata}));
                        $F.service({
                            type: 'post',
                            url: self.urlController + 'actionPayment/' + param.param[0],
                            data: JSON.stringify({
                                optiondata
                            }),
                            success: function(data) {
                                if (data.Status == 0) {
                                    $F.popup.show({
                                        content: data.Message
                                    });
                                    $('.modal-content').attr("style", "width : 250px");
                                    setTimeout(function() {
                                        window.location = $F.config.get('baseUri') + '#/transaction/invoice' + routeback;
                                        $('.popup-close-button').click();
                                    }, 1500);
                                } else {
                                    var err = '';
                                    // if (data.data.Message == 'Invalid Input') {
                                        for (var field in data.Errors) {
                                            err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                                        }
                                    // } else {
                                    //     err = data.data.errors;
                                    // }
                                    DZ.showAlertWarning(err, ".modal-footer", 0);
                                }
                            }
                        }); //tutup service
                    });
                  } else {
                    optiondata = {
                        data: ser,
                        id: param.param[0],
                        delete: $('.close').attr('delete')
                    };
                    $F.service({
                        type: 'post',
                        url: self.urlController + 'actionPayment/' + param.param[0],
                        data: JSON.stringify({
                            optiondata
                        }),
                        success: function(data) {
                            if (data.Status == 0) {
                                $F.popup.show({
                                    content: data.Message
                                });
                                $('.modal-content').attr("style", "width : 250px");
                                setTimeout(function() {
                                    window.location = $F.config.get('baseUri') + '#/transaction/invoice' + routeback;
                                    $('.popup-close-button').click();
                                }, 1500);
                            } else {
                                var err = '';
                                // if (data.data.Message == 'Invalid Input') {
                                    for (var field in data.Errors) {
                                        err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                                    }
                                // } else {
                                //     err = data.data.errors;
                                // }
                                DZ.showAlertWarning(err, ".modal-footer", 0);
                            }
                        }
                    }); //tutup service
                  }



               //    var ser = $F.serialize('#action');
               //    $F.service({
               //      type: 'post',
               //      url: self.urlController+'action',
               //      data: JSON.stringify({"data":ser}),
               //      success: function(data){
               //        if (data.data.Message == 'Success') {
               //          $F.popup.show({content:data.Messages});
               //          $('.modal-content').attr("style", "width : 250px");
               //             setTimeout(function () {
               //                 window.location = $F.config.get('baseUri')+'#/user/shift';
               //                 $('.popup-close-button').click();
               //             }, 1500);
               //        } else {
               //        var err = '';
               //        for(var field in data.errors) {
               //         err += data.errors[field] + '</br>';
               //       }
               //       DZ.showAlertWarning(err, "#action :submit", 0);
               //            }
               //     }
               // });
                });
        },
        readFile(el, afterRead) {
            if (!window.FileReader) {
                afterRead(false, null);
                return false;
            }
            var read = new window.FileReader();
            read.onload = (function(after, f) {
                return function(e) {
                    var output = {
                        data: window.btoa(e.target.result),
                        name: f.name,
                        size: f.size,
                        type: f.type
                    }
                    after(true, output);
                }
            })(afterRead, el.files[0]);

            if (read.readAsBinaryString) {
                read.readAsBinaryString(el.files[0]);
            } else {
                read.readAsArrayBuffer(el.files[0]);
            }
            return true;
        }
        // ,refreshTable: function (self) {
        //   $F.service({
        //     url:self.urlControllerBrand+"getAllData",
        //     type: 'GET',
        //     success: function(data){
        //       var x = data.length;
        //       var body = $("#BrandID");
        //       for(var i = 0; i<x; i++){
        //         var item = data[i];
        //         var td = "<option value='"+item.BrandID+"'>"+item.BrandName+"</option>";
        //         body.append(td);
        //       }
        //     }
        //   });
        // }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateDetailData/' + param.param[1],
            success: function(data) {
              if(data != null) {
                var item = data.InvoicePaymentDetail[0];
                $('#InvoicePaymentID').val(item.InvoicePaymentID);
                $('#Date').val(item.Date);
                $('#Amount').val(item.Paid);
                if(item.TransferProof != "" && item.TransferProof != null){
                  $('#PhotoCategory').attr('src', item.TransferProof);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
