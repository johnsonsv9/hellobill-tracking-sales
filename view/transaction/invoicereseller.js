$F.loadView(function () {
  "use strict";

  return {
   title: 'Invoice Reseller - HelloBill',
   viewtitle: 'Invoice Reseller',
   urlController: 'invoice/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var sf = this;
     sf.refreshTable(sf);
   //  checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    // $('.addbutton').attr('href','#/transaction/quotation/action-quotation');
   //  $('.branch').attr('href','#/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
  }, refreshTable: function (self) {
    $F.service({
        url:self.urlController+"getAllDataReseller",
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data.InvoiceReseller != null) {
            var x = data.InvoiceReseller.length;
            var row = $("#row");
            var body = $("#Body");
            var usrtype = data["Usertype"];
            var usr = data["UserID"];

            for(var i = 0; i < x; i++){
              var item = data.InvoiceReseller[i];
              if(usrtype != "Sales" && usrtype != "Reseller" || usr == item.UserID) {
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                $(".InvResNo", temp).text(item.InvoiceResellerNO);
                $(".QuoNo", temp).text(item.QuotationNO);
                $(".BranchName", temp).text(item.BranchName);
                $(".PIC", temp).text(item.UserFullName);
                $(".GrandTotal", temp).text(item.GrandTotal);
                $(".GrandTotalRes", temp).text(item.GrandTotalReseller);
                $(".Profit", temp).text(item.GrandTotal - item.GrandTotalReseller);
                $(".DateDeliv", temp).text(item.DeliveryDate);
                $(".Status", temp).text(item.Status);
                if(item.Status == "Pending" || item.Status == "Sent" || item.Status == "Paid") {
                  $('.act', temp).append('<option value="2" class="updateStatus">View Detail</option>');
                  if(usr == item.UserID) {
                    if(item.Status != "Paid")
                      $('.act', temp).append('<option value="1" class="updateStatus">Update Payment</option>');
                  }
                }

                // if(usr == item.UserID) {
                //   $('.updateStatus').removeClass('looptemplate');
                // }

                $('.act', temp).attr("attr-id", item.InvoiceResellerID);
                $('.act', temp).attr("quo-id", item.QuotationID);
                body.append(temp);

                // if(data[0] == "Finance") {
                //   $('.afterApp').removeClass("looptemplate");
                // }

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  var quoID = $(this).attr("quo-id");
                  if(Val == 1) {
                      $F.service({
                        type: 'post',
                        url: 'invoice/actionStatusReseller/'+attrID,
                        data: JSON.stringify({
                          Status: 'Paid'
                        }),
                        success: function(d){
                          if(d.Status == 0) {
                            location.reload();
                          } else {
                            DZ.showAlertWarning("Username or password is wrong", "#primarycontent", 0);
                          }
                        }
                      }); //tutup service
                  } else if(Val == 2) {
                    window.location.href = '#/transaction/invoice/preview.'+attrID+'/CUST';
                  }
                  // else if(Val == 2) {
                  //   $F.service({
                  //     type: 'post',
                  //     url: 'quotation/actionStatus/'+attrID,
                  //     data: JSON.stringify({
                  //       Status: 'Requesting Invoice'
                  //     }),
                  //     success: function(d){
                  //       if(d.Status == 0) {
                  //         location.reload();
                  //       } else {
                  //         DZ.showAlertWarning("Username or password is wrong", "#error", 0);
                  //       }
                  //     }
                  //   }); //tutup service
                  // } else if(Val == 3) {
                  //   //disini create invoice utk reseller
                  // } else if(Val == 4) {
                  //   //disini untuk generate invoice, setelah itu status berubah jadi closing
                  // }
                });
              }
            } //akhir loop for
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
  }
}

//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
