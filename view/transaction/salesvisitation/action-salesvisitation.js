$F.loadView(function() {
    "use strict";
    return {
       title: 'Sales Visitation - HelloBill',
       viewtitle: 'Sales Visitation',
       urlController: 'salesvisitation/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post="";

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                getUpdateData(param, self);
                post = param.param[0];
            } else {
                $('#topic').text('Add ' + self.viewtitle);
            }

      //Memastikan semua data telah dimasukkan
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  console.log(ser);
                  $F.service({
                    type: 'post',
                    url: 'salesvisitation/action',
                    data: JSON.stringify(ser),
                    success: function(data){
                      if(data != null) {
                        if(data.Status == 0) {
                          $F.popup.show({
                              content: data.Message
                          });
                          $('.modal-content').attr("style", "width : 250px");
                          setTimeout(function() {
                              window.location = $F.config.get('baseUri') + '#/transaction/salesvisitation';
                              $('.popup-close-button').click();
                          }, 1500);
                        } else {
                          var err = '';
                          for (var field in data.Errors) {
                              err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                          }
                          DZ.showAlertWarning(err, ".modal-footer", 0);
                        }
                      } else {
                        DZ.showAlertWarning("Something is wrong, Update failed.", ".modal-footer", 0);
                      }
                    }
                  }); //tutup service

                });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
                if(data != null) {
                    var item = data.SalesVisitationDetail[0];
                    $('#SalesVisitationID').val(item.SalesVisitationID);
                    $('#BranchID').val(item.BranchID);
                    $('#UserID').val(item.UserID);
                    $('#SalesVisitationNO').val(item.SalesVisitationNO);
                    $('#BranchName').val(item.BranchName);
                    $('#PIC').val(item.UserFullName);
                    $('#StartDate').val(item.StartDate);
                    $('#LastStatus').val(item.LastStatus);
                    $('#BriefDescription').val(item.BriefDescription);
                    $('#GPS').val(item.GPS);
                } else DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
            }
        });
    }
}());
