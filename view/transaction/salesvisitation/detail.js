$F.loadView(function () {
  "use strict";

  return {
   title: 'Detail Sales Visitation - HelloBill',
   viewtitle: 'Detail Sales Visitation',
   urlController: 'detail/',
   urlControllerHeader: 'salesvisitation/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
    var sf = this;
    var imgCount = 0;
    // checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    $('.addbutton').attr('href','#/transaction/salesvisitation/detail/action-detail.'+ arg.param[0]);
    $('#back').attr('href','#/transaction/salesvisitation');
    sf.refreshTable(sf, arg);

    if(arg.param[0]) {
      getHeaderData(arg, sf);
    } else {
      window.logation = "#/transaction/salesvisitation";
    }

    $("#Files").change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#PhotoCategory')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
            $('.close').removeAttr('delete').removeClass('hidden');
        }
    });

    $('#AddPhoto').click(function() {
      $('#Files').click();
    });

    $('.close').click(function() {
       $('#PhotoCategory').attr('src', 'assets/images/default-item.jpg');
       $("#Files").val("");
       $(this).attr('delete', 'true');
       $('.close').addClass('hidden');
   });

    // alert(arg.param[0]);
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
 }, refreshTable: function (self, arg) {
    $F.service({
        url:self.urlController+"getAllData/"+arg.param[0],
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data != null) {
            if(data.SalesVisitationDetailbySalesID) {
              var x = data.SalesVisitationDetailbySalesID.length;
              var row = $("#row");
              var body = $("#Body");

              for(var i = 0; i < x; i++){
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                var detail = data.SalesVisitationDetailbySalesID[i];
                $(".Date", temp).text(detail.Date);
                $(".Description", temp).text(detail.Description);
                $(".Status", temp).text(detail.Status);
                $('.act', temp).attr("attr-id", detail.SalesVisitationDetailID);
                body.append(temp);

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  if(Val == 1)
                    window.location.href = '#/transaction/salesvisitation/detail/action-detail.'+arg.param[0]+'/'+attrID;
                  else if(Val == 2) { // not used
                    // var ser = $F.serialize('#action');
                    // console.log(ser);
                    $F.service({
                      type: 'post',
                      url: 'detail/deleteUpdateData/'+attrID,
                      data: JSON.stringify({
                        id:attrID
                      }),
                      success: function(d){
                        if(d != null) {
                          if(d.Status == 0) {
                            // window.location= '#/brand';
                            location.reload();
                          }
                        } else {
                          DZ.showAlertWarning("Something is wrong, row is not deleted", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                  }
                });

              }
            }
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
            }
}

    // function checkPermission(){
    //     $F.service({
    //         url:'init/getValidPermissionSingle/payment-method',
    //         type: 'GET',
    //         success: function(data){
    //             if(data.data.Permissions[0].HavePermission === false)
    //                 window.location.href = $F.config.get('baseUri') + 'forbidden.html';
    //         }
    //     })
    //  };

    function getHeaderData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlControllerHeader + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data.SalesVisitationDetail != null){
                if(data.SalesVisitationDetail[0]){
                  var item = data.SalesVisitationDetail[0];
                  if(data["UserID"] != item.UserID || item.LastStatus == "Closing") {
                    $('.edit').addClass("hidden");
                  }

                  $("#topic").text(item.SalesVisitationNO + ', ' + item.BranchName);
                  // $("#Topic").text(data[1].BrandName);
                  // document.getElementById("Topic").innerHTML(data[1].BrandName);
                  // $('#QuotationNO').val(item.QuotationNO);
                  // $('#BranchName').val(item.BranchName);
                  // $('#UserFullName').val(item.UserFullName);
                  // $('#Type').val(item.UserTypeName);
                } else {
                  DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        }); //tutup service
    }

}());
