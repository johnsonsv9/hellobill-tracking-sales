$F.loadView(function() {
    "use strict";
    return {
       title: 'Detail Sales Visitation - HelloBill',
       viewtitle: 'Detail Sales Visitation',
       urlController: 'detail/',
       urlControllerBrand: 'brand/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = ''; //menyimpan branch ID
            var routeback = ''; //menyimpan path /branch.brandID atau string kosong
            var txt = '';

      //bwt munculin judul
            if (param.param[1]) {
                $('#topic').text('Update ' + self.viewtitle);
                $('#Status').attr('disabled', true);
                getUpdateData(param, self);
                post = param.param[1];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }

      //Mengatur tombol back
            if(param.param[0]) {
              routeback = '/detail.' + param.param[0];
            }
            $("#back").attr('href', '#/transaction/salesvisitation' + routeback);

            $('#Status').change(function(){
              // alert($(this).val());
              $('#StatusTerpilih').val($(this).val());
            });

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  console.log(ser);
                  $F.service({
                      type: 'post',
                      url: 'detail/action/' + param.param[0],
                      data: JSON.stringify(ser),
                      success: function(data){
                        if(data != null) {
                          if(data.Status == 0) {
                              $F.popup.show({
                                  content: data.Message
                              });
                              $('.modal-content').attr("style", "width : 250px");
                              setTimeout(function() {
                                  window.location = $F.config.get('baseUri') + '#/transaction/salesvisitation' + routeback;
                                  $('.popup-close-button').click();
                              }, 1500);
                          } else {
                            var err = '';
                            for (var field in data.Errors) {
                                err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                        } else {
                              DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                        }
                      }
                  }); //tutup service


               //    var ser = $F.serialize('#action');
               //    $F.service({
               //      type: 'post',
               //      url: self.urlController+'action',
               //      data: JSON.stringify({"data":ser}),
               //      success: function(data){
               //        if (data.data.Message == 'Success') {
               //          $F.popup.show({content:data.Messages});
               //          $('.modal-content').attr("style", "width : 250px");
               //             setTimeout(function () {
               //                 window.location = $F.config.get('baseUri')+'#/user/shift';
               //                 $('.popup-close-button').click();
               //             }, 1500);
               //        } else {
               //        var err = '';
               //        for(var field in data.errors) {
               //         err += data.errors[field] + '</br>';
               //       }
               //       DZ.showAlertWarning(err, "#action :submit", 0);
               //            }
               //     }
               // });
                });
        },
        // refreshTable: function (self) {
        //   $F.service({
        //     url:self.urlControllerBrand+"getAllData",
        //     type: 'GET',
        //     success: function(data){
        //       var x = data.length;
        //       var body = $("#BrandID");
        //       for(var i = 0; i<x; i++){
        //         var item = data[i];
        //         var td = "<option value='"+item.BrandID+"'>"+item.BrandName+"</option>";
        //         body.append(td);
        //       }
        //     }
        //   });
        // }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateDetailData/' + param.param[1],
            success: function(data) {
              if(data != null) {
                if(data.SalesVisitationDetailbyID[0]) {
                  var item = data.SalesVisitationDetailbyID[0];
                  $('#SalesVisitationDetailID').val(item.SalesVisitationDetailID);
                  $('#Status').val(item.Status);
                  $('#StatusTerpilih').val(item.Status);
                  $('#Description').val(item.Description);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
