$F.loadView(function () {
  "use strict";

  return {
   title: 'Quotation - HelloBill',
   viewtitle: 'Quotation',
   urlController: 'quotation/',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var sf = this;
     sf.refreshTable(sf);
   //  checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    // $('.addbutton').attr('href','#/transaction/quotation/action-quotation');
   //  $('.branch').attr('href','#/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
  }, refreshTable: function (self) {
    $F.service({
        url:self.urlController+"getAllData",
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data.Quotation != null) {
            var x = data.Quotation.length;
            var row = $("#row");
            var body = $("#Body");
            var usr = data["UserID"];
            var usrtype = data["UserTypeName"];
            // alert(usrtype);

            for(var i = 0; i < x; i++){
              var item = data.Quotation[i];
              if(usrtype != "Sales" && usrtype != "Reseller" || usr == item.UserID) {
                var temp = row.clone();
                temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
                $(".Number", temp).text(item.QuotationNO);
                $(".BranchName", temp).text(item.BranchName);
                $(".PIC", temp).text(item.UserFullName);
                $(".Status", temp).text(item.Status);
                $(".GrandTotal", temp).text(item.GrandTotal);
                $(".ResPrice", temp).text(item.GrandTotalReseller);
                $(".Commission", temp).text(item.Commission);
                if(item.Status == "Approved" || item.Status == "PDF Downloaded") {
                  if(usr == item.UserID) {
                    if (item.Status == "PDF Downloaded") {
                      $('.act', temp).append('<option value="2" class="afterApp">Request Invoice</option>');
                    }
                    $('.act', temp).append('<option value="1" class="afterApp">Download</option>');
                  }
                } else if(item.Status == "Requesting Invoice" || item.Status == "Invoice Generated") {
                  if(usr == item.UserID) {
                    $('.act', temp).append('<option value="1" class="afterApp">Download</option>');
                    if(usrtype == "Reseller") {
                      $('.act', temp).append('<option value="4" class="genInvRes">Generate Invoice Reseller</option>');
                    }
                  }
                  if(item.Status == "Requesting Invoice") {
                    if(usrtype == "Finance") {
                      $('.act', temp).append('<option value="5" class="genInv">Generate Invoice</option>');
                    }
                    // $('.act', temp).append('<option value="3" class="genInvRes">Generate Invoice Reseller</option>');
                  }
                }

                // if(usrtype == "Head of Sales" || usrtype == "Sales" || usrtype == "Reseller") {
                //   $('.afterApp').removeClass('looptemplate');
                //   if(usrtype == "Reseller") {
                //     $('.genInvRes').removeClass('looptemplate');
                //   }
                // } if(usrtype == "Finance") {
                //   $('.genInv').removeClass('looptemplate');
                // }

                $('.act', temp).attr("attr-id", item.QuotationID);
                body.append(temp);

                $('.act', temp).change(function () {
                  var Val = $(this).val();
                  var attrID = $(this).attr("attr-id");
                  if(Val == 3)
                    window.location.href = '#/transaction/quotation/action-quotation.'+attrID;
                  else if(Val == 1)
                    window.location.href = '#/transaction/quotation/preview.'+attrID;
                  else if(Val == 2) {
                    $F.service({
                      type: 'post',
                      url: 'quotation/actionStatus/'+attrID,
                      data: JSON.stringify({
                        Status: 'Requesting Invoice'
                      }),
                      success: function(d){
                        if(d.Status == 0) {
                          location.reload();
                        } else {
                          DZ.showAlertWarning("Username or password is wrong", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                } else if(Val == 4) {
                    //disini create invoice utk reseller
                    $F.service({
                      type: 'post',
                      url: 'invoice/actionReseller',
                      data: JSON.stringify({
                        id: attrID
                      }),
                      success: function(d){
                        if(d.Status == 0) {
                          window.location.href = '#/transaction/invoicereseller';
                        } else {
                          DZ.showAlertWarning("Username or password is wrong", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                } else if(Val == 5) {
                    //disini untuk generate invoice, setelah itu status berubah jadi invoice generated
                    $F.service({
                      type: 'post',
                      url: 'invoice/action',
                      data: JSON.stringify({
                        id: attrID
                      }),
                      success: function(d){
                        if(d.Status == 0) {
                          $F.service({
                            type: 'post',
                            url: 'quotation/actionStatus/'+attrID,
                            data: JSON.stringify({
                              Status: 'Invoice Generated'
                            }),
                            success: function(d){
                              if(d.Status == 0) {
                                window.location.href = '#/transaction/invoice';
                              } else {
                                DZ.showAlertWarning("Username or password is wrong", "#primarycontent", 0);
                              }
                            }
                          }); //tutup service
                          // window.location.href = '#/transaction/invoice';
                        } else {
                          DZ.showAlertWarning("Username or password is wrong", "#primarycontent", 0);
                        }
                      }
                    }); //tutup service
                  }
                });
              }
            } //akhir loop for
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
  }
}

//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
