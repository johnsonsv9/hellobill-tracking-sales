$F.loadView(function() {
    "use strict";
    return {
       title: 'Master Brand - HelloBill',
       viewtitle: 'Master Brand',
       urlController: 'brand/',
       urlControllerProduct: 'product/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = '';
            // var c = document.getElementById('myCanvas');
            // var filename = '';

            $("#Files").change(function() {
              // var ctx = c.getContext("2d");
              // var img = document.getElementById('PhotoCategory');
              // ctx.drawImage(img, 10, 10);
              // alert(c.toDataURL());
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#PhotoCategory')
                            .attr('src', e.target.result);
                    };
                    reader.readAsDataURL(this.files[0]);
                    // filename = this.value.split(/(\\|\/)/g).pop();
                    // alert(fileName);
                    $('.close').removeAttr('delete').removeClass('hidden');
                }
            });

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                // getUpdateData(param, self);
                post = param.param[0];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }
            self.refreshTable(param, self, txt);

            $('#PhotoCategory').click(function() {
              $('#Files').click();
            });
            $('.close').click(function() {
               $('#PhotoCategory').attr('src', 'assets/images/default-item.jpg');
               $("#Files").val("");
               $(this).attr('delete', 'true');
               $('.close').addClass('hidden');
           });

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();
                  var ser = $F.serialize('#action');
                  var optiondata;
                  if($('#Files').val().length != 0) {
                    var maxSize = 2 * 1024 * 1024;
                    var fileData = [];
                    var myFile = document.getElementById('Files');
                    var col = [myFile];
                    var f = col[0];
                    if (f.files[0].size > maxSize) {
                      var report = 'Your file is more than 2 MB\nYour file is ' + f.files[0].size + ' bytes';
                      DZ.showAlertWarning(report, "#action :submit", 0);
                      return false;
                    }
                    self.readFile(f, function(a, d) {
                        fileData.push(d);
                        optiondata = {
                            files: fileData,
                            data: ser,
                            id: param.param[0],
                            delete: $('.close').attr('delete')
                        };
                        console.log(JSON.stringify({
                            optiondata
                        }));
                        $F.service({
                            type: 'post',
                            url: self.urlController + 'action',
                            data: JSON.stringify({
                                optiondata
                            }),
                            success: function(data) {
                              if (data != null) {
                                if (data.Status == 0) {
                                    $F.popup.show({
                                        content: data.Message
                                    });
                                    $('.modal-content').attr("style", "width : 250px");
                                    setTimeout(function() {
                                        window.location = $F.config.get('baseUri') + '#/master/brand';
                                        $('.popup-close-button').click();
                                    }, 1500);
                                } else {
                                    var err = '';
                                        for (var field in data.Errors) {
                                            err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                                        }
                                    DZ.showAlertWarning(err, ".modal-footer", 0);
                                }
                            } else DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                          }
                        }); //tutup service
                    });
                  } else {
                    optiondata = {
                        data: ser,
                        id: param.param[0]
                        // delete: $('.close').attr('delete')
                    };
                    console.log(JSON.stringify({optiondata}));
                    $F.service({
                        type: 'post',
                        url: self.urlController + 'action',
                        data: JSON.stringify({
                            optiondata
                        }),
                        success: function(data) {
                            if (data.Status == 0) {
                                $F.popup.show({
                                    content: data.Message
                                });
                                $('.modal-content').attr("style", "width : 250px");
                                setTimeout(function() {
                                    window.location = $F.config.get('baseUri') + '#/master/brand';
                                    $('.popup-close-button').click();
                                }, 1500);
                            } else {
                                var err = '';
                                // if (data.data.Message == 'Invalid Input') {
                                    for (var field in data.Errors) {
                                        err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                                    }
                                // } else {
                                //     err = data.data.errors;
                                // }
                                DZ.showAlertWarning(err, ".modal-footer", 0);
                            }
                        }
                    }); //tutup service
                  }
                  // ser.Base64 = c.toDataURL();
                  // ser.FileName = filename;
                  // console.log(ser);
                  // $F.service({
                  //   type: 'post',
                  //   url: 'brand/action',
                  //   data: JSON.stringify(ser),
                  //   success: function(d){
                  //     if(d != null) {
                  //       if(d.Status == 0) {
                  //         $F.popup.show({
                  //           content: d.Message
                  //         });
                  //         $('.modal-content').attr("style", "width : 250px");
                  //         setTimeout(function() {
                  //           window.location = $F.config.get('baseUri') + '#/master/brand';
                  //           $('.popup-close-button').click();
                  //         }, 1500);
                  //       // }
                  //       // if(d.Status == 0) {
                  //       //   window.location= '#/master/brand';
                  //       // } else {
                  //       //   for(var i = d.Errors.length-1; i >= 0; i--) {
                  //       //     DZ.showAlertWarning(d.Errors[i].Message, "#error_" + d.Errors[i].ID, 0);
                  //       //     $('#' + d.Errors[i].ID).focus();
                  //       //   }
                  //       // }
                  //     } else {
                  //       var err = '';
                  //       // if (d.Message == 'Invalid Input') {
                  //         for (var field in d.Errors) {
                  //           err += d.Errors[field].ID + ' : ' + d.Errors[field].Message + '</br>';
                  //         }
                  //       // } else {
                  //       //   err = d.Errors;
                  //       // }
                  //       DZ.showAlertWarning(err, ".modal-footer", 0);
                  //       // DZ.showAlertWarning("Something is wrong, no data is " + txt, "#error_insert_update", 0);
                  //     }
                  //   }
                  //   }
                  // }); //tutup service

                });
        },
        readFile(el, afterRead) {
            if (!window.FileReader) {
                afterRead(false, null);
                return false;
            }
            var read = new window.FileReader();
            read.onload = (function(after, f) {
                return function(e) {
                    var output = {
                        Data: window.btoa(e.target.result),
                        Filename: f.name,
                        size: f.size,
                        type: f.type
                    }
                    after(true, output);
                }
            })(afterRead, el.files[0]);

            if (read.readAsBinaryString) {
                read.readAsBinaryString(el.files[0]);
            } else {
                read.readAsArrayBuffer(el.files[0]);
            }
            return true;
        }, refreshTable: function (param, self, jenis) {
          $F.service({
            url:self.urlControllerProduct+"getAllData",
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.length;
                var body = $("#ProductID");
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data[i];
                  var td = "<option value='"+item.ProductID+"'>"+item.ProductName+"</option>";
                  body.append(td);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                var item = data.Brand[0];
                $('#BrandID').val(item.BrandID);
                $('#ProductID').val(item.ProductID);
                $('#BrandCode').val(item.BrandCode);
                $('#BrandName').val(item.BrandName);
                if(item.Image != "" && item.Image != null){
                  $('#PhotoCategory').attr('src', item.Image);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
