$F.loadView(function () {
  "use strict";

  return {
   title: 'Master Branch - HelloBill',
   viewtitle: 'Master Branch',
   urlController: 'branch/',
   urlControllerHeader: 'brand/',
   object: 'branch',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
    var sf = this;
    // checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    $('.addbutton').attr('href','#/master/brand/branch/action-branch.'+ arg.param[0]);
    $('#back').attr('href','#/master/brand');
    sf.refreshTable(sf, arg);

    //memeriksa akses tanpa parameter (tidak mungkin ada branch yang tidak ada brand)
    if (arg.param[0]) {
        var post = arg.param[0];
        getHeaderData(arg, sf);
    } else {
        window.location = "#/master/brand";
    }

    // alert(arg.param[0]);
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
 }, refreshTable: function (self, arg) {
    $F.service({
        url:self.urlController+"getAllData/"+arg.param[0],
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data != null) {
            var x = data.Branch.length;
            var row = $("#row");
            var body = $("#Body");

            for(var i = 0; i < x; i++){
              var temp = row.clone();
              temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
              var branch = data.Branch[i];
              $(".BranchCode", temp).text(branch.BranchCode);
              $(".BranchName", temp).text(branch.BranchName);
              $(".Address", temp).text(branch.Address);
              $(".Phone", temp).text(branch.Phone);
              $(".Status", temp).text(branch.Status);
              $('.act', temp).attr("attr-id", branch.BranchID);
              body.append(temp);

              $('.act', temp).change(function () {
                var Val = $(this).val();
                var attrID = $(this).attr("attr-id");
                if(Val == 1)
                  window.location.href = '#/master/brand/branch/action-branch.'+arg.param[0]+'/'+attrID;
                else if(Val == 2) {
                  // var ser = $F.serialize('#action');
                  // console.log(ser);
                  var r=confirm("Are you sure to delete this " + self.object + " ?");
                  if(r) {
                  $F.service({
                    type: 'post',
                    url: 'branch/deleteUpdateData/'+attrID,
                    data: JSON.stringify({
                      id:attrID
                    }),
                    success: function(d){
                      if(d != null) {
                        if(d.Status == 0) {
                            DZ.showAlertSuccess(self ,"Success", "#primarycontent", 3);
                            self.refreshTable(self);
                        }
                      } else {
                        DZ.showAlertWarning("Something is wrong, row is not deleted", "#primarycontent", 0);
                      }
                    }
                  }); //tutup service
              } else $(this).val(0);}
              });

            }
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
            }
}

    // function checkPermission(){
    //     $F.service({
    //         url:'init/getValidPermissionSingle/payment-method',
    //         type: 'GET',
    //         success: function(data){
    //             if(data.data.Permissions[0].HavePermission === false)
    //                 window.location.href = $F.config.get('baseUri') + 'forbidden.html';
    //         }
    //     })
    //  };
    function getHeaderData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlControllerHeader + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                var item = data.Brand[0];

                $("#topic").text(item.BrandName);
                // $("#Topic").text(item.BrandName);
                // document.getElementById("Topic").innerHTML(item.BrandName);
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }

}());
