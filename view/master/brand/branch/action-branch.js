$F.loadView(function() {
    "use strict";
    return {
       title: 'Master Branch - HelloBill',
       viewtitle: 'Master Branch',
       urlController: 'branch/',
       urlControllerBrand: 'brand/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = ''; //menyimpan branch ID
            var routeback = ''; //menyimpan path /branch.brandID atau string kosong
            var txt = '';

      //bwt munculin judul
            if (param.param[1]) {
                $('#topic').text('Update ' + self.viewtitle);
                getUpdateData(param, self);
                post = param.param[1];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }

      //Mengatur tombol back, dan memeriksa akses tanpa parameter (tidak mungkin add branch tapi gada brand nya)
            if(param.param[0]) {
              routeback = '/branch.' + param.param[0];
            } else {
              window.location = '#/master/brand';
            }
            $("#back").attr('href', '#/master/brand' + routeback);

            var numbersOnly = document.getElementsByClassName('numbers-only');
            for(var i = 0 ; i < numbersOnly.length ; i++) {
              numbersOnly[i].addEventListener('keydown', function(e) {
                var key   = e.keyCode ? e.keyCode : e.which;

                if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                     (key >= 96 && key <= 105)
                   )) e.preventDefault();
             });
           }

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  var optiondata = {
                    data: ser,
                    brand: param.param[0],
                    id: param.param[1]
                    // delete: $('.close').attr('delete')
                  };
                  console.log(optiondata);
                  $F.service({
                      type: 'post',
                      url: 'branch/action',
                      data: JSON.stringify({optiondata}),
                      success: function(d){
                        if(d != null) {
                          if(d.Status == 0) {
                            $F.popup.show({
                              content: d.Message
                            });
                            $('.modal-content').attr("style", "width : 250px");
                            setTimeout(function() {
                              window.location = $F.config.get('baseUri') + '#/master/brand' + routeback;
                              $('.popup-close-button').click();
                            }, 1500);
                              // window.location= '#/master/brand' + routeback;
                          } else {
                            var err = '';
                            // for(var i = d.Errors.length-1; i >= 0; i--) {
                            //   DZ.showAlertWarning(d.Errors[i].Message, "#error_" + d.Errors[i].ID, 0);
                            //   $('#' + d.Errors[i].ID).focus();
                            // }
                            for (var field in d.Errors) {
                              err += d.Errors[field].ID + ' : ' + d.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                        } else {
                              DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                        }
                      }
                  }); //tutup service


               //    var ser = $F.serialize('#action');
               //    $F.service({
               //      type: 'post',
               //      url: self.urlController+'action',
               //      data: JSON.stringify({"data":ser}),
               //      success: function(data){
               //        if (data.data.Message == 'Success') {
               //          $F.popup.show({content:data.Messages});
               //          $('.modal-content').attr("style", "width : 250px");
               //             setTimeout(function () {
               //                 window.location = $F.config.get('baseUri')+'#/user/shift';
               //                 $('.popup-close-button').click();
               //             }, 1500);
               //        } else {
               //        var err = '';
               //        for(var field in data.errors) {
               //         err += data.errors[field] + '</br>';
               //       }
               //       DZ.showAlertWarning(err, "#action :submit", 0);
               //            }
               //     }
               // });
                });
        },
        // refreshTable: function (self) {
        //   $F.service({
        //     url:self.urlControllerBrand+"getAllData",
        //     type: 'GET',
        //     success: function(data){
        //       var x = data.length;
        //       var body = $("#BrandID");
        //       for(var i = 0; i<x; i++){
        //         var item = data[i];
        //         var td = "<option value='"+item.BrandID+"'>"+item.BrandName+"</option>";
        //         body.append(td);
        //       }
        //     }
        //   });
        // }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[1],
            success: function(data) {
              if(data != null) {
                var item = data.BranchDetail[0];
                $('#BranchID').val(item.BranchID);
                $('#BranchCode').val(item.BranchCode);
                $('#BranchName').val(item.BranchName);
                $('#Address').val(item.Address);
                $('#Phone').val(item.Phone);
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
