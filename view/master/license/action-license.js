$F.loadView(function() {
    "use strict";
    return {
       title: 'Master License - HelloBill',
       viewtitle: 'Master License',
       urlController: 'license/',
       urlControllerProduct: 'product/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = '';

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                // getUpdateData(param, self);
                post = param.param[0];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }
            self.refreshTable(param, self, txt);

      //Commission Percentage or Value
            // var textboxPersen = document.getElementById("Percentage");
            // var textboxValue = document.getElementById("Value");
            // var radioPersen = document.getElementById("persen");
            // var radioValue = document.getElementById("value");
            //
            // textboxValue.disabled = true;
            // textboxValue.style.backgroundColor = "Gainsboro";
            //
            // $("#persen").click(function() {
            //   textboxValue.disabled = true;
            //   textboxPersen.disabled = false;
            //   textboxPersen.style.backgroundColor = "white";
            //   if(radioValue.checked){
            //       radioValue.checked = false;
            //       textboxValue.style.backgroundColor = "Gainsboro";
            //   }
            // });
            //
            // $("#value").click(function(){
            //   textboxPersen.disabled = true;
            //   textboxValue.disabled = false;
            //   textboxValue.style.backgroundColor = "white";
            //   if(radioPersen.checked){
            //     radioPersen.checked = false;
            //     textboxPersen.style.backgroundColor = "Gainsboro";
            //   }
            // });
            $('input.terikat').keyup(function(){
              if($(this).val()) {
                $('input.terikat').not(this).val('');
                // $('input.terikat').not(this).removeAttr('required');
              } else {
                // $('input.terikat').prop('required', 'required');
              }
            });

            var numbersOnly = document.getElementsByClassName('numbers-only');
            for(var i = 0 ; i < numbersOnly.length ; i++) {
              numbersOnly[i].addEventListener('keydown', function(e) {
                var key   = e.keyCode ? e.keyCode : e.which;

                if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                     (key >= 96 && key <= 105)
                   )) e.preventDefault();
             });
           }


      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                    var ser = $F.serialize('#action');
                    var optiondata = {
                      data: ser,
                      id: param.param[0]
                    };
                    console.log(optiondata);
                    $F.service({
                      type: 'post',
                      url: 'license/action',
                      data: JSON.stringify({optiondata}),
                      success: function(data){
                        if(data != null) {
                          if(data.Status == 0) {
                            $F.popup.show({
                                content: data.Message
                            });
                            $('.modal-content').attr("style", "width : 250px");
                            setTimeout(function() {
                                window.location = $F.config.get('baseUri') + '#/master/license';
                                $('.popup-close-button').click();
                            }, 1500);
                          } else {
                            var err = '';
                            for (var field in data.Errors) {
                                err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                        } else {
                          DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                        }
                      }
                    });

                });
        }, refreshTable: function (param, self, jenis) {
          $F.service({
            url:self.urlControllerProduct+"getAllData",
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.length;
                var body = $("#ProductID");
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data[i];
                  var td = "<option value='"+item.ProductID+"'>"+item.ProductName+"</option>";
                  body.append(td);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                $('#LicenseID').val(data[0].LicenseID);
                $('#LicenseCode').val(data[0].LicenseCode);
                $('#LicenseName').val(data[0].LicenseName);
                $('#ProductID').val(data[0].ProductID);
                $('#Price').val(data[0].Price);
                $('#ResPrice').val(data[0].ResellerPrice);
                $('#Duration').val(data[0].Duration);
                $('#Percentage').val(data[0].CommissionPercentage);
                $('#Value').val(data[0].CommissionValue);
                if(data[0].CommissionValue) {
                  // $("#value").prop("checked",true);
                  // $("#Value").css("background-color","white");
                  // $("#persen").prop("checked",false);
                  // $("#Percentage").css("background-color","Gainsboro");
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
