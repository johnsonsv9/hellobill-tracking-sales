$F.loadView(function() {
    "use strict";
    return {
       title: 'Master User - HelloBill',
       viewtitle: 'Master User',
       urlController: 'user/',
       urlControllerUserType: 'usertype/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = '';
            // self.refreshTable(self);

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                // getUpdateData(param, self);
                post = param.param[0];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }
            self.refreshTable(param, self, txt);

            var numbersOnly = document.getElementsByClassName('numbers-only');
            for(var i = 0 ; i < numbersOnly.length ; i++) {
              numbersOnly[i].addEventListener('keydown', function(e) {
                var key   = e.keyCode ? e.keyCode : e.which;

                if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                     (key >= 96 && key <= 105)
                   )) e.preventDefault();
             });
           }

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var newPass = $('#Password').val();
                  var confirm = $('#RePassword').val();
                  if(newPass != confirm){
                      DZ.showAlertWarning('Password and Confirm Password Does not match', "#action :submit", 0);
                  } else {
                      var ser = $F.serialize('#action');
                      var optiondata = {
                        data: ser,
                        id: param.param[0]
                      };
                      console.log({optiondata});
                      $F.service({
                        type: 'post',
                        url: 'user/action',
                        data: JSON.stringify({optiondata}),
                        success: function(data){
                          if(data != null) {
                            if(data.Status == 0) {
                              $F.popup.show({
                                  content: data.Message
                              });
                              $('.modal-content').attr("style", "width : 250px");
                              setTimeout(function() {
                                  window.location = $F.config.get('baseUri') + '#/master/user';
                                  $('.popup-close-button').click();
                              }, 1500);
                            } else {
                              var err = '';
                              for (var field in data.Errors) {
                                err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                              }
                              DZ.showAlertWarning(err, ".modal-footer", 0);
                              // for(var i = d.Errors.length-1; i >= 0; i--) {
                              //   DZ.showAlertWarning(d.Errors[i].Message, "#error_" + d.Errors[i].ID, 0);
                              //   $('#' + d.Errors[i].ID).focus();
                              // }
                            }
                          } else {
                            DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                          }
                        }
                    }); //tutup service
                }
             });
        }, refreshTable: function (param, self, jenis) {
          $F.service({
            url:self.urlControllerUserType+"getAllData",
            type: 'GET',
            success: function(data){
              if(data != null) {
                var x = data.length;
                var body = $("#UserTypeID");
                // body.empty();
                // var firstLine = "<option value=''>Please select</option>";
                // body.append(firstLine);
                // alert(jenis);
                for(var i = 0; i<x; i++){
                  var item = data[i];
                  var td = "<option value='"+item.UserTypeID+"'>"+item.UserTypeName+"</option>";
                  body.append(td);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
              if(jenis == "updated") getUpdateData(param, self);
            }
          });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                $('#UserID').val(data[0].UserID);
                $('#UserCode').val(data[0].UserCode);
                $('#UserFullName').val(data[0].UserFullName);
                // alert(data[0].UserTypeName);
                $('#UserTypeID').val(data[0].UserTypeID);
                $('#Email').val(data[0].Email);
                $('#Phone').val(data[0].Phone);
                $('#Address').val(data[0].Address);
                $('#Username').val(data[0].Username);
                $('#Password').val(data[0].Password);
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
