$F.loadView(function() {
    "use strict";
    return {
       title: 'Master Product - HelloBill',
       viewtitle: 'Master Product',
       urlController: 'product/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = '';

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                getUpdateData(param, self);
                post = param.param[0];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  var optiondata = {
                    data: ser,
                    id: param.param[0]
                  };
                  console.log(optiondata);
                  $F.service({
                    type: 'post',
                    url: 'product/action',
                    data: JSON.stringify({optiondata}),
                    success: function(data){
                      if(data != null) {
                        if(data.Status == 0) {
                          $F.popup.show({
                              content: data.Message
                          });
                          $('.modal-content').attr("style", "width : 250px");
                          setTimeout(function() {
                              window.location = $F.config.get('baseUri') + '#/master/product';
                              $('.popup-close-button').click();
                          }, 1500);
                        } else {
                          var err = '';
                          for (var field in data.Errors) {
                              err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                          }
                          DZ.showAlertWarning(err, ".modal-footer", 0);
                        }
                      } else {
                        DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                      }
                    }
                  }); //tutup service

                });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                // var item = data.Data;
                $('#ProductID').val(data[0].ProductID);
                $('#ProductCode').val(data[0].ProductCode);
                $('#ProductName').val(data[0].ProductName);
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
