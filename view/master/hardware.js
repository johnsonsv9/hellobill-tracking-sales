$F.loadView(function () {
  "use strict";

  return {
   title: 'Master Hardware - HelloBill',
   viewtitle: 'Master Hardware',
   urlController: 'hardware/',
   object: 'hardware',
   defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
     var sf = this;
     sf.refreshTable(sf);
   //  checkPermission();
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    $('.addbutton').attr('href','#/master/hardware/action-hardware');
   //  $('.branch').attr('href','#/brand/branch')
   //  $('#export').click(function () {
   //      window.location.href = 'services/index.php/'+ sf.urlController +'export_excel';
   //  });
   // $F.service({
   //        url: sf.urlController+'getValidSort/',
   //        type: 'GET',
   //        success: function(result) {
   //            length = result.ValidSortColumn.length;
   //            for(var i = 0; i < length; i++){
   //              if(result.ValidSortColumn[i] != ""){
   //              var sort = $("#i"+result.ValidSortColumn[i]);
   //              $("#i"+result.ValidSortColumn[i]).removeClass("no-sort");
   //              }
   //            }
   //            sf.refreshTable(sf);
   //        }
   //      });
  }, refreshTable: function (self) {
    $F.service({
        url:self.urlController+"getAllData",
        type: 'GET',
        success: function(data){
            DZ.remDataTable("MainTable_category");
            $('.dataRow').remove();
          if(data != null) {
            var x = data.length;
            var row = $("#row");
            var body = $("#Body");

            for(var i = 0; i < x; i++){
              var temp = row.clone();
              temp.removeAttr("id").removeClass("looptemplate").addClass("dataRow");
              $(".HardwareCode", temp).text(data[i].HardwareCode);
              $(".HardwareName", temp).text(data[i].HardwareName);
              $(".Price", temp).text(data[i].Price);
              $(".Type", temp).text(data[i].Type);
              $(".Description", temp).text(data[i].Description);
              $('.act', temp).attr("attr-id", data[i].HardwareID);
              body.append(temp);

              $('.act', temp).change(function () {
                var Val = $(this).val();
                var attrID = $(this).attr("attr-id");
                if(Val == 1)
                  window.location.href = '#/master/hardware/action-hardware.'+attrID;
                  else if(Val == 2) {
                    // var ser = $F.serialize('#action');
                    // console.log(ser);
                    var r=confirm("Are you sure to delete this " + self.object + " ?");
                    if(r) {
                    $F.service({
                      type: 'post',
                      url: 'hardware/deleteUpdateData/'+attrID,
                      data: JSON.stringify({
                        id:attrID
                      }),
                      success: function(d){
                        if(d != null) {
                          if(d.Status == 0) {
                              DZ.showAlertSuccess(self ,"Success", "#primarycontent", 3);
                              self.refreshTable(self);
                          }
                        } else {
                          DZ.showAlertWarning("Something is wrong, row is not deleted", "#primarycontent", 0);
                        }
                      }
                  }); //tutup service
              } else $(this).val(0);
                  }
              });

            }
          } else {
            DZ.showAlertWarning("Something is wrong, the services is not available for now.", "#primarycontent", 0);
          }
          $('#MainTable_category').DataTable({ "pagingType": "full_numbers" });
        }
    })
  }
}

//
//     function checkPermission(){
//         $F.service({
//             url:'init/getValidPermissionSingle/payment-method',
//             type: 'GET',
//             success: function(data){
//                 if(data.data.Permissions[0].HavePermission === false)
//                     window.location.href = $F.config.get('baseUri') + 'forbidden.html';
//             }
//         })
//      };
}());
