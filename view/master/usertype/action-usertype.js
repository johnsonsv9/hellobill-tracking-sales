$F.loadView(function() {
    "use strict";
    return {
       title: 'Master User Type - HelloBill',
       viewtitle: 'Master User Type',
       urlController: 'usertype/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = '';

      //bwt munculin judul
            if (param.param[0]) {
              $('#topic').text('Update ' + self.viewtitle);
              getUpdateData(param, self);
              post = param.param[0];
              txt = 'updated';
            } else {
              $('#topic').text('Add ' + self.viewtitle);
              txt = 'inserted';
              getOptionPermission(self);
            }

            $("input[type=radio][name=PIC]").click(function(){
                if($(this).val() == 1) {
                    $("#PPIC").removeClass("hidden");
                    $("#PassedPIC_no").prop('checked', 'checked');
                } else $("#PPIC").addClass("hidden");
            });

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  var optiondata = {
                    data: ser,
                    id: param.param[0]
                  };
                  console.log(optiondata);
                  $F.service({
                    type: 'post',
                    url: 'usertype/action',
                    data: JSON.stringify({optiondata}),
                    success: function(data){
                      if(data == null) {DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0)}
                      else {
                          if(data.Status == 0) {
                            $F.popup.show({
                                content: data.Message
                            });
                            $('.modal-content').attr("style", "width : 250px");
                            setTimeout(function() {
                                window.location = $F.config.get('baseUri') + '#/master/usertype';
                                $('.popup-close-button').click();
                            }, 1500);
                          } else {
                            var err = '';
                            for (var field in data.Errors) {
                              err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                      }
                    }
                  }); // tutup service

                });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                $('#UserTypeID').val(data[0].UserTypeID);
                $('#UserTypeCode').val(data[0].UserTypeCode);
                $('#UserTypeName').val(data[0].UserTypeName);
                $('#Description').val(data[0].Description);
                $('input[type=radio][name=PassedPIC][value='+ (data[0].PIC == true ? 1 : 0) +']').prop("checked", true);
                $('input[type=radio][name=PassedPIC][value='+ (data[0].PassedPIC == true ? 1 : 0) +']').prop("checked", true);
                $('input[type=radio][name=Revenue][value='+ data[0].RevenueType +']').prop("checked", true);

              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }

    function createPermissionList(data){
        var a = $('#iTemplateBody');
        var b = $('#iTemplate');
        var perm = data.Permission;
        for(var field in perm) {
            var itemgroup = perm[field].Entry;
            // alert(itemgroup);
            var temp = b.clone();
            temp.removeAttr('id').removeClass('looptemplate').addClass('dataRow').css("list-style-type", 'none');
            $('.permission', temp).html("<b>"+itemgroup+"</b>");
            $('.permissionID', temp).attr("data-permission",itemgroup.replace(" ","_"));
            $('.permissionID', temp).click(function(){
                if($(this).prop("checked")){
                    $("input[data-permission='"+$(this).data("permission")+"']").prop("checked", true);
                }
                else{
                    $("input[data-permission='"+$(this).data("permission")+"']").prop("checked", false);
                }
            });
            a.append(temp);
            var item = perm[field].Permissions;
            for(var i = 0; i < item.length; i++) {
                var count = 0;
                var tempChild = b.clone();
                var entry = item[i];
                tempChild.removeAttr('id').removeClass('looptemplate').addClass('dataRow').css("list-style-type","none").css("padding-left","15px");
                $('.permission', tempChild).html(entry["PermissionName"]);
                $('.permissionID', tempChild).attr("data-permission",itemgroup.replace(" ","_")).attr('value', entry["PermissionID"]).attr('name', 'Permission[]');
                $('.permissionID', tempChild).click(function(){
                    var groupCheckBox = $("input[type=checkbox][data-permission="+$(this).data("permission")+"]").eq(0);
                    var count = $("input[type=checkbox]:not(:checked)[data-permission="+$(this).data("permission")+"]").length;
                    if(groupCheckBox.prop("checked") == false) count -= 1;
                    if(count == 0) groupCheckBox.prop("checked", true);
                    else groupCheckBox.prop("checked", false);
                });
                if(entry.Checked == 1) {
                    $(".permissionID", tempChild).prop("checked", true);
                    count++;
                }
                a.append(tempChild);
            }
            if(count == item.length) {
                $('.permissionID', temp).prop("checked", true);
            }
        }
        $(".permission").click(function(){
            var statusChecked = $(this).prev().is(":checked");
            $(this).prev().click();
            // if(statusChecked) {
            //     $(this).prev().prop("checked", false);
            // } else if(statusChecked == false) {
            //     $(this).prev().prop("checked", true);
            // }
        });
    }

    function getOptionPermission(self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getPermission',
            success: function(data) {
                if(data != null) {
                    if(data.Status == 0) {
                        createPermissionList(data);
                        // console.log(data.Permission.User);
                        var res = '';
                        // alert(data.Permission);
                        var perm = data.Permission;
                        for(var field in perm) {
                            res += perm[field].Entry +'<br>';
                            var entry = perm[field].Permissions;
                            for(var i = 0; i < entry.length; i++) {
                                // console.log(entry[i]);
                                res += i+1 + '<br>';
                                var entryy = entry[i];
                                for(var item in entryy) {
                                    res += "&nbsp;" + "&nbsp;" + "&nbsp;" + item + ', ' + entryy[item] + '<br>';
                                }
                            }
                        }
                        // document.getElementById("demo").innerHTML = res;
                    }
                } else DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
            }
        });
    }

}());
