$F.loadView(function() {
    "use strict";
    return {
       title: 'Master Hardware - HelloBill',
       viewtitle: 'Master Hardware',
       urlController: 'hardware/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = "";
            var txt = '';

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                getUpdateData(param, self);
                post = param.param[0];
                txt = 'updated';
            } else {
                $('#topic').text('Add ' + self.viewtitle);
                txt = 'inserted';
            }

            var numbersOnly = document.getElementsByClassName('numbers-only');
            for(var i = 0 ; i < numbersOnly.length ; i++) {
              numbersOnly[i].addEventListener('keydown', function(e) {
                var key   = e.keyCode ? e.keyCode : e.which;

                if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                     (key >= 96 && key <= 105)
                   )) e.preventDefault();
             });
           }

      //Submit Form
            $('#action').submit(function(e) {
                  e.preventDefault();

                  var ser = $F.serialize('#action');
                  var optiondata = {
                    data: ser,
                    id: param.param[0]
                  };
                  console.log(optiondata);
                  $F.service({
                      type: 'post',
                      url: 'hardware/action',
                      data: JSON.stringify({optiondata}),
                      success: function(data){
                        if(data != null) {
                          if(data.Status == 0) {
                              $F.popup.show({
                                  content: data.Message
                              });
                              $('.modal-content').attr("style", "width : 250px");
                              setTimeout(function() {
                                  window.location = $F.config.get('baseUri') + '#/master/hardware';
                                  $('.popup-close-button').click();
                              }, 1500);
                          } else {
                            var err = '';
                            for (var field in data.Errors) {
                                err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                        } else {
                              DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                          }
                      }
                  }); //tutup service

                });
        }
    };

    function getUpdateData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                $('#HardwareID').val(data[0].HardwareID);
                $('#HardwareCode').val(data[0].HardwareCode);
                $('#HardwareName').val(data[0].HardwareName);
                $('#Price').val(data[0].Price);
                $('#Type').val(data[0].Type);
                $('#Description').val(data[0].Description);
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }
}());
