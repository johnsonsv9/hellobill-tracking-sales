$F.loadView(function() {
    "use strict";
    return {
       title: 'Master Discount - HelloBill',
       viewtitle: 'Master Discount',
       urlController: 'discount/',
       urlControllerHardware: 'hardware/',
       urlControllerLicense: 'license/',
        afterLoad: function(param) {
            var self = this;
            var th = this.parent;
            var post = '';
            var txt = 'inserted';

            // tes menggunakan json
            // var paramtes ={
            //     id:'a',
            //     code:'a'
            // };
            // var $hardwareID = new Array();
            // $hardwareID = [3,5,6,8,10];
            // param.hardwareID = $hardwareID;
            // console.log(JSON.stringify(paramtes));

      //bwt munculin judul
            if (param.param[0]) {
                $('#topic').text('Update ' + self.viewtitle);
                txt = 'updated';
                getUpdateData(param, self, txt);
                post = param.param[0];
                $('#judul_detail').removeClass("looptemplate");
            } else {
                $('#topic').text('Add ' + self.viewtitle);
            }

      //mengeluarkan detail barang2 yg akan didiskon
            $('#Type').change(function(){
              if($('#Type').val()){
                $('#judul_detail').removeClass("looptemplate");
                if($('#Type').val() == 'Hardware') {
                  // self.refreshTableTes(9, "Hardware");
                  self.refreshTableHardware(param, self, txt);
                }
                else {
                  // self.refreshTableTes(12, "License");
                  self.refreshTableLicense(param, self, txt);
                }
              } else {
                $('#judul_detail').addClass("looptemplate");
                $('#Body').empty();
              }
            });

      //Datepicker
            Date.prototype.addDays = function(days) {
              var dat = new Date(this.valueOf());
              dat.setDate(dat.getDate() + days);
              return dat;
            }
            Date.prototype.reFormat = function() {
              var twoDigitMonth = ((this.getMonth()+1) < 10) ? '0'+(this.getMonth()+1) : (this.getMonth()+1);
              var currentDate = this.getFullYear() + "-" + twoDigitMonth + "-" + this.getDate();
              return currentDate;
            }

            var fullDate = new Date();
            $('#Period').daterangepicker({
              autoUpdateInput: false,
              locale: {
                format: 'YYYY-MM-DD',
                cancelLabel: 'Clear'
              },
              drops: 'up',
              minDate: fullDate,
              startDate: fullDate,
              showDropdowns: true,
              // endDate: fullDate.addDays(7),
              // minDate: fullDate.reFormat(),
              // ranges: {
              //   'Today': [moment(), moment()],
              //   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              //   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              //   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              //   'This Month': [moment().startOf('month'), moment().endOf('month')],
              //   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              // },
              // alwaysShowCalendars: true,
              // linkedCalendars: false
              // showCustomRangeLabel: false
              // beforeShowDay: NotBeforeToday
            // }).on('apply.daterangepicker', function(ev, picker) {
            //   $('#Period').daterangepicker('setAutoUpdateInput', true);
            //   $('#Period').daterangepicker('setDrops', 'up');
            });

            $('#Period').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#Period').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
            });

            // var fullDate1 = new Date();
            // var dumDate1 = '';
            // $('#StartDate').datepicker({
            //   format: 'yyyy-mm-dd',
            //   autoclose: false,
            //   startDate: fullDate1.reFormat()
            // }).on('changeDate', function(selected) {
            //   var startDate = new Date(selected.date.valueOf());
            //   $('#EndDate').datepicker('setStartDate', startDate);
            //   alert((dumDate1 == '') ? 'Kosong' : dumDate1.getDate());
            //   // alert($('#EndDate').val());
            //   if($('#EndDate').val() == ''                         ? true  :
            //      dumDate1 == ''                                    ? true  :
            //      startDate.getFullYear() != dumDate1.getFullYear() ? (startDate.getFullYear() > dumDate1.getFullYear() ? true : false) :
            //      startDate.getMonth() != dumDate1.getMonth()       ? (startDate.getMonth() > dumDate1.getMonth() ? true : false) :
            //      startDate.getDate() > dumDate1.getDate()          ? true  : false) {
            //     // $('#EndDate').val($(this).val());
            //     $('#EndDate').datepicker('setDate', startDate);
            //     dumDate1 = new Date(selected.date.valueOf());
            //   }
            // });
            // var fullDate2 = new Date();
            // var dumDate2 = '';
            // $('#EndDate').datepicker({
            //   format: 'yyyy-mm-dd',
            //   autoclose: false,
            //   startDate: fullDate2.reFormat()
            // }).on('changeDate', function(selected) {
            //   var startDate = new Date(selected.date.valueOf());
            //   if($('#StartDate').val() == '') {
            //     // $('#EndDate').datepicker('setStartDate', startDate);
            //     $('#StartDate').datepicker('setDate', fullDate2);
            //     var inBetween = document.getElementById('EndDate').getElementsByClassName('day');
            //     for(var i = 0 ; i < inBetween.length ; i++)
            //       inBetween.style.backgroundColor = "Aquamarine";
            //     // $('#EndDate.day').css('background-color', 'coral');
            //   }
            // });


      //Discount Percentage or Value
            $('input.terikat').keyup(function(){
              if($(this).val()) {
                $('input.terikat').not(this).val('');
                // $('input.terikat').not(this).removeAttr('required');
              } else {
                // $('input.terikat').prop('required', 'required');
              }
            });

            var numbersOnly = document.getElementsByClassName('numbers-only');
            for(var i = 0 ; i < numbersOnly.length ; i++) {
              numbersOnly[i].addEventListener('keydown', function(e) {
                var key   = e.keyCode ? e.keyCode : e.which;

                if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
                     (key >= 35 && key <= 40) ||
                     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                     (key >= 96 && key <= 105)
                   )) e.preventDefault();
             });
           }

            // var textboxPersen = document.getElementById("Percentage");
            // var textboxValue = document.getElementById("Value");
            // var checkboxPersen = document.getElementById("persen");
            // var checkboxValue = document.getElementById("value");
            //
            // textboxValue.disabled = true;
            // textboxValue.style.backgroundColor = "Gainsboro";
            //
            // $("#persen").click(function(){
            //   if(checkboxPersen.checked == false){
            //     // bagian ini untuk checkbox yang bisa di uncheck kembali
    	      //    textboxPersen.disabled = true;
            //    textboxPersen.style.backgroundColor = "Gainsboro";
            //    // $("#Percentage").css("border-color", "coral");
            //  }
            //   else{
            // 		textboxValue.disabled = true;
            // 		textboxPersen.disabled = false;
            //     textboxPersen.style.backgroundColor = "white";
            // 		if(checkboxValue.checked){
            //    			checkboxValue.checked = false;
            //         textboxValue.style.backgroundColor = "Gainsboro";
            //         // $("#Value").css("border-color", "coral");
            //     }
            //     // $("#Percentage").css("border-color", "Aqua");
            //   }
            // });
            //
            // $("#value").click(function(){
            //   if(checkboxValue.checked == false){
            //     textboxValue.disabled = true;
            //     // $("#Value").css("border-color", "coral");
            //   }
            //   else {
            //     textboxPersen.disabled = true;
            //     textboxValue.disabled = false;
            //     textboxValue.style.backgroundColor = "white";
            //     if(checkboxPersen.checked){
            //       checkboxPersen.checked = false;
            //       textboxPersen.style.backgroundColor = "Gainsboro";
            //       // $("#Percentage").css("border-color", "coral");
            //     }
            //     // $("#Value").css("border-color", "Aqua");
            //   }
            // });

      //Discount Start Date and End Date
            var dur = document.getElementById("Period");
            // var dur2 = document.getElementById("EndDate");
            var checkboxDuration = document.getElementById("duration");

            dur.disabled = true;
            // dur2.disabled = true;
            dur.style.backgroundColor = "Gainsboro";
            // dur2.style.backgroundColor = "Gainsboro";

            $("#duration").click(function(){
              if(checkboxDuration.checked){
                dur.disabled = false;
                // dur2.disabled = false;
                dur.style.backgroundColor = "white";
                // dur2.style.backgroundColor = "white";
                // $("#Period").css("border-color", "Aqua");
              } else {
                dur.disabled = true;
                // dur2.disabled = true;
                dur.style.backgroundColor = "Gainsboro";
                // dur2.style.backgroundColor = "Gainsboro";
                // $("#Period").css("border-color", "coral");
              }
            });

      //Mengatur checkbox yang select all
            $('#select_all').click(
                function(){
                    checkAllorNothing(true);
              // if($(this).prop("checked") == true){
                // $(".selectedItem").prop("checked", true);
              // }
              // else{
                // $(".selectedItem").prop("checked", false);
              // }
            }
        );

            $('#unselect_all').click(function(){checkAllorNothing(false)});

            // mencoba mengambil daftar item yg terpilih di checkbox ke dalam array
            // $('#tesbut').click(function(e){
            //   e.preventDefault();
            //   var array = $('.selectedItem:checked').map(function(){
            //     return $(this).val();
            //   }).get();
            //   console.log(array);
            //   for(var i = 0; i<array.length; i++) {
            //     alert(array[i]);
            //   }
            // });

      //Submit Form
            $('#action').submit(function(e) {
               e.preventDefault();

                var ser = $F.serialize('#action');
                var optiondata = {
                  data: ser,
                  id: param.param[0]
                };
                console.log({optiondata});

            //insert ke tabel diskon dan detail diskon
                  $F.service({
                      type: 'post',
                      url: 'discount/action',
                      data: JSON.stringify(optiondata),
                      success: function(data){
                        if(data != null) {
                          if(data.Status == 0) {
                              $F.popup.show({
                                  content: data.Message
                              });
                              $('.modal-content').attr("style", "width : 250px");
                              setTimeout(function() {
                                  window.location = $F.config.get('baseUri') + '#/master/discount';
                                  $('.popup-close-button').click();
                              }, 1500);
                          } else {
                            var err = '';
                            for (var field in data.Errors) {
                                err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                            }
                            DZ.showAlertWarning(err, ".modal-footer", 0);
                          }
                        } else {
                              DZ.showAlertWarning("Something is wrong, no data is " + txt, ".modal-footer", 0);
                          }
                      }
                  }); //tutup service (insert diskon dan detail diskon)

              });
            }, refreshTableTes: function (x, tipe) {
                    // var x = 5;
                    // var row = $("#row");
                    var body = $("#Body");
                    var depan = '<div class="form-group"><div class="col-md-4 col-sm-4 col-xs-4"></div><div class="col-md-8 col-sm-8 col-xs-8">';
                    var belakang = '</div></div>';
                    body.empty();
                    for(var i = 0; i < x; i++){
                      // var temp = row.clone();
                      // temp.removeAttr("id").removeClass("looptemplate");
                      // $(".ID", temp).text('Pilihan ke-' + (i+1));
                      // $(".Name", temp).prop("value", i);
                      var temp = depan + '<input type="checkbox" class="selectedItem" name="Item[]" id="detail-'+i+'" value="' + i + '"><label for="detail-'+i+'">' + tipe + ' ke-' + (i+1) + '</label>' + belakang;
                      body.append(temp);
                    }
            }, refreshTableHardware: function (param, self, jenis) {
              $F.service({
                  url:self.urlControllerHardware+"getAllData",
                  type: 'GET',
                  success: function(data){
                    if(data != null) {
                      var x = data.length;
                      var body = $("#Body");
                      var depan = '<div class="form-group"><div class="col-md-4 col-sm-4 col-xs-4"></div><div class="col-md-8 col-sm-8 col-xs-8">';
                      var belakang = '</div></div>';
                      body.empty();

                      for(var i = 0; i < x; i++){
                        var temp = depan + '<input type="checkbox" class="selectedItem" name="Item[]" id="detail-'+i+'" value="' + data[i].HardwareID + '"> <label for="detail-'+i+'">' + data[i].HardwareName + '</label>' + belakang;
                        body.append(temp);
                      }
                      if(jenis == 'updated') {getDetailData(param, self);}
                    } else {
                      DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#Body", 0);
                    }
                  }
              })
            }, refreshTableLicense: function (param, self, jenis) {
              $F.service({
                  url:self.urlControllerLicense+"getAllData",
                  type: 'GET',
                  success: function(data){
                    if(data != null) {
                      var x = data.length;
                      var body = $("#Body");
                      var depan = '<div class="form-group"><div class="col-md-4 col-sm-4 col-xs-4"></div><div class="col-md-8 col-sm-8 col-xs-8">';
                      var belakang = '</div></div>';
                      body.empty();

                      for(var i = 0; i < x; i++){
                        var temp = depan + '<input type="checkbox" class="selectedItem" name="Item[]" id="detail-'+i+'" value="' + data[i].LicenseID + '"> <label for="detail-'+i+'">' + data[i].LicenseName + '</label>' + belakang;
                        body.append(temp);
                      }
                      if(jenis == 'updated') getDetailData(param, self);
                    } else {
                      DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#Body", 0);
                    }
                  }
              })
            }

          };

    function getUpdateData(param, self, jenis) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getUpdateData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                var item = data.Discount[0];
                $('#DiscountID').val(item.DiscountID);
                $('#DiscountCode').val(item.DiscountCode);
                $('#DiscountName').val(item.DiscountName);
                $('#Description').val(item.Description);
                $('#Type').val(item.Type);
                if(item.Type == 'Hardware') self.refreshTableHardware(param, self, jenis);
                else self.refreshTableLicense(param, self, jenis);
                // getDetailData(param, self);
                $('#Percentage').val(item.DiscountPercentage);
                $('#Value').val(item.DiscountValue);
                if(item.DiscountValue) {
                  // $("#value").prop("checked",true);
                  // $("#Value").css("background-color","white");
                  // $("#Value").prop("disabled",false);
                  // $("#persen").prop("checked",false);
                  // $("#Percentage").css("background-color","Gainsboro");
                  // $("#Percentage").prop("disabled",true);
                }
                if(item.StartDate && item.EndDate) {
                  // alert(item.StartDate);
                  // alert(item.EndDate);
                  $('#duration').prop("checked", true);
                  $('#StartDate').prop("disabled", false);
                  $('#StartDate').css("background-color", "white");
                  $('#EndDate').prop("disabled", false);
                  $('#EndDate').css("background-color", "white");
                  var i = item.StartDate;
                  var j = item.EndDate;
                  $('#Period').val(i + ' - ' + j);
                  // $('#StartDate').val(i);
                  // $('#EndDate').val(j);
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#primarycontent", 0);
              }
            }
        });
    }

    function getDetailData(param, self) {
        $F.service({
            type: 'get',
            url: self.urlController + 'getDetailData/' + param.param[0],
            success: function(data) {
              if(data != null) {
                var x = data.Discount.length;
                var select = $('.selectedItem');

                for(var i = 0; i < x; i++) {
                  var discount = data.Discount[i];
                  var item='';
                  if(discount.HardwareID) {
                    item = discount.HardwareID;
                  } else {
                    item = discount.LicenseID;
                  }
                  // alert(item);

                  select.each(function(){
                    // alert($(this).val());
                    if($(this).val() == item) {
                      // alert(item);
                      $(this).attr("checked", "checked");
                    }
                    // else alert(item);
                  });
                }
              } else {
                DZ.showAlertWarning("Cannot fetch the data, no services is available for now", "#Body", 0);
              }
            }
        });
    }

    function checkAllorNothing(is_select_all) {
        $(".selectedItem").prop("checked", is_select_all);
        // alert("aa, you chose " + is_select_all);
    }

}());
