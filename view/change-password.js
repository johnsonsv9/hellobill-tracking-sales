$F.loadView(function () {
  "use strict";

  return {
    title: 'Change Password - HelloBill',
    viewtitle: 'CHANGE PASSWORD',
    urlController: 'login/',
    defaultArguments: {
     param1: ''
   }, afterLoad: function (arg) {
    var sf = this;
    window.document.title = sf.title;
    $('#viewTitle').text(sf.viewtitle.toUpperCase());
    $('#back').attr('href', $F.config.get('baseUri'));

    $('#action').submit(function(e){
      e.preventDefault();
      var newPass = $('#NewPassword').val();
      var confirm = $('#ReNewPassword').val();
      if(newPass != confirm){
          DZ.showAlertWarning('New Password and Confirm New Password Does not match', "#action :submit", 0);
      } else {
        var ser = $F.serialize('#action');
        console.log(JSON.stringify({data:ser}));
        $F.service({
            type: 'post',
            url: sf.urlController+'changePassword',
            data: JSON.stringify({data:ser}),
            success: function(data){
                if(data.Message == 'Success') {
                    $F.alertSuccess(data.Message,'.mainalert');
                    $('#OldPassword').val("");
                    $('#NewPassword').val("");
                    $('#ReNewPassword').val("");
                } else {
                    var err = '';
                    for(var field in data.Errors) {
                        err += data.Errors[field].ID + ' : ' + data.Errors[field].Message + '</br>';
                    }
                    DZ.showAlertWarning(err, "#action :submit", 0);
                }
            }
        });
      }
    });

  }
};
}());
