
$(document).ready(function($) {

  $('#username').focus();


    $F.service({
        url: 'init/initialize',
        success: function (data) {
            if(data.IsLogin == '1') {
                window.location= $F.config.get('baseUri');
            }
        }
    });

    const input = document.querySelector('#password');
    input.addEventListener('keyup', function(e){
      if(e.keyCode === 13) {
        e.preventDefault();
        var ser = $F.serialize('#action');
        console.log(ser);
        $F.service({
            type: 'post',
            url: 'login/loginWeb',
            data: JSON.stringify({
              username:$("#username").val(),
              password:$("#password").val()
            }),
            success: function(d){
              if(d != null) {
                  if(d.Status == 0) {
                      window.location= $F.config.get('baseUri');
                  } else {
                    DZ.showAlertWarning(d.Message, "#error", 0);
                  }
                } else {
                    DZ.showAlertWarning("Something is wrong, no services is available for now", "#error", 0);
                }
            }
        });
      }
    });

    $('#loginButton').click(function(e){
        e.preventDefault();
        var ser = $F.serialize('#action');
        console.log(ser);
        $F.service({
            type: 'post',
            url: 'login/loginWeb',
            data: JSON.stringify({
              username:$("#username").val(),
              password:$("#password").val()
            }),
            success: function(d){
              if(d != null) {
                  if(d.Status == 0) {
                      window.location= $F.config.get('baseUri');
                  } else {
                    DZ.showAlertWarning(d.Message, "#error", 0);
                  }
                } else {
                    DZ.showAlertWarning("Something is wrong, no services is available for now", "#error", 0);
                }
            }
        });
    });
});
