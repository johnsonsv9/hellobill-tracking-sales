
$(document).ready(function($) {
// getValidPermission();
// ---------------------------------SSO-------------------------------------------
    // $F.service({
    //     type: 'get',
    //     url: 'init/checkProductCode',
    //     success: function(data){
    //         if(data.otp == '1'){
    //             $('#changeBranch').remove();
    //             $('#branchName').text(data.branch);
    //         }
    //         else if(data.otp == '2')
    //             window.location.href = $F.config.get('changeBranch');
    //         else if(data.otp == '0'){
    //             $('#branchName').text(data.branch);
    //             $('.changeBranch').click(function(e){
    //                 e.preventDefault();
    //                 $F.service({
    //                     type: 'get',
    //                     url: 'login/auth',
    //                     success: function(data){
    //                         window.location.href = $F.config.get('changeBranch');
    //                     }
    //                 })
    //             });
    //         }
    //         if(data.status != 'match'){
    //             if(data.uri === false)
    //                 window.location.href = $F.config.get('loginUri');
    //             else
    //                 window.location.href = data.uri;
    //         }
    //     }
    // })
//    ------------------------------------Non SSO--------------------------------
//    $F.service({
//        type: 'get',
//        url: 'init/getOptionBranch',
//        success: function(data){
//            for (var i = 0; i < data.data.ValidBranchs.length; i++) {
//                var opt = $('<option></option>').attr('value', data.data.ValidBranchs[i].BranchID).text(data.data.ValidBranchs[i].BranchName);
//                $('#SBranchID').append(opt);
//            }
//            $("#SBranchID").val(data.data.activeBranch).select2();
//        }
//    })
//
//    $('#SBranchID').change(function() {
//        changeActiveBranch();
//    });
//    function changeActiveBranch(){
//        var branchID = $('#SBranchID').val();
//        $F.service({
//            type: 'get',
//            url: $F.util.buildUrl('init/activeBranch/',{
//                SBranchID : branchID
//            }),
//            success: function(data){
//                location.reload();
//            }
//        })
//    }
//
 function getValidPermission(){
     $F.service({
        type: 'get',
        url: 'init/getValidPermission',
        success: function(data){
            var menuLength = data.data.Menu.length;
            for(var i = 0; i < menuLength; i++){
                var menu = data.data.Menu[i];
                var span = '<span class="menu-icon ' + menu.ClassName + '"></span>';
                var p = '<p>' + menu.Name + '</p>';
                if(menu.Link === null){
                    menu.Link = '#';
                }
                var href = '';
                var ul = '';
                var subLength = menu.MenuList.length;
                    for(var j = 0; j < subLength; j++){
                        var sub = menu.MenuList[j];
                        var ul = ul + '<li class="sub-nav" id="' + sub.BackendPermissionID + '"><a href="' + sub.Link + '">' + sub.Name + '</a></li>';
                    }
                if(subLength > 0){

                    var ul = '<ul class="sub-menu"> ' + ul + '</ul>';
                    var arrow = '<span class="arrow"></span>';
                    href = '<a href="' + menu.Link + '" class="waves-effect waves-button">' + span + p + arrow + '</a>';
                    var li = '<li class="droplink nav menu ' + menu.Name + '" id="' + menu.BackendPermissionID + '">' + href + ul + '</li>';
                } else {
                    href = '<a href="' + menu.Link + '" class="waves-effect waves-button">' + span + p + '</a>';
                    var li = '<li class="nav menu ' + menu.Name + '" id="' + menu.BackendPermissionID + '">' + href + '</li>';
                }
                $('.left-sidebar').find('.page-sidebar-inner').find('.accordion-menu').append(li);
            }
            $.getScript("assets/js/modern.min.js", function(){
            });
            $('.sub-nav.hidden').remove();
            $('.nav.hidden').remove();

        }
     })
 }
});
